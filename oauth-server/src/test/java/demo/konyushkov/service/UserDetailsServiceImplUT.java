package demo.konyushkov.service;

import demo.konyushkov.base.exception.ForbiddenException;
import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.service.CaptchaClient;
import demo.konyushkov.model.AuthoritiesEnum;
import demo.konyushkov.model.Role;
import demo.konyushkov.model.User;
import demo.konyushkov.principal.UserPrincipal;
import demo.konyushkov.repository.UserRepository;
import demo.konyushkov.specification.UserSpecifications;
import demo.konyushkov.web.utils.HttpUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserDetailsServiceImplUT {

    private UserDetailsService userDetailsService;
    @Mock
    AttemptStore attemptStore;
    @Mock UserRepository userRepository;
    @Mock HttpServletRequest request;
    @Mock CaptchaClient captchaClient;

    private static final String IP = "192.168.0.1";
    private static final String NOT_FOUND_CODE = "{AbstractUserDetailsAuthenticationProvider.badCredentials}";
    private static final UUID ID = UUID.randomUUID();
    private static final String EMAIL = "test@mai.ru";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String PASSWORD_HASH = "$2a$04$KXKrdHLqqWXex11KGXqHreJRD3jPIHF1y07s.f8aVc0qo/nj5y8Gi";

    private final User user = new User();

    @BeforeEach
    void init() {
        userDetailsService = new UserDetailsServiceImpl(userRepository, attemptStore, request, captchaClient);

        Mockito.when(HttpUtils.getClientIP(request)).thenReturn(IP);

        user.setId(ID);
        user.setEmail(EMAIL);
        user.setRole(new Role(ROLE_ADMIN, "", Set.of(AuthoritiesEnum.READ, AuthoritiesEnum.WRITE)));
        user.setPassword(PASSWORD_HASH);
    }

    @Nested
    class LoadUserByUserName {

        Specification<User> specification = UserSpecifications.findByEmailFetchRole(EMAIL);

        @Test
        void When_CorrectCredentialsAndCaptchaHide_Expect_ReturnUserPrincipal() {
            Mockito.when(attemptStore.isExceeded(IP)).thenReturn(false);
            Mockito.when(userRepository.findOne(any(specification.getClass()))).thenReturn(Optional.of(user));

            UserPrincipal principal = (UserPrincipal) userDetailsService.loadUserByUsername(EMAIL);
            assertNotNull(principal, "Find single user by EMAIL");
            assertEquals(user.getId().toString(), principal.getUsername(), "Return correct object");
        }

        @Test
        void When_CorrectCredentialsAndCaptchaShowAndCorrectCaptchaValidation_Expect_ReturnUserPrincipal() {
            CaptchaResponse successResponse = new CaptchaResponse(true);

            Mockito.when(attemptStore.isExceeded(IP)).thenReturn(true);
            Mockito.when(captchaClient.validate(any(CaptchaRequest.class))).thenReturn(successResponse);
            Mockito.when(userRepository.findOne(any(specification.getClass()))).thenReturn(Optional.of(user));

            UserPrincipal principal = (UserPrincipal) userDetailsService.loadUserByUsername(EMAIL);
            assertNotNull(principal, "Find single user by EMAIL");
            assertEquals(user.getId().toString(), principal.getUsername(), "Return correct object");
        }

        @Test
        void When_CaptchaShowAndCaptchaValidationNotSuccess_Expect_ThrowRequestExceptionWithMessageReportReason() {
            String exMessage = "validation failed";
            Mockito.when(captchaClient.validate(any(CaptchaRequest.class))).thenThrow(new ForbiddenException(exMessage));
            Mockito.when(attemptStore.isExceeded(IP)).thenReturn(true);

            ForbiddenException exception = assertThrows(ForbiddenException.class, () -> userDetailsService.loadUserByUsername(EMAIL));
            assertEquals(exMessage, exception.getMessage());
        }

        @Test
        void When_CaptchaShowAndCaptchaResponseWithFalse_Expect_ThrowRequestExceptionWithMessageReportReason() {
            Mockito.when(captchaClient.validate(any(CaptchaRequest.class))).thenReturn(new CaptchaResponse(false));
            Mockito.when(attemptStore.isExceeded(IP)).thenReturn(true);

            assertThrows(ForbiddenException.class, () -> userDetailsService.loadUserByUsername(EMAIL));
        }

        @Test
        void When_InvalidCredentials_Expect_ThrowUsernameNotFoundException() {
            Mockito.when(attemptStore.isExceeded(IP)).thenReturn(false);

            Throwable exception = assertThrows(UsernameNotFoundException.class, () ->
                    userDetailsService.loadUserByUsername("invalid@mail.ru")
            );

            assertEquals(NOT_FOUND_CODE, exception.getMessage());
        }
    }
}
