package demo.konyushkov.principal;

import demo.konyushkov.model.AuthoritiesEnum;
import demo.konyushkov.model.Role;
import demo.konyushkov.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


class UserPrincipalUT {

    private User user;

    private static final UUID VALID_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");
    private static final String VALID_PASSWORD_HASH = "$2a$04$KXKrdHLqqWXex11KGXqHreJRD3jPIHF1y07s.f8aVc0qo/nj5y8Gi";
    private static final String VALID_EMAIL = "demo_test-user@mail.ru";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    @BeforeEach
    void init() {
        user = new User();
        user.setId(VALID_ID);
        user.setEmail(VALID_EMAIL);
        user.setRole(new Role(ROLE_ADMIN, "", Set.of(AuthoritiesEnum.READ, AuthoritiesEnum.WRITE)));
        user.setPassword(VALID_PASSWORD_HASH);
    }

    @Nested
    class GetAuthorities {
        @Test
        void When_UserHasAuthorities_Expect_ReturnListOfAuthoritiesEnum() {
            UserPrincipal userPrincipal = new UserPrincipal(user);
            Collection<? extends GrantedAuthority>  authorities = userPrincipal.getAuthorities();
            assertEquals(2, authorities.size());
            authorities.containsAll(Set.of(AuthoritiesEnum.READ, AuthoritiesEnum.WRITE));
        }
    }

    @Nested
    class GetUserName {
        @Test
        void When_UserPrincipalsFilled_Expect_ReturnUserId() {
            UserPrincipal userPrincipal = new UserPrincipal(user);
            assertEquals(user.getId().toString(), userPrincipal.getUsername());
        }
    }

    @Nested
    class GetUserPassword {
        @Test
        void When_UserPrincipalsFilled_Expect_ReturnUserPassword() {
            UserPrincipal userPrincipal = new UserPrincipal(user);
            assertEquals(user.getPassword(), userPrincipal.getPassword());
        }
    }

    @Nested
    class GetEnabled {

        @Test
        void When_UserEnabledIsTrue_Expect_ReturnTrue() {
            user.setEnabled(true);
            UserPrincipal userPrincipal = new UserPrincipal(user);
            assertTrue(userPrincipal.isEnabled());
        }

        @Test
        void When_UserEnabledIsFalse_Expect_ReturnFalse() {
            user.setEnabled(false);
            UserPrincipal userPrincipal = new UserPrincipal(user);
            assertFalse(userPrincipal.isEnabled());
        }

        @Test
        void When_UserEnabledIsNull_Expect_ReturnFalse() {
            user.setEnabled(null);
            UserPrincipal userPrincipal = new UserPrincipal(user);
            assertFalse(userPrincipal.isEnabled());
        }
    }
}
