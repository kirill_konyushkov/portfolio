package demo.konyushkov.controller;

import demo.konyushkov.config.WebAppContextIT;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

class OauthServerControllerIT {

    @Nested
    class IS_CAPTCHA_SHOW extends WebAppContextIT {

        @Autowired
        private WebApplicationContext wac;

        @Autowired
        PasswordEncoder passwordEncoder;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_LoginCaptchaServiceIsNotShow_Expect_ReturnFalse() throws Exception {

            String hash = passwordEncoder.encode("zaq12345");
            System.out.println(hash);
            /*
            this.mockMvc.perform(get("/oauth/captcha/show")
                    .accept(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$.value0").value(Boolean.FALSE))
                    .andDo(print());*/
        }
    }

}
