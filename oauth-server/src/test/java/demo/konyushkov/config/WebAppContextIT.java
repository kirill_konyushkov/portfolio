package demo.konyushkov.config;

import demo.konyushkov.test.config.AbstractWebAppContextIT;
import demo.konyushkov.test.config.BaseConfigIT;
import demo.konyushkov.test.config.Profiles;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@ActiveProfiles(value = {Profiles.WEB_API_IT})
@ContextConfiguration(classes = { AppConfig.class, BaseConfigIT.class})
public class WebAppContextIT extends AbstractWebAppContextIT {}
