package demo.konyushkov;

import demo.konyushkov.config.WebAppContextIT;
import demo.konyushkov.test.tools.OAuth2Helper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class OauthServerIT {

    @Nested
    class Login extends WebAppContextIT {

        static final String VALID_EMAIL = "demo_test-user@mail.ru";
        static final String VALID_PASSWORD = "zaq12345";

        static final String INVALID_EMAIL = "invalid@mail.ru";
        static final String  INVALID_PASSWORD = "invalid123";

        @Autowired private WebApplicationContext wac;
        @Autowired private OAuth2Helper helper;
        @Autowired private MessageSourceAccessor messageSourceAccessor;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_ValidClientAndUserCredentials_Expect_ReturnAccessToken() throws Exception {
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("grant_type", helper.getGrantType());
            params.add("client_id", helper.getClientId());
            params.add("username", VALID_EMAIL);
            params.add("password", VALID_PASSWORD);

            this.mockMvc.perform(post("/oauth/token")
                    .params(params)
                    .with(httpBasic(helper.getClientId(), helper.getClientSecret()))
                    .accept(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$.access_token").exists())
                    .andReturn();
        }

        @Test
        void When_InvalidClientAndValidUser_Expect_ReturnUnauthorized() throws Exception {
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("grant_type", helper.getGrantType());
            params.add("client_id", "invalid" + helper.getClientId());
            params.add("username", VALID_EMAIL);
            params.add("password", VALID_PASSWORD);

            MvcResult mvcResult = this.mockMvc.perform(post("/oauth/token")
                    .params(params)
                    .with(httpBasic( "invalid" + helper.getClientId(), helper.getClientSecret()))
                    .accept(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(status().isUnauthorized())
                    .andReturn();

            assertEquals("", mvcResult.getResponse().getContentAsString());
        }

        @Test
        void When_ValidClientAndInvalidUser_Expect_ReturnUnauthorized() throws Exception {
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("grant_type", helper.getGrantType());
            params.add("client_id", helper.getClientId());
            params.add("username", INVALID_EMAIL);
            params.add("password", INVALID_PASSWORD);

            this.mockMvc.perform(post("/oauth/token")
                    .params(params)
                    .with(httpBasic( helper.getClientId(), helper.getClientSecret()))
                    .accept(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("error").value("invalid_grant"))
                    .andExpect(jsonPath("$.error_description").value(messageSourceAccessor.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials")));
        }
    }
}
