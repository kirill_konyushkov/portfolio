package demo.konyushkov.authentification.listener;


import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.web.utils.HttpUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AuthenticationFailureListenerUT {

    private static final String IP = "192.168.1.0";

    private AuthenticationFailureListener afListener;
    @Mock AttemptStore attemptStore;
    @Mock HttpServletRequest request;
    private int failedAttempts = 0;

    @BeforeEach
    void init() {
        Mockito.when(HttpUtils.getClientIP(request)).thenReturn(IP);
        this.afListener = new AuthenticationFailureListener(attemptStore, request);
        Mockito.doAnswer((i) -> failedAttempts++).when(attemptStore).increase(IP);
    }

    @Test
    void When_OnApplicationEventTriggered_Expect_IncrementAttempts() {
        failedAttempts = 0;
        afListener.onApplicationEvent(null);
        assertEquals(1, failedAttempts);
    }
}
