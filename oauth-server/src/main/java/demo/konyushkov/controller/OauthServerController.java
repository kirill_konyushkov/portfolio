package demo.konyushkov.controller;

import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.config.qualifier.Login;
import demo.konyushkov.web.utils.HttpUtils;
import org.javatuples.Unit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/oauth")
public class OauthServerController {

    private final AttemptStore attemptStore;

    @Autowired
    public OauthServerController(@Login AttemptStore attemptStore) {
        this.attemptStore = attemptStore;
    }

    @GetMapping("/captcha/show")
    public Unit<Boolean> isCaptchaShow(HttpServletRequest request) {
        String ip = HttpUtils.getClientIP(request);
        boolean show = attemptStore.isExceeded(ip);
        return new Unit<>(show);
    }
}
