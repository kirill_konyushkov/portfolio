package demo.konyushkov.principal;

import demo.konyushkov.model.AuthoritiesEnum;
import demo.konyushkov.model.User;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class UserPrincipal implements UserDetails {

    private final String password;
    private final String username;
    private final Set<GrantedAuthority> authorities;
    private final boolean enabled;

    public UserPrincipal(@NonNull User user) {
        this.username = user.getId().toString();
        this.password = user.getPassword();
        this.authorities = user.getRole().getAuthorities()
                .stream().map(this::getGrantedAuthority).collect(Collectors.toSet());
        this.enabled = BooleanUtils.toBoolean(user.getEnabled());
    }

    private SimpleGrantedAuthority getGrantedAuthority(AuthoritiesEnum authority) {
        return new SimpleGrantedAuthority(authority.name());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
}
