package demo.konyushkov.authentification.listener;

import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.config.qualifier.Login;
import demo.konyushkov.web.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class AuthenticationSuccessEventListener
  implements ApplicationListener<AuthenticationSuccessEvent> {


    private final HttpServletRequest request;
    private final AttemptStore attemptStore;

    @Autowired
    public AuthenticationSuccessEventListener(@Login AttemptStore attemptStore, HttpServletRequest request) {
        this.attemptStore = attemptStore;
        this.request = request;
    }

    @Override
    public void onApplicationEvent(@NonNull AuthenticationSuccessEvent e) {
        String ip = HttpUtils.getClientIP(request);
        attemptStore.invalidate(ip);
    }
}
