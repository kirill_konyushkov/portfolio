package demo.konyushkov.service;

import demo.konyushkov.base.exception.ForbiddenException;
import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.base.utils.Precondition;
import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.service.CaptchaClient;
import demo.konyushkov.captcha.utils.CaptchaUtils;
import demo.konyushkov.config.qualifier.Login;
import demo.konyushkov.principal.UserPrincipal;
import demo.konyushkov.repository.UserRepository;
import demo.konyushkov.specification.UserSpecifications;
import demo.konyushkov.web.utils.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final AttemptStore attemptStore;
    private final HttpServletRequest request;
    private final CaptchaClient captchaClient;

    @Autowired
    public UserDetailsServiceImpl(UserRepository repository,
                                  @Login AttemptStore attemptStore,
                                  HttpServletRequest request, CaptchaClient captchaClient) {
        this.userRepository = repository;
        this.attemptStore = attemptStore;
        this.request = request;
        this.captchaClient = captchaClient;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        log.info(String.format("Loading user by email: %s", username));

        validateCaptcha(request);

        return userRepository.findOne(UserSpecifications.findByEmailFetchRole(username))
                .map(UserPrincipal::new)
                .orElseThrow(() -> new UsernameNotFoundException("{AbstractUserDetailsAuthenticationProvider.badCredentials}"));
    }

    private void validateCaptcha(HttpServletRequest request) {
        final String ip = HttpUtils.getClientIP(request);

        // If login failed attempts > attemptStore.MAX_ATTEMPTS, then validate captcha
        if(attemptStore.isExceeded(ip)) {
            String gResponse = CaptchaUtils.getCaptchaResponse(request);
            CaptchaRequest captchaRequest = new CaptchaRequest(ip, gResponse);
            CaptchaResponse captchaResponse = captchaClient.validate(captchaRequest);
            Precondition.checkArgument(captchaResponse.getSuccess(), ForbiddenException.class);
        }
    }
}
