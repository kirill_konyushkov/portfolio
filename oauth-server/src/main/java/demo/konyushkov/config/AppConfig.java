package demo.konyushkov.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.konyushkov.base.config.BaseConfig;
import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.base.utils.InMemoryAttemtStore;
import demo.konyushkov.config.qualifier.Login;
import demo.konyushkov.security.config.SecurityConfig;
import demo.konyushkov.web.config.AbstractWebMvcConfig;
import demo.konyushkov.web.config.WebRequestContextListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableAsync
@Import({
        BaseConfig.class,
        HibernateConfig.class,
        WebRequestContextListener.class,
        SecurityConfig.class,
        RabbitMqConfig.class
})
public class AppConfig extends AbstractWebMvcConfig {

    @Autowired
    protected AppConfig(LocalValidatorFactoryBean validator, ObjectMapper objectMapper) {
        super(validator, objectMapper);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Login
    public AttemptStore loginAttemptStore() {
        return new InMemoryAttemtStore(4, 10, TimeUnit.HOURS);
    }
}
