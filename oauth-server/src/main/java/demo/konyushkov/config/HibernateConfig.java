package demo.konyushkov.config;

import demo.konyushkov.database.config.AbstractHibernateConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HibernateConfig extends AbstractHibernateConfig {}
