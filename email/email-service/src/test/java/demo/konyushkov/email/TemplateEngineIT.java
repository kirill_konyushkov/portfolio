package demo.konyushkov.email;

import demo.konyushkov.email.config.EmailContextIT;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import java.util.UUID;

public class TemplateEngineIT {

    @Nested
    class RegistrationTemplate extends EmailContextIT {

        @Autowired ITemplateEngine templateEngine;

        @Test
        void When_ValidVariables_Expect_ReturnContent() {
            String template = "registration";
            Context context = new Context();
            context.setVariable("frontendUrl", "localhost:8081");
            context.setVariable("userId", UUID.randomUUID());
            context.setVariable("confirmCode", UUID.randomUUID());

            String content = templateEngine.process(template, context);
            System.out.println(content);
        }
    }

    @Nested
    class RestorePasswordTemplate extends EmailContextIT {

        @Autowired ITemplateEngine templateEngine;

        @Test
        void When_ValidVariables_Expect_ReturnContent() {
            String template = "restore_password";
            Context context = new Context();
            context.setVariable("frontendUrl", "localhost:8081");
            context.setVariable("userId", UUID.randomUUID());
            context.setVariable("confirmCode", UUID.randomUUID());

            String content = templateEngine.process(template, context);
            System.out.println(content);
        }
    }

}
