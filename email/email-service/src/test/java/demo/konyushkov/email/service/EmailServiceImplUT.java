package demo.konyushkov.email.service;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.dto.TextMail;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.exceptions.TemplateProcessingException;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class EmailServiceImplUT {

    @Mock JavaMailSender mailSender;
    @Mock ITemplateEngine templateEngine;
    private EmailService emailService;

    @BeforeEach
    void init() throws IllegalAccessException {
        emailService = new EmailServiceImpl(mailSender, templateEngine);
        FieldUtils.writeField(emailService, "username", "from@localhost", true);
    }

    @Nested
    class Send_TextMail {

        @Test
        void When_ValidTextMail_Expect_SendMail() {
            AtomicBoolean send = new AtomicBoolean(false);

            TextMail mail = new TextMail();
            mail.setTo("demo_test-user@mail.ru");
            mail.setSubject("Mail subject");
            String content = "Hello world content";
            mail.setContent(content);

            Mockito.doAnswer(i -> {
                send.set(true);
                return null;
            }).when(mailSender).send(any(SimpleMailMessage.class));

            emailService.send(mail);
            assertTrue(send.get());

            ArgumentCaptor<SimpleMailMessage> argumentMailMessage = ArgumentCaptor.forClass(SimpleMailMessage.class);
            verify(mailSender).send(argumentMailMessage.capture());
            SimpleMailMessage mailMessage = argumentMailMessage.getValue();

            assertNotNull(mailMessage.getTo());
            assertEquals(1, mailMessage.getTo().length);
            assertEquals(mail.getTo(), mailMessage.getTo()[0]);
            assertEquals(mail.getSubject(), mailMessage.getSubject());
            assertEquals(content, mailMessage.getText());
        }

        @Test
        void When_JavaMailSenderThrowMailException_Expect_ThrowMailException() {
            TextMail mail = new TextMail();
            mail.setContent("Hello world");
            mail.setSubject("Test");
            mail.setTo("to@localhost");

            String exMessage = "failed send";
            Mockito.doThrow(new MailSendException(exMessage)).when(mailSender).send(any(SimpleMailMessage.class));

            MailException exception = assertThrows(MailException.class, () -> emailService.send(mail));
            assertEquals(exMessage, exception.getMessage());
        }
    }

    @Nested
    class Send_HtmlMail {

        MimeMessage mimeMessage;

        @BeforeEach
        void init() {
            mimeMessage = new MimeMessage((Session) null);
            Mockito.when(mailSender.createMimeMessage()).thenReturn(mimeMessage);
        }

        @Test
        void When_ValidHtmlMailWithVariables_Expect_SendMail() throws MessagingException, IOException {
            AtomicBoolean send = new AtomicBoolean(false);

            HtmlMail htmlMail = new HtmlMail();
            htmlMail.setTemplate("template");
            htmlMail.setTo("demo_test-user@mail.ru");
            htmlMail.setVariables(Map.of("message", "Hello world"));
            htmlMail.setSubject("Mail subject");

            String htmlContent = "Hello world html content";
            Mockito.when(templateEngine.process(eq(htmlMail.getTemplate()), any(Context.class))).thenReturn(htmlContent);



            Mockito.doAnswer(i -> {
                send.set(true);
                return null;
            }).when(mailSender).send(any(MimeMessage.class));

            emailService.send(htmlMail);
            assertTrue(send.get());

            ArgumentCaptor<Context> argumentContext = ArgumentCaptor.forClass(Context.class);
            verify(templateEngine).process(eq(htmlMail.getTemplate()), argumentContext.capture());
            Context context = argumentContext.getValue();

            ArgumentCaptor<MimeMessage> argumentMailMessage = ArgumentCaptor.forClass(MimeMessage.class);
            verify(mailSender).send(argumentMailMessage.capture());
            MimeMessage mimeMessage = argumentMailMessage.getValue();

            assertEquals(htmlMail.getVariables().get("message"), context.getVariable("message"));
            assertNotNull(mimeMessage);
            assertEquals(1, mimeMessage.getAllRecipients().length);
            assertEquals(htmlMail.getTo(), mimeMessage.getAllRecipients()[0].toString());
            assertEquals(htmlMail.getSubject(), mimeMessage.getSubject());
            assertEquals(htmlContent, mimeMessage.getContent().toString());

        }

        @Test
        void When_ValidHtmlMailWithNullVariables_Expect_SendMail() throws MessagingException, IOException {
            AtomicBoolean send = new AtomicBoolean(false);

            HtmlMail htmlMail = new HtmlMail();
            htmlMail.setTemplate("template");
            htmlMail.setTo("demo_test-user@mail.ru");
            htmlMail.setSubject("Mail subject");

            String htmlContent = "Hello world html content";
            Mockito.when(templateEngine.process(eq(htmlMail.getTemplate()), any(Context.class))).thenReturn(htmlContent);



            Mockito.doAnswer(i -> {
                send.set(true);
                return null;
            }).when(mailSender).send(any(MimeMessage.class));

            emailService.send(htmlMail);
            assertTrue(send.get());

            ArgumentCaptor<Context> argumentContext = ArgumentCaptor.forClass(Context.class);
            verify(templateEngine).process(eq(htmlMail.getTemplate()), argumentContext.capture());
            Context context = argumentContext.getValue();


            ArgumentCaptor<MimeMessage> argumentMailMessage = ArgumentCaptor.forClass(MimeMessage.class);
            verify(mailSender).send(argumentMailMessage.capture());
            MimeMessage mimeMessage = argumentMailMessage.getValue();

            assertTrue(context.getVariableNames().isEmpty());
            assertNotNull(mimeMessage);
            assertEquals(1, mimeMessage.getAllRecipients().length);
            assertEquals(htmlMail.getTo(), mimeMessage.getAllRecipients()[0].toString());
            assertEquals(htmlMail.getSubject(), mimeMessage.getSubject());
            assertEquals(htmlContent, mimeMessage.getContent().toString());
        }

        @Test
        void When_TemplateEngineThrowTemplateProcessingException_Expect_CatchAndThrowBadRequestExceptionWithCause() {
            HtmlMail htmlMail = new HtmlMail();
            htmlMail.setTemplate("template");

            String exMessage = "failed processing";
            Mockito.when(templateEngine.process(eq(htmlMail.getTemplate()), any(Context.class))).thenThrow(new TemplateProcessingException(exMessage));

            BadRequestException exception = assertThrows(BadRequestException.class, () -> emailService.send(htmlMail));
            assertEquals("{BadRequestException.message}", exception.getMessage());
            assertEquals(exMessage, exception.getCause().getMessage());
        }

        @Test
        void When_JavaMailSenderThrowMailException_Expect_ThrowMailException() {
            HtmlMail htmlMail = new HtmlMail();
            htmlMail.setTemplate("template");
            htmlMail.setTo("to@localhost");

            String exMessage = "failed send";
            String htmlContent = "html content";
            Mockito.when(templateEngine.process(eq(htmlMail.getTemplate()), any(Context.class))).thenReturn(htmlContent);
            Mockito.doThrow(new MailSendException(exMessage)).when(mailSender).send(any(MimeMessage.class));

            MailException exception = assertThrows(MailException.class, () -> emailService.send(htmlMail));
            assertEquals(exMessage, exception.getMessage());
        }

    }

}