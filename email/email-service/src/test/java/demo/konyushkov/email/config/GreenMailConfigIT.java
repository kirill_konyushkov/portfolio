package demo.konyushkov.email.config;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class GreenMailConfigIT {

    private static final String HOST = "mail.host";
    private static final String PORT = "mail.port";
    private static final String USERNAME = "mail.username";
    private static final String PASSWORD = "mail.password";

    @Bean
    public GreenMail greenMail(Environment environment) {
        Integer port = environment.getRequiredProperty(PORT, Integer.TYPE);
        String host = environment.getRequiredProperty(HOST);
        String username = environment.getRequiredProperty(USERNAME);
        String password = environment.getRequiredProperty(PASSWORD);

        ServerSetup setup = new ServerSetup(port, host, ServerSetup.PROTOCOL_SMTP);
        GreenMail greenMail = new GreenMail(setup);
        greenMail.setUser(username, password);

        return greenMail;
    }

}
