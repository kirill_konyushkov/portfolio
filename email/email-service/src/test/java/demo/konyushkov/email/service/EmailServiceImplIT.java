package demo.konyushkov.email.service;

import com.icegreen.greenmail.util.GreenMail;
import demo.konyushkov.email.config.EmailContextIT;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.dto.TextMail;
import demo.konyushkov.email.utils.MailUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.MessagingException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class EmailServiceImplIT {

    @Nested
    class Send_TextMail extends EmailContextIT {

        @Autowired GreenMail greenMail;
        @Autowired EmailService emailService;

        @BeforeEach
        void startUp() {
            greenMail.start();
        }

        @AfterEach
        void tearDown() {
            greenMail.stop();
        }

        @Test
        void When_ValidMail_Expect_SendMail() {
            String to = "to@localhost";
            String subject = "subject";
            String content = "content";

            TextMail mail = new TextMail();
            mail.setTo(to);
            mail.setSubject(subject);
            mail.setContent(content);

            emailService.send(mail);

            List<TextMail> received = Arrays.stream(greenMail.getReceivedMessages())
                    .map(MailUtils::transformMimeMessage).collect(Collectors.toList());

            assertEquals(1, received.size());

            TextMail current = received.iterator().next();

            assertEquals(mail.getSubject(), current.getSubject());
            assertEquals(mail.getTo(), current.getTo());
            assertTrue(current.getContent().contains(mail.getContent()));
        }
    }

    @Nested
    class Send_HtmlMail extends EmailContextIT {

        @Autowired GreenMail greenMail;
        @Autowired EmailService emailService;

        @BeforeEach
        void startUp() {
            greenMail.start();
        }

        @AfterEach
        void tearDown() {
            greenMail.stop();
        }

        @Test
        void When_ValidMail_Expect_SendMail() throws MessagingException {
            String to = "to@localhost";
            String subject = "subject";
            Map<String, Object> variables = Map.of("message", "Hello World!");

            HtmlMail mail = new HtmlMail();
            mail.setTo(to);
            mail.setTemplate("test.html");
            mail.setSubject(subject);
            mail.setVariables(variables);

            emailService.send(mail);

            List<TextMail> received = Arrays.stream(greenMail.getReceivedMessages())
                    .map(MailUtils::transformMimeMessage).collect(Collectors.toList());

            assertEquals(1, received.size());

            TextMail current = received.iterator().next();

            assertEquals(mail.getSubject(), current.getSubject());
            assertEquals(mail.getTo(), current.getTo());
            assertTrue(current.getContent().contains(variables.get("message").toString()));
        }
    }
}
