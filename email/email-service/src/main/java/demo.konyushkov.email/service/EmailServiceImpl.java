package demo.konyushkov.email.service;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.dto.TextMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.exceptions.TemplateProcessingException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service
public class EmailServiceImpl implements EmailService {

    @Value("${mail.username}")
    private String username;

    private final JavaMailSender mailSender;
    private final ITemplateEngine emailTemplateEngine;

    @Autowired
    public EmailServiceImpl(JavaMailSender mailSender, ITemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.emailTemplateEngine = templateEngine;
    }

    @Override
    public void send(TextMail mail) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(username);
        message.setTo(mail.getTo());
        message.setSubject(mail.getSubject());
        message.setText(mail.getContent());
        mailSender.send(message);
    }

    @Override
    public void send(HtmlMail mail) throws MessagingException {
        try {
            final Context ctx = new Context();
            if(mail.getVariables() !=  null && !mail.getVariables().isEmpty()) {
                mail.getVariables().forEach(ctx::setVariable);
            }

            String htmlContent = this.emailTemplateEngine.process(mail.getTemplate(), ctx);

            MimeMessage mimeMessage = mailSender.createMimeMessage();
            mimeMessage.setContent(htmlContent, "text/html; charset=UTF-8");
            mimeMessage.setFrom(new InternetAddress(username));
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(mail.getTo()));
            mimeMessage.setSubject(mail.getSubject(), "UTF-8");

            mailSender.send(mimeMessage);
        }
        catch (TemplateProcessingException ex) {
            throw new BadRequestException(ex);
        }
    }
}
