package demo.konyushkov.email.service;

import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.dto.TextMail;

import javax.mail.MessagingException;

public interface EmailService {
    void send(TextMail mail);
    void send(HtmlMail mail) throws MessagingException;
}
