package demo.konyushkov.email.consumer;

import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.dto.TextMail;
import demo.konyushkov.email.service.EmailService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Component
public class EmailConsumer {

    private final EmailService emailService;

    @Autowired
    public EmailConsumer(EmailService emailService) {
        this.emailService = emailService;
    }

    @RabbitListener(queues = "#{sendHtmlMailQueue.name}")
    public void send(HtmlMail mail) throws MessagingException {
        emailService.send(mail);
    }

    @RabbitListener(queues = "#{sendTextMailQueue.name}")
    public void send(TextMail mail) {
        emailService.send(mail);
    }
}
