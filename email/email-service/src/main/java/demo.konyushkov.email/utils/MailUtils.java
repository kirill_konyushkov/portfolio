package demo.konyushkov.email.utils;

import demo.konyushkov.email.dto.TextMail;
import lombok.SneakyThrows;

import javax.mail.internet.MimeMessage;

public class MailUtils {

    @SneakyThrows
    public static TextMail transformMimeMessage(MimeMessage mimeMessage) {
        return TextMail.builder()
                .subject(mimeMessage.getSubject())
                .to(mimeMessage.getAllRecipients()[0].toString())
                .content(mimeMessage.getContent().toString()).build();
    }
}
