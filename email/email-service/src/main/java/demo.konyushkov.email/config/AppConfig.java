package demo.konyushkov.email.config;

import demo.konyushkov.base.config.BaseConfig;
import demo.konyushkov.base.utils.PropsUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Configuration
@Import({BaseConfig.class, RabbitMqConfig.class})
public class AppConfig {

    private static final String PROPS_PREFIX = "mail";
    private static final String HOST = "mail.host";
    private static final String PORT = "mail.port";
    private static final String USERNAME = "mail.username";
    private static final String PASSWORD = "mail.password";

    private static final String HTML_RESOLVER_PREFIX = "/mail/";
    private static final String HTML_RESOLVER_SUFFIX = ".html";

    @Bean
    public JavaMailSender getJavaMailSender(Environment environment) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost(environment.getRequiredProperty(HOST));
        mailSender.setPort(environment.getRequiredProperty(PORT, Integer.TYPE));
        mailSender.setUsername(environment.getRequiredProperty(USERNAME));
        mailSender.setPassword(environment.getRequiredProperty(PASSWORD));

        Properties mailSenderProps = mailSender.getJavaMailProperties();
        Properties additionalProps = PropsUtils.getProperties(environment, PROPS_PREFIX,
                HOST, PORT, USERNAME, PASSWORD); // ignored
        mailSenderProps.putAll(additionalProps);

        return mailSender;
    }

    @Bean
    public ITemplateEngine emailTemplateEngine(MessageSource messageSource) {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.addTemplateResolver(htmlTemplateResolver());
        templateEngine.setTemplateEngineMessageSource(messageSource);
        return templateEngine;
    }

    private ITemplateResolver htmlTemplateResolver() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix(HTML_RESOLVER_PREFIX);
        templateResolver.setSuffix(HTML_RESOLVER_SUFFIX);
        templateResolver.setTemplateMode(TemplateMode.HTML);
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        templateResolver.setCacheable(false);
        return templateResolver;
    }
}
