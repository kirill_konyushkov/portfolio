package demo.konyushkov.email.config;

import demo.konyushkov.rabbit.config.AbstractRabbitMqConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({EmailServiceQueueConfig.class})
public class RabbitMqConfig extends AbstractRabbitMqConfig {}
