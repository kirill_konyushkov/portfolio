package demo.konyushkov.email;

import demo.konyushkov.base.ApplicationStarter;
import demo.konyushkov.email.config.AppConfig;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        new ApplicationStarter().start(AppConfig.class);
    }

}
