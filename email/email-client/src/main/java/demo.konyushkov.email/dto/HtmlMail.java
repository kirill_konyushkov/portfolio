package demo.konyushkov.email.dto;

import lombok.*;

import java.util.Map;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HtmlMail {
    private String to;
    private String subject;
    private String template;
    private Map<String,Object> variables;
}
