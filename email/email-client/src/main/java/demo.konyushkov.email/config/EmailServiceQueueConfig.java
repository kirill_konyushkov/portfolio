package demo.konyushkov.email.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailServiceQueueConfig {

    @Bean
    public Queue sendHtmlMailQueue() {
        return new Queue("email.send.html_mail");
    }

    @Bean
    public Queue sendTextMailQueue() {
        return new Queue("email.send.text_mail");
    }

}
