package demo.konyushkov.email.service;

import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.dto.TextMail;

public interface EmailClient {

    void send(TextMail mail);
    void send(HtmlMail mail);

}
