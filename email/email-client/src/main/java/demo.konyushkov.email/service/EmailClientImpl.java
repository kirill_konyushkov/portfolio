package demo.konyushkov.email.service;

import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.dto.TextMail;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EmailClientImpl implements EmailClient {

    @Value("#{sendHtmlMailQueue.name}")
    private String SEND_HTML_MAIL;

    @Value("#{sendTextMailQueue.name}")
    private String SEND_TEXT_MAIL;

    private final AmqpTemplate amqpTemplate;

    @Autowired
    public EmailClientImpl(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    @Override
    public void send(HtmlMail mail) {
        amqpTemplate.convertAndSend(SEND_HTML_MAIL, mail);
    }

    @Override
    public void send(TextMail mail) {
        amqpTemplate.convertAndSend(SEND_TEXT_MAIL, mail);
    }
}
