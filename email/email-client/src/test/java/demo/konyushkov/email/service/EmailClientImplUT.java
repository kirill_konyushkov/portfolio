package demo.konyushkov.email.service;

import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.dto.TextMail;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.amqp.core.AmqpTemplate;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class EmailClientImplUT {

    @Mock AmqpTemplate amqpTemplate;
    private EmailClient emailClient;

    @BeforeEach
    void init() throws IllegalAccessException {
        emailClient = new EmailClientImpl(amqpTemplate);
        FieldUtils.writeField(emailClient, "SEND_HTML_MAIL", "email.send.html_mail", true);
        FieldUtils.writeField(emailClient, "SEND_TEXT_MAIL", "email.send.text_mail", true);
    }

    @Nested
    class Send_HtmlMail {

        @Test
        void When_ValidHtmlMail_Expect_ConvertAndSend() {
            AtomicBoolean send = new AtomicBoolean(false);
            HtmlMail mail = new HtmlMail();
            Mockito.doAnswer(i -> {
                send.set(true);
                return null;
            }).when(amqpTemplate).convertAndSend("email.send.html_mail", mail);

            emailClient.send(mail);
            assertTrue(send.get());
        }
    }

    @Nested
    class Send_TextMail {

        @Test
        void When_ValidTextMail_Expect_ConvertAndSend() {
            AtomicBoolean send = new AtomicBoolean(false);
            TextMail mail = new TextMail();
            Mockito.doAnswer(i -> {
                send.set(true);
                return null;
            }).when(amqpTemplate).convertAndSend("email.send.text_mail", mail);

            emailClient.send(mail);
            assertTrue(send.get());
        }
    }

}