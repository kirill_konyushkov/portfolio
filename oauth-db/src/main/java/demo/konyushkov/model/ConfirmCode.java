package demo.konyushkov.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "confirm_code")
public class ConfirmCode {

    @Id
    private UUID code;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull(message = "{ConfirmCode.action.NotNull}")
    private Action action;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    @NotNull(message = "{ConfirmCode.user.NotNull}")
    private User user;

    @Column(name = "user_id", nullable = false, insertable=false, updatable=false)
    private UUID userId;

    public enum Action {
        RESTORE_PASSWORD,
        REGISTRATION
    }
}
