package demo.konyushkov.model;

import com.fasterxml.jackson.annotation.JsonView;
import demo.konyushkov.base.validation.constraints.DateBetween;
import demo.konyushkov.base.validation.constraints.Email;
import demo.konyushkov.base.validation.constraints.PasswordHash;
import demo.konyushkov.json.views.UserViews;
import demo.konyushkov.validation.groups.UserGroups;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Entity
@Builder
@EqualsAndHashCode(of = "id")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "email", name = "user_email_ukey"))
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GenericGenerator(name = "id", strategy = "uuid2")
    private UUID id;

    /* Основная информация */
    @NotBlank(message = "{User.name.NotBlank}",
            groups = { Default.class, UserGroups.Personal.class })
    @Column(nullable = false)
    @JsonView(UserViews.Personal.class)
    private String name;
    @JsonView(UserViews.Personal.class)
    private String surname;
    @JsonView(UserViews.Personal.class)
    private String photo;
    @DateBetween(min = "1950-01-01", max = "2004-12-31",
            message = "{User.birthday.DateBetween}",
            groups = { Default.class, UserGroups.Personal.class })
    @JsonView(UserViews.Personal.class)
    private LocalDate birthday;
    @Column(columnDefinition = "clob")
    @JsonView(UserViews.Personal.class)
    private String note;
    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "smallint")
    @JsonView(UserViews.Personal.class)
    private Gender gender;

    /* Контактная информация */
    @Email
    @NotNull(message = "{User.email.NotNull}")
    @Column(nullable = false)
    @JsonView(UserViews.Contacts.class)
    private String email;

    @Column(name = "phone_number")
    @JsonView(UserViews.Contacts.class)
    private String phoneNumber;


    /* Безопасность */
    @PasswordHash
    @NotNull(message = "{User.password.NotNull}")
    @Column(nullable = false)
    @JsonView(UserViews.Security.class)
    private String password;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_code")
    @JsonView(UserViews.Security.class)
    private Role role;

    @JsonView(UserViews.Security.class)
    private Boolean enabled;
}

