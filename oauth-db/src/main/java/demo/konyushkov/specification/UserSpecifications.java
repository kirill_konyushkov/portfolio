package demo.konyushkov.specification;

import demo.konyushkov.model.Role;
import demo.konyushkov.model.User;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;

public class UserSpecifications {

    public static Specification<User> findByEmailFetchRole(String email) {
        return (Specification<User>) (root, criteriaQuery, criteriaBuilder) -> {
            Fetch<User, Role> roleJoin = root.fetch("role", JoinType.LEFT);
            roleJoin.fetch("authorities", JoinType.LEFT);
            return criteriaBuilder.equal(root.get("email"), email);
        };
    }

}
