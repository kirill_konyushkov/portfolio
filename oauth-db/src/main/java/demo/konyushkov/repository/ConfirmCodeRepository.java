package demo.konyushkov.repository;

import demo.konyushkov.model.ConfirmCode;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ConfirmCodeRepository  extends CrudRepository<ConfirmCode, UUID> {}
