package demo.konyushkov.json.views;

public abstract class UserViews {
    public interface Personal {}
    public interface Contacts {}
    public interface Security {}
    public interface PersonalAndContacts extends Personal, Contacts {}

}
