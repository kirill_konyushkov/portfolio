package demo.konyushkov.service;

import demo.konyushkov.base.exception.NotFoundException;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;
import demo.konyushkov.repository.ConfirmCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class ConfirmCodeServiceImpl implements ConfirmCodeService {

    private final ConfirmCodeRepository confirmCodeRepository;

    @Autowired
    public ConfirmCodeServiceImpl(ConfirmCodeRepository confirmCodeRepository) {
        this.confirmCodeRepository = confirmCodeRepository;
    }

    @Override
    public ConfirmCode findByCode(UUID code) {
        return confirmCodeRepository.findById(code)
                .orElseThrow(() -> new NotFoundException("{ConfirmCodeServiceImpl.findByCode.NotFound}"));
    }

    @Override
    @Transactional
    public ConfirmCode prepareAndSave(@NonNull User user, @NonNull ConfirmCode.Action action) {
        ConfirmCode confirmCode = ConfirmCode.builder()
                .code(UUID.randomUUID())
                .action(action)
                .user(user)
                .build();

        return confirmCodeRepository.save(confirmCode);
    }

    @Override
    @Transactional
    public void deleteByCode(UUID code) {
        confirmCodeRepository.deleteById(code);
    }
}
