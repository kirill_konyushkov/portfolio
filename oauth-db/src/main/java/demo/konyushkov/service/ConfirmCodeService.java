package demo.konyushkov.service;

import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;

import java.util.UUID;

public interface ConfirmCodeService {
    ConfirmCode findByCode(UUID code);
    ConfirmCode prepareAndSave(User user, ConfirmCode.Action action);
    void deleteByCode(UUID code);
}
