package demo.konyushkov.service;

import demo.konyushkov.model.User;

import java.util.UUID;


public interface UserService {
    User findById(UUID id);
    User findByEmail(String email);
    User save(User user);
    boolean existsByEmail(String email);
}
