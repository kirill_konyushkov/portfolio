package demo.konyushkov.model;

import demo.konyushkov.test.tools.ValidatorTest;
import demo.konyushkov.validation.groups.UserGroups;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.util.UUID;

class UserValidationUT {

    private static ValidatorTest<User> validator;
    private User user;

    @BeforeAll
    static void setUp() {
        validator = new ValidatorTest<>();
    }

    @BeforeEach
    void init() {
        user = buildValid();
    }

    @Nested
    class DefaultGroup {

        @Nested
        class Success {
            @Test
            void When_FullyValidProfile_Expect_ViolationsIsEmpty() {
                validator.successValidation(user);
            }

            @Test
            void When_PhoneNumberIsNull_Expect_ViolationsIsEmpty() {
                user.setPhoneNumber(null);
                validator.successValidation(user);
            }
        }

        @Nested
        class Failed {
            @AfterEach
            void test() {
                validator.failedValidation(user);
            }

            @Test
            void When_NameInvalidNotBlank_Expect_OneConstraintViolationWithNotBlankMessageTemplate() {
                user.setName(" ");
                validator.fillTestData("name", "{User.name.NotBlank}");
            }

            @Test
            void When_DateInvalidDateBetween_Expect_OneConstraintViolationWithDateBetweenMessageTemplate() {
                user.setBirthday(LocalDate.MIN);
                validator.fillTestData("birthday", "{User.birthday.DateBetween}");
            }

            @Test
            void When_EmailInvalidNotNull_Expect_OneConstraintViolationWithNotNullMessageTemplate() {
                user.setEmail(null);
                validator.fillTestData("email", "{User.email.NotNull}");
            }

            @Test
            void When_EmailInvalidEmail_Expect_OneConstraintViolationWithEmailMessageTemplate() {
                user.setEmail("invalid email");
                validator.fillTestData("email", "{demo.konyushkov.validation.constraints.Email}");
            }

            @Test
            void When_PasswordInvalidNotNull_Expect_OneConstraintViolationWithNotNullMessageTemplate() {
                user.setPassword(null);
                validator.fillTestData("password", "{User.password.NotNull}");
            }

            @Test
            void When_PasswordInvalidPassword_Expect_OneConstraintViolationWithPasswordMessageTemplate() {
                user.setPassword("invalid password hash");
                validator.fillTestData("password", "{demo.konyushkov.validation.constraints.PasswordHash}");
            }
        }
    }

    @Nested
    class PersonalGroup {

        @Nested
        class Success {
            @Test
            void When_FullyValidProfile_Expect_ViolationsIsEmpty() {
                validator.successValidation(user, UserGroups.Personal.class);
            }

            @Test
            void When_EmailInvalidNotNull_Expect_IgnoreEmailAndViolationsIsEmpty() {
                user.setEmail(null);
                validator.successValidation(user, UserGroups.Personal.class);
            }

            @Test
            void When_EmailInvalidEmail_Expect_IgnoreEmailAndViolationsIsEmpty() {
                user.setEmail("invalid email");
                validator.successValidation(user, UserGroups.Personal.class);
            }

            @Test
            void When_PasswordInvalidNotNull_Expect_IgnorePasswordAndViolationsIsEmpty() {
                user.setPassword(null);
                validator.successValidation(user, UserGroups.Personal.class);
            }

            @Test
            void When_PasswordInvalidPassword_Expect_IgnorePasswordAndViolationsIsEmpty() {
                user.setPassword("invalid password hash");
                validator.successValidation(user, UserGroups.Personal.class);
            }

            @Test
            void When_PhoneNumberIsNull_Expect_ViolationsIsEmpty() {
                user.setPhoneNumber(null);
                validator.successValidation(user);
            }
        }

        @Nested
        class Failed {
            @AfterEach
            void test() {
                validator.failedValidation(user);
            }

            @Test
            void When_NameInvalidNotBlank_Expect_OneConstraintViolationWithNotBlankMessageTemplate() {
                user.setName(" ");
                validator.fillTestData("name", "{User.name.NotBlank}");
            }

            @Test
            void When_DateInvalidDateBetween_Expect_OneConstraintViolationWithDateBetweenMessageTemplate() {
                user.setBirthday(LocalDate.MIN);
                validator.fillTestData("birthday", "{User.birthday.DateBetween}");
            }
        }
    }

    static User buildValid() {
        final UUID VALID_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");
        final String VALID_PASSWORD_HASH = "$2a$04$KXKrdHLqqWXex11KGXqHreJRD3jPIHF1y07s.f8aVc0qo/nj5y8Gi";
        String VALID_EMAIL = "demo_test-user@mail.ru";

        User profile = new User();
        profile.setId(VALID_ID);
        profile.setName("Кирилл");
        profile.setSurname("Конюшков");
        profile.setPhoneNumber("8 (901) 189-12-38");
        profile.setPhoto("/img/photo.jpg");
        profile.setEmail(VALID_EMAIL);
        profile.setPassword(VALID_PASSWORD_HASH);
        profile.setGender(Gender.MALE);
        profile.setNote("note");
        profile.setBirthday(LocalDate.parse("1996-01-27"));
        profile.setEnabled(true);
        return profile;
    }
}
