package demo.konyushkov.model;

import demo.konyushkov.test.tools.ValidatorTest;
import org.junit.jupiter.api.*;

import java.util.UUID;

class ConfirmCodeValidationUT {

    private static ValidatorTest<ConfirmCode> validator;
    private ConfirmCode confirmCode;

    @BeforeAll
    static void setUp() {
        validator = new ValidatorTest<>();
    }

    @BeforeEach
    void init() {
        confirmCode = getValid();
    }

    @Nested
    class DefaultGroup {

        @Nested
        class Success {
            @Test
            void When_FullyValid_Expect_ViolationsIsEmpty() {
                validator.successValidation(confirmCode);
            }

            @Test
            void When_UserIdIsNull_Expect_ViolationsIsEmpty() {
                confirmCode.setUserId(null);
                validator.successValidation(confirmCode);
            }
        }

        @Nested
        class Failed {
            @AfterEach
            void test() {
                validator.failedValidation(confirmCode);
            }

            @Test
            void When_ActionIsNull_Expect_OneConstraintViolationWithActionNotNullMessageTemplate() {
                confirmCode.setAction(null);
                validator.fillTestData("action", "{ConfirmCode.action.NotNull}");
            }

            @Test
            void When_UserIsNull_Expect_OneConstraintViolationWithUserIdNotNullMessageTemplate() {
                confirmCode.setUser(null);
                validator.fillTestData("user", "{ConfirmCode.user.NotNull}");
            }
        }
    }

    private ConfirmCode getValid() {
        final UUID CODE = UUID.fromString("5722a55d-3eb7-421e-84e9-2eb1ce70cce9");
        final UUID USER_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");

        ConfirmCode confirmCode = new ConfirmCode();
        confirmCode.setCode(CODE);
        confirmCode.setAction(ConfirmCode.Action.RESTORE_PASSWORD);

        User user = new User();
        user.setId(USER_ID);

        confirmCode.setUser(user);
        confirmCode.setUserId(user.getId());

        return confirmCode;
    }
}
