package demo.konyushkov.model;

import demo.konyushkov.json.views.UserViews;
import demo.konyushkov.test.tools.JsonTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.UUID;

class UserSerializationUT {

    private static JsonTest<User> jsonTest;
    private User user;

    @BeforeAll
    static void setUp() {
        jsonTest = new JsonTest<>();
    }

    @BeforeEach
    void init() {
        user = buildValid();
    }

    @Test
    void PersonalView() {
        jsonTest.testSerialization(user, UserViews.Personal.class,
                "id", "name", "surname", "photo", "birthday", "note", "gender");
    }

    @Test
    void ContactsView() {
        jsonTest.testSerialization(user, UserViews.Contacts.class,
                "id", "email", "phoneNumber");
    }

    @Test
    void SecurityView() {
        jsonTest.testSerialization(user, UserViews.Security.class,
                "id", "role", "password", "enabled");
    }

    static User buildValid() {
        final UUID VALID_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");
        final String VALID_PASSWORD_HASH = "$2a$04$KXKrdHLqqWXex11KGXqHreJRD3jPIHF1y07s.f8aVc0qo/nj5y8Gi";
        String VALID_EMAIL = "demo_test-user@mail.ru";

        User profile = new User();
        profile.setId(VALID_ID);
        profile.setName("Кирилл");
        profile.setSurname("Конюшков");
        profile.setPhoto("/img/photo.jpg");
        profile.setEmail(VALID_EMAIL);
        profile.setPassword(VALID_PASSWORD_HASH);
        profile.setGender(Gender.MALE);
        profile.setNote("note");
        profile.setBirthday(LocalDate.parse("1996-01-27"));
        profile.setEnabled(true);
        return profile;
    }
}
