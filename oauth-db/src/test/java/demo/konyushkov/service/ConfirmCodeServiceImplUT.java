package demo.konyushkov.service;

import demo.konyushkov.base.exception.NotFoundException;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;
import demo.konyushkov.repository.ConfirmCodeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.transaction.TransactionSystemException;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.any;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ConfirmCodeServiceImplUT {

    @Mock private ConfirmCodeRepository confirmCodeRepository;
    private ConfirmCodeService confirmCodeService;

    @BeforeEach
    void init() {
        confirmCodeService = new ConfirmCodeServiceImpl(confirmCodeRepository);
    }


    @Nested
    class FindByCode {

        @Test
        void When_ConfirmCodeExist_Expect_ReturnConfirmCode() {
            ConfirmCode confirmCode = getValid();

            Mockito.when(confirmCodeRepository
                    .findById(confirmCode.getCode()))
                    .thenReturn(Optional.of(confirmCode));

            ConfirmCode code = confirmCodeService.findByCode(confirmCode.getCode());
            assertEquals(confirmCode, code);
        }

        @Test
        void When_ConfirmCodeNotExist_Expect_ThrowNotFoundException() {
            ConfirmCode confirmCode = getValid();
            Mockito.when(confirmCodeRepository.findById(confirmCode.getCode()))
                    .thenReturn(Optional.empty());

            NotFoundException exception = assertThrows(NotFoundException.class,
                    () -> confirmCodeService.findByCode(confirmCode.getCode()));
            assertEquals("{ConfirmCodeServiceImpl.findByCode.NotFound}", exception.getMessage());
        }

        @Test
        void When_CodeIsNull_Expect_ThrowNotFoundException() {
            Mockito.when(confirmCodeRepository.findById(null))
                    .thenReturn(Optional.empty());

            NotFoundException exception = assertThrows(NotFoundException.class,
                    () -> confirmCodeService.findByCode(null));
            assertEquals("{ConfirmCodeServiceImpl.findByCode.NotFound}", exception.getMessage());
        }
    }

    @Nested
    class PrepareAndSave {

        @Test
        void When_ValidUserAndAction_Expect_SaveAndReturnConfirm() {
            Mockito.when(confirmCodeRepository.save(any(ConfirmCode.class))).thenAnswer(returnsFirstArg());

            User user = new User();
            user.setId(UUID.randomUUID());

            ConfirmCode confirmCode = confirmCodeService.prepareAndSave(user, ConfirmCode.Action.RESTORE_PASSWORD);
            assertEquals(user, confirmCode.getUser());
            assertEquals(ConfirmCode.Action.RESTORE_PASSWORD, confirmCode.getAction());
            assertNotNull(confirmCode.getCode());
        }

        @Test
        void When_FailedSaveToDb_Expect_ThrowTransactionSystemException() {
            String tseMessage = "Failed save to db";
            Mockito.when(confirmCodeRepository.save(any(ConfirmCode.class))).thenThrow(new TransactionSystemException(tseMessage));

            User user = new User();
            user.setId(UUID.randomUUID());

            TransactionSystemException ex = assertThrows(TransactionSystemException.class,
                    () -> confirmCodeService.prepareAndSave(user, ConfirmCode.Action.RESTORE_PASSWORD));
            assertEquals(tseMessage, ex.getMessage());
        }
    }

    @Nested
    class DeleteByCode {

        @Test
        void When_Success_Expect_DeleteFromDb() {
            ConfirmCode confirmCode = getValid();
            AtomicBoolean deleted = new AtomicBoolean(false);

            Mockito.doAnswer((i) -> {
                deleted.set(true);
                return null;
            }).when(confirmCodeRepository).deleteById(confirmCode.getCode());

            confirmCodeService.deleteByCode(confirmCode.getCode());
            assertTrue(deleted.get());
        }

        @Test
        void When_Failed_Expect_ThrowTransactionSystemException() {
            String tseMessage = "Failed delete from db";
            Mockito.doThrow(new TransactionSystemException(tseMessage))
                .when(confirmCodeRepository).deleteById(any(UUID.class));

            ConfirmCode confirmCode = getValid();

            TransactionSystemException ex = assertThrows(TransactionSystemException.class,
                    () -> confirmCodeService.deleteByCode(confirmCode.getCode()));
            assertEquals(tseMessage, ex.getMessage());
        }
    }

    private ConfirmCode getValid() {
        final UUID CODE = UUID.fromString("5722a55d-3eb7-421e-84e9-2eb1ce70cce9");
        final UUID USER_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");

        ConfirmCode confirmCode = new ConfirmCode();
        confirmCode.setCode(CODE);
        confirmCode.setAction(ConfirmCode.Action.RESTORE_PASSWORD);

        User user = new User();
        user.setId(USER_ID);

        confirmCode.setUser(user);
        confirmCode.setUserId(user.getId());

        return confirmCode;
    }

}
