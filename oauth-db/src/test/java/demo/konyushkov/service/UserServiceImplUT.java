package demo.konyushkov.service;

import demo.konyushkov.base.exception.NotFoundException;
import demo.konyushkov.model.User;
import demo.konyushkov.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.transaction.TransactionSystemException;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserServiceImplUT {

     @Mock private UserRepository userRepository;
    private UserService userService;

    private static final UUID ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");
    private static final String EMAIL = "demo_test-user@mail.ru";
    private static final String PASSWORD_HASH = "$2a$04$KXKrdHLqqWXex11KGXqHreJRD3jPIHF1y07s.f8aVc0qo/nj5y8Gi";

    private User user;

    @BeforeEach
    void init() {
        this.userService = new UserServiceImpl(userRepository);
        user = new User();
        user.setId(ID);
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD_HASH);
    }

    @Nested
    class FindByEmail {

        @Test
        void When_UserExist_Expect_ReturnUser() {
            Mockito.when(userRepository.findByEmail(EMAIL)).thenReturn(Optional.of(user));
            User account = userService.findByEmail(EMAIL);
            assertEquals(user, account);
        }

        @Test
        void When_UserNotExist_Expect_ThrowNotFoundException() {
            Mockito.when(userRepository.findByEmail(EMAIL)).thenReturn(Optional.empty());

            NotFoundException exception = assertThrows(NotFoundException.class, () -> userService.findByEmail(EMAIL));
            assertEquals("{UserServiceImpl.findByEmail.NotFound}", exception.getMessage());
        }

        @Test
        void When_EmailIsNull_Expect_ThrowNotFoundException() {
            Mockito.when(userRepository.findByEmail(null)).thenReturn(Optional.empty());

            NotFoundException exception = assertThrows(NotFoundException.class, () -> userService.findByEmail(null));
            assertEquals("{UserServiceImpl.findByEmail.NotFound}", exception.getMessage());

            assertThrows(NotFoundException.class, () -> userService.findByEmail(null));
        }
    }

    @Nested
    class FindById {

        @Test
        void When_UserExist_Expect_ReturnUser() {
            Mockito.when(userRepository.findById(ID)).thenReturn(Optional.of(user));
            User account = userService.findById(ID);
            assertEquals(user, account);
        }

        @Test
        void When_UserNotExist_Expect_ThrowNotFoundException() {
            Mockito.when(userRepository.findById(ID)).thenReturn(Optional.empty());

            NotFoundException exception = assertThrows(NotFoundException.class, () -> userService.findById(ID));
            assertEquals("{UserServiceImpl.findById.NotFound}", exception.getMessage());
        }

        @Test
        void When_EmailIsNull_Expect_ThrowNotFoundException() {
            Mockito.when(userRepository.findById(null)).thenReturn(Optional.empty());

            NotFoundException exception = assertThrows(NotFoundException.class, () -> userService.findById(null));
            assertEquals("{UserServiceImpl.findById.NotFound}", exception.getMessage());

            assertThrows(NotFoundException.class, () -> userService.findById(null));
        }
    }

    @Nested
    class Save {

        @Test
        void When_ValidUser_Expect_SaveAndReturnUser() {
            Mockito.when(userRepository.save(user)).thenReturn(user);
            User save = userService.save(user);
            assertEquals(user, save);
        }

        @Test
        void When_FailedSaveToDb_Expect_ThrowTransactionSystemException() {
            String exMessage = "Failed save to db";
            Mockito.when(userRepository.save(any(User.class))).thenThrow(new TransactionSystemException(exMessage));

            TransactionSystemException ex = assertThrows(TransactionSystemException.class,
                    () -> userService.save(user));
            assertEquals(exMessage, ex.getMessage());
        }
    }

    @Nested
    class ExistByEmail {

        @Test
        void When_UserExist_Expect_Return_True() {
            Mockito.when(userRepository.existsByEmail(EMAIL)).thenReturn(true);
            assertTrue(userService.existsByEmail(EMAIL));
        }

        @Test
        void When_UserNotExist_Expect_Return_False() {
            Mockito.when(userRepository.existsByEmail(EMAIL)).thenReturn(false);
            assertFalse(userService.existsByEmail(EMAIL));
        }
    }
}