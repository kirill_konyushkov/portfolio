package demo.konyushkov.repository;

import com.vladmihalcea.sql.SQLStatementCountValidator;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;
import demo.konyushkov.test.config.RepositoryContextIT;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;

import javax.validation.ConstraintViolationException;
import java.util.Optional;
import java.util.UUID;

import static com.vladmihalcea.sql.SQLStatementCountValidator.*;
import static org.junit.jupiter.api.Assertions.*;

class ConfirmCodeRepositoryIT {

    @Nested
    class FindById extends RepositoryContextIT {

        @Autowired
        ConfirmCodeRepository confirmCodeRepository;

        @Test
        void When_ConfirmCodeExistInDb_Expect_ReturnOptionalWithConfirmCode() {
            SQLStatementCountValidator.reset();
            Optional<ConfirmCode> optional = confirmCodeRepository.findById(Constants.CODE);
            assertSelectCount(1);

            assertTrue(optional.isPresent());
            ConfirmCode confirmCode = optional.get();

            assertEquals(Constants.CODE, confirmCode.getCode());
            assertEquals(Constants.USER_ID, confirmCode.getUserId());
            assertEquals(ConfirmCode.Action.RESTORE_PASSWORD, confirmCode.getAction());
        }
    }

    @Nested
    class Save extends RepositoryContextIT {
        @Autowired
        ConfirmCodeRepository confirmCodeRepository;

        @Test
        void When_ValidConfirmCode_Expect_SaveConfirmCodeAndReturn() {
            SQLStatementCountValidator.reset();

            UUID code = UUID.randomUUID();
            User user = new User();
            user.setId(Constants.USER_ID);
            ConfirmCode newCode = ConfirmCode.builder()
                    .code(code)
                    .action(ConfirmCode.Action.RESTORE_PASSWORD)
                    .user(user)
                    .build();

            confirmCodeRepository.save(newCode);

            assertInsertCount(1);

            Optional<ConfirmCode> optional = confirmCodeRepository.findById(code);
            assertTrue(optional.isPresent());
            ConfirmCode existCode = optional.get();

            assertEquals(code, existCode.getCode());
            assertEquals(Constants.USER_ID, existCode.getUserId());
            assertEquals(ConfirmCode.Action.RESTORE_PASSWORD, existCode.getAction());
        }

        @Test
        void When_InvalidConfirmCode_Expect_SaveConfirmCodeAndReturn() {
            SQLStatementCountValidator.reset();

            UUID code = UUID.randomUUID();
            ConfirmCode newCode = ConfirmCode.builder()
                    .code(code)
                    .action(null)
                    .user(null)
                    .build();

            TransactionSystemException tse = assertThrows(TransactionSystemException.class, () -> confirmCodeRepository.save(newCode));
            assertNotNull(tse.getRootCause());
            assertEquals(ConstraintViolationException.class, tse.getRootCause().getClass());
        }
    }

    @Nested
    class DeleteById extends RepositoryContextIT {

        @Autowired
        ConfirmCodeRepository confirmCodeRepository;

        @Test
        void When_ConfirmCodeExistInDb_Expect_DeleteFromDb() {
            SQLStatementCountValidator.reset();
            ConfirmCode confirmCode = new ConfirmCode();
            confirmCode.setCode(Constants.CODE);
            confirmCodeRepository.deleteById(confirmCode.getCode());
            assertDeleteCount(1);

            Optional<ConfirmCode> optional = confirmCodeRepository.findById(confirmCode.getCode());
            assertFalse(optional.isPresent());
        }
    }

    static class Constants {
        static final UUID CODE = UUID.fromString("5722a55d-3eb7-421e-84e9-2eb1ce70cce9");
        static final UUID USER_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");
    }
}
