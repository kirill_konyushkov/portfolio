package demo.konyushkov.repository;

import com.vladmihalcea.sql.SQLStatementCountValidator;
import demo.konyushkov.model.AuthoritiesEnum;
import demo.konyushkov.model.Role;
import demo.konyushkov.model.User;
import demo.konyushkov.specification.UserSpecifications;
import demo.konyushkov.test.config.RepositoryContextIT;
import org.hibernate.LazyInitializationException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static com.vladmihalcea.sql.SQLStatementCountValidator.assertSelectCount;
import static org.junit.jupiter.api.Assertions.*;

class UserRepositoryIT {

    @Nested
    class FindById extends RepositoryContextIT {

        @Autowired
        UserRepository userRepository;

        @Test
        void When_UserExistInDb_Expect_ReturnOptionalWithUser() {
            SQLStatementCountValidator.reset();
            Optional<User> optional = userRepository.findById(Constants.VALID_ID);
            assertSelectCount(1);

            assertTrue(optional.isPresent());
            User user = optional.get();

            assertEquals(Constants.VALID_EMAIL, user.getEmail());
            assertEquals(Constants.VALID_ID, user.getId());
            assertEquals(Constants.ROLE_ADMIN, user.getRole().getCode());
            assertThrows(LazyInitializationException.class, () -> user.getRole().getAuthorities().forEach(System.out::println));
        }
    }


    @Nested
    class FindByEmail extends RepositoryContextIT {

        @Autowired
        UserRepository userRepository;

        @Test
        void When_UserExistInDb_Expect_ReturnOptionalWithUser() {
            SQLStatementCountValidator.reset();
            Optional<User> optional = userRepository.findByEmail(Constants.VALID_EMAIL);
            assertSelectCount(1);

            assertTrue(optional.isPresent());
            User user = optional.get();

            assertEquals(Constants.VALID_EMAIL, user.getEmail());
            assertEquals(Constants.VALID_ID, user.getId());
            assertEquals(Constants.ROLE_ADMIN, user.getRole().getCode());
            assertThrows(LazyInitializationException.class,  () -> user.getRole().getAuthorities().forEach(System.out::println));
        }
    }

    @Nested
    class FindOne extends RepositoryContextIT {

        @Autowired
        UserRepository userRepository;

        @Test
        void When_UseUserSpecificationsFindByEmailFetchRole_Expect_ReturnUserJoinRoleAndAuthorities() {
            SQLStatementCountValidator.reset();
            Optional<User> optional = userRepository.findOne(UserSpecifications.findByEmailFetchRole(Constants.VALID_EMAIL));
            assertTrue(optional.isPresent());
            assertSelectCount(1);

            User user = optional.get();
            Role role = user.getRole();
            assertEquals(Constants.ROLE_ADMIN, role.getCode());
            assertEquals(role.getAuthorities(), Set.of(AuthoritiesEnum.WRITE, AuthoritiesEnum.READ));
        }
    }

    @Nested
    class ExistByEmail extends RepositoryContextIT {

        @Autowired
        UserRepository userRepository;

        @Test
        void When_UserExist_Expect_Return_True() {
            boolean exist = userRepository.existsByEmail(Constants.VALID_EMAIL);
            assertTrue(exist);
        }

        @Test
        void When_UserNotExist_Expect_Return_False() {
            boolean exist = userRepository.existsByEmail(Constants.MISSING_EMAIL);
            assertFalse(exist);
        }
    }

    static class Constants {
        // ID
        static final UUID VALID_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");

        // EMAIL
        static final String VALID_EMAIL = "demo_test-user@mail.ru";
        static final String MISSING_EMAIL = "missing@email.ru";

        // ROLE
        static final String ROLE_ADMIN = "ROLE_ADMIN";
    }

}
