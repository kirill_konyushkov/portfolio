CREATE TABLE role
(
    code VARCHAR(255) NOT NULL
        CONSTRAINT role_pkey
        PRIMARY KEY,
    description VARCHAR(255)
);

CREATE TABLE authority
(
    name VARCHAR(255) NOT NULL
        CONSTRAINT authority_pkey
        PRIMARY KEY
);

CREATE TABLE roles_authorities
(
    authority_name VARCHAR(255) NOT NULL
        CONSTRAINT roles_authorities_authority_name_fkey
        REFERENCES authority,
    role_code      VARCHAR(255)         NOT NULL
        CONSTRAINT roles_authorities_role_code_fkey
        REFERENCES role
);

CREATE TABLE user
(
    id         UUID NOT NULL
        CONSTRAINT user_pkey
        PRIMARY KEY,
    email        VARCHAR(255)
        CONSTRAINT user_email_ukey
        UNIQUE,
    name         VARCHAR(255),
    password     VARCHAR(255),
    photo        VARCHAR(255),
    birthday     DATE,
    surname      VARCHAR(255),
    phone_number VARCHAR(255),
    note         TEXT,
    gender       SMALLINT,
    role_code    VARCHAR(255)
          CONSTRAINT user_role_code_fkey
          REFERENCES role,
    enabled      BOOLEAN,
);

CREATE TABLE confirm_code
(
    code         UUID NOT NULL
        CONSTRAINT confirm_code_pkey
        PRIMARY KEY,
    action         VARCHAR(255) NOT NULL,
    user_id    UUID NOT NULL
          CONSTRAINT confirm_code_user_id_fkey
          REFERENCES user
);
