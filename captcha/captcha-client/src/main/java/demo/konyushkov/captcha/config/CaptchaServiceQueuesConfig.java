package demo.konyushkov.captcha.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CaptchaServiceQueuesConfig {

    @Bean
    public Queue captchaValidateQueue() {
        return new Queue("captcha.validate");
    }

}
