package demo.konyushkov.captcha.utils;

import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.web.utils.HttpUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public abstract class CaptchaUtils {

    public static CaptchaRequest buildCaptchaRequest(HttpServletRequest request) {
        String ip = HttpUtils.getClientIP(request);
        String response = getCaptchaResponse(request);

        return new CaptchaRequest(ip, response);
    }

    public static String getCaptchaResponse(HttpServletRequest request) {
        return Optional.ofNullable(request)
                .map((req) -> req.getParameter("g-recaptcha-response"))
                .orElse(null);
    }
}
