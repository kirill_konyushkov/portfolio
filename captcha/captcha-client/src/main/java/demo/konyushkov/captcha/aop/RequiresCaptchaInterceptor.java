package demo.konyushkov.captcha.aop;

import demo.konyushkov.base.exception.ForbiddenException;
import demo.konyushkov.base.utils.Precondition;
import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.service.CaptchaClient;
import demo.konyushkov.captcha.utils.CaptchaUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequiresCaptchaInterceptor extends HandlerInterceptorAdapter {

    private final CaptchaClient captchaClient;

    public RequiresCaptchaInterceptor(CaptchaClient captchaClient) {
        this.captchaClient = captchaClient;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (HandlerMethod.class.isAssignableFrom(handler.getClass())) {
            validateCaptchaBeforeMethodCall(request, (HandlerMethod)handler);
        }

        return true;
    }

    private void validateCaptchaBeforeMethodCall(HttpServletRequest request, HandlerMethod handlerMethod) {
        if (handlerMethod.getMethodAnnotation(RequiresCaptcha.class) != null) {
            CaptchaRequest captchaRequest = CaptchaUtils.buildCaptchaRequest(request);
            CaptchaResponse captchaResponse = captchaClient.validate(captchaRequest);
            Precondition.checkArgument(captchaResponse.getSuccess(), ForbiddenException.class);
        }
    }
}
