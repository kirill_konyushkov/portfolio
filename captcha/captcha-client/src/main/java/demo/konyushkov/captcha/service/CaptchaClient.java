package demo.konyushkov.captcha.service;

import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;

public interface CaptchaClient {
    CaptchaResponse validate(CaptchaRequest request);
}
