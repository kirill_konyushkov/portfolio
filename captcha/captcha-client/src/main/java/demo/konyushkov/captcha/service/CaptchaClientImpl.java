package demo.konyushkov.captcha.service;

import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CaptchaClientImpl implements CaptchaClient {

    @Value("#{captchaValidateQueue.name}")
    private String CAPTCHA_VALIDATE;

    private final AmqpTemplate amqpTemplate;

    @Autowired
    public CaptchaClientImpl(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    @Override
    public CaptchaResponse validate(CaptchaRequest request) {
        return (CaptchaResponse) amqpTemplate.convertSendAndReceive(CAPTCHA_VALIDATE, request);
    }
}
