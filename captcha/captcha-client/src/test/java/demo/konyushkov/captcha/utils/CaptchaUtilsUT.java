package demo.konyushkov.captcha.utils;

import demo.konyushkov.captcha.dto.CaptchaRequest;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CaptchaUtilsUT {

    @Mock private HttpServletRequest request;

    private final static String VALID_RESPONSE = "validResponse";
    private final static String IP = "192.168.1.0";

    @Nested
    class GetRecaptchaResponse {

        @Test
        void When_RequestHasGRecaptchaResponseParameter_Expect_ReturnGRecaptchaResponseParameter() {
            Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(VALID_RESPONSE);
            assertEquals(VALID_RESPONSE, CaptchaUtils.getCaptchaResponse(request));
        }

        @Test
        void When_RequestNotHasGRecaptchaResponseParameter_Expect_ReturnNull() {
            Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(null);
            assertNull(CaptchaUtils.getCaptchaResponse(request));
        }

        @Test
        void When_RequestIsNull_Expect_ReturnNull() {
            assertNull(CaptchaUtils.getCaptchaResponse(null));
        }
    }

    @Nested
    class BuildCaptchaRequest {

        @Test
        void When_HttpServletRequestHasClientIpAndRecaptchaResponse_Expect_ReturnCaptchaRequest() {
            Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(VALID_RESPONSE);
            Mockito.when(request.getRemoteAddr()).thenReturn(IP);

            CaptchaRequest captchaRequest = CaptchaUtils.buildCaptchaRequest(request);
            assertEquals(VALID_RESPONSE, captchaRequest.getResponse());
            assertEquals(IP, captchaRequest.getIp());
        }

        @Test
        void When_HttpServletRequestHasClientIpButRecaptchaResponseIsNull_Expect_ReturnCaptchaRequest() {
            Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(null);
            Mockito.when(request.getRemoteAddr()).thenReturn(IP);

            CaptchaRequest captchaRequest = CaptchaUtils.buildCaptchaRequest(request);
            assertNull(captchaRequest.getResponse());
            assertEquals(IP, captchaRequest.getIp());
        }

        @Test
        void When_HttpServletHasRequestRecaptchaResponseButClientIpIsNull_Expect_ReturnCaptchaRequest() {
            Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(VALID_RESPONSE);
            Mockito.when(request.getRemoteAddr()).thenReturn(null);

            CaptchaRequest captchaRequest = CaptchaUtils.buildCaptchaRequest(request);
            assertEquals(VALID_RESPONSE, captchaRequest.getResponse());
            assertNull(captchaRequest.getIp());
        }

        @Test
        void When_RecaptchaResponseIsNullAndClientIpIsNull_Expect_ReturnCaptchaRequest() {
            Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(null);
            Mockito.when(request.getRemoteAddr()).thenReturn(null);

            CaptchaRequest captchaRequest = CaptchaUtils.buildCaptchaRequest(request);
            assertNull(captchaRequest.getResponse());
            assertNull(captchaRequest.getIp());
        }
    }

}
