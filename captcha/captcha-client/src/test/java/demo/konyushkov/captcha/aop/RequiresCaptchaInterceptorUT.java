package demo.konyushkov.captcha.aop;

import demo.konyushkov.base.exception.ForbiddenException;
import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.service.CaptchaClient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class RequiresCaptchaInterceptorUT {

    @Mock private HttpServletRequest request;
    @Mock private CaptchaClient captchaClient;
    @Mock private HttpServletResponse response;
    private RequiresCaptchaInterceptor interceptor;
    private HandlerMethod handlerMethod;

    private static final String IP = "192.168.0.1";
    private static final String VALID_RESPONSE = "validResponse";


    @BeforeEach
    void init() throws NoSuchMethodException {
        interceptor = new RequiresCaptchaInterceptor(captchaClient);
        Mockito.when(request.getParameter("g-recaptcha-response")).thenReturn(VALID_RESPONSE);
        Mockito.when(request.getRemoteAddr()).thenReturn(IP);

        Method method = TestController.class.getDeclaredMethod("testMethod");
        TestController controller = new TestController();
        handlerMethod = new HandlerMethod(controller, method);
    }

    @Nested
    class ValidateCaptchaBeforeMethodCall {

        @Test
        void When_MethodWithRequiresCaptchaAndCaptchaValid_Expect_ReturnMethodResponse() {
            CaptchaResponse successResponse = new CaptchaResponse(true);
            successResponse.setSuccess(true);

            AtomicBoolean validate = new AtomicBoolean(false);

            Mockito.when(captchaClient.validate((any(CaptchaRequest.class)))).thenAnswer(i -> {
                validate.set(true);
                return successResponse;
            });

            boolean result = interceptor.preHandle(request, response, handlerMethod);

            // Test validation
            assertTrue(result);
            assertTrue(validate.get());

        }

        @Test
        void When_MethodWithRequiresCaptchaAndCaptchaInvalid_Expect_ThrowRequestException() {
            CaptchaResponse failedResponse = new CaptchaResponse(false);

            AtomicBoolean validate = new AtomicBoolean(false);

            Mockito.when(captchaClient.validate((any(CaptchaRequest.class)))).thenAnswer(i -> {
                validate.set(true);
                return failedResponse;
            });

            // Test validation
            assertThrows(ForbiddenException.class, () -> interceptor.preHandle(request, response, handlerMethod));
            assertTrue(validate.get());
        }

        @Test
        void When_MethodWithRequiresCaptchaAndCaptchaClientThrowException_Expect_ThrowException() {
            Mockito.when(captchaClient.validate((any(CaptchaRequest.class)))).thenThrow(new ForbiddenException());

            // Test validation
            assertThrows(ForbiddenException.class, () -> interceptor.preHandle(request, response, handlerMethod));
        }
    }

    @NoArgsConstructor
    private static class TestController {

        @RequiresCaptcha
        ResponseEntity<RestMsg> testMethod() {
            return new ResponseEntity<>(new RestMsg("Mock msg"), HttpStatus.OK);
        }
    }

    @Data
    @EqualsAndHashCode
    @AllArgsConstructor
    private static class RestMsg {
        private String msg;
    }
}
