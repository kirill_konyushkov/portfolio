package demo.konyushkov.captcha.service;

import demo.konyushkov.base.exception.ForbiddenException;
import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.amqp.core.AmqpTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CaptchaClientImplUT {

    @Mock AmqpTemplate amqpTemplate;
    private CaptchaClient captchaClient;

    @BeforeEach
    void init() throws IllegalAccessException {
        captchaClient = new CaptchaClientImpl(amqpTemplate);
        FieldUtils.writeField(captchaClient, "CAPTCHA_VALIDATE", "captcha.validate", true);
    }

    @Nested
    class Validate {

        private static final String CAPTCHA_VALIDATE = "captcha.validate";

        @Test
        void When_ValidCaptchaRequest_Expect_ReturnCaptchaResponseWithSuccessTrue() {
            Mockito.when(amqpTemplate.convertSendAndReceive(eq(CAPTCHA_VALIDATE), any(CaptchaRequest.class))).thenReturn(new CaptchaResponse(true));
            CaptchaResponse validate = captchaClient.validate(new CaptchaRequest("ip", "response"));
            assertTrue(validate.getSuccess());
        }

        @Test
        void When_InvalidCaptchaRequest_Expect_ThrowForbiddenException() {
            String exMessage = "Failed validate captcha";
            Mockito.when(amqpTemplate.convertSendAndReceive(eq(CAPTCHA_VALIDATE), any(CaptchaRequest.class))).thenThrow(new ForbiddenException(exMessage));
            ForbiddenException exception = assertThrows(ForbiddenException.class, () -> captchaClient.validate(new CaptchaRequest("ip", "invalidResponse")));
            assertEquals(exMessage, exception.getMessage());
        }
    }

}