package demo.konyushkov.captcha.config;

import demo.konyushkov.rabbit.config.AbstractRabbitMqConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(CaptchaServiceQueuesConfig.class)
public class RabbitMqConfig extends AbstractRabbitMqConfig {}
