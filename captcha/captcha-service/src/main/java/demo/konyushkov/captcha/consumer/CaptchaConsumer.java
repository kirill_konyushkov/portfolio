package demo.konyushkov.captcha.consumer;

import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.service.CaptchaService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CaptchaConsumer {

    private final CaptchaService captchaService;

    @Autowired
    public CaptchaConsumer(CaptchaService captchaService) {
        this.captchaService = captchaService;
    }

    @RabbitListener(queues = "#{captchaValidateQueue.name}")
    public CaptchaResponse validate(CaptchaRequest request) {
        return captchaService.validate(request.getIp(), request.getResponse());
    }
}
