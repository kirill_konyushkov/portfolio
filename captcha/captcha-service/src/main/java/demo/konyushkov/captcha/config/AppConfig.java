package demo.konyushkov.captcha.config;

import demo.konyushkov.base.config.BaseConfig;
import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.base.utils.InMemoryAttemtStore;
import demo.konyushkov.captcha.dto.CaptchaSettings;
import demo.konyushkov.captcha.service.CaptchaService;
import demo.konyushkov.captcha.service.CaptchaServiceImpl;
import demo.konyushkov.captcha.utils.CaptchaValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

@Configuration
@Import({BaseConfig.class, RabbitMqConfig.class})
public class AppConfig {

    @Value("${google.recaptcha.key.site}")
    private String CAPTCHA_KEY;

    @Value("${google.recaptcha.key.secret}")
    private String CAPTCHA_SECRET;

    @Bean
    public AttemptStore attemptStore() {
        return new InMemoryAttemtStore(10, 1, TimeUnit.DAYS);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public CaptchaSettings captchaSettings() {
        return new CaptchaSettings(CAPTCHA_KEY, CAPTCHA_SECRET);
    }

    @Bean
    public CaptchaValidator captchaValidator() {
        return new CaptchaValidator(captchaSettings(), restTemplate());
    }

    @Bean
    public CaptchaService loginCaptchaService() {
        return new CaptchaServiceImpl(captchaValidator(), attemptStore());
    }
}
