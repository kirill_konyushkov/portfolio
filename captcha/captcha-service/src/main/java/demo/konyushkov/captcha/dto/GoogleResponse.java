package demo.konyushkov.captcha.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Data
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GoogleResponse {

    @JsonProperty("success")
    boolean success;

    @JsonProperty("challenge_ts")
    String challengeTs;

    @JsonProperty("hostname")
    String hostname;

    @JsonProperty("error-codes")
    ErrorCode[] errorCodes;

    @JsonIgnore
    public boolean hasClientError() {
        return hasAnyError(ErrorCode.InvalidResponse, ErrorCode.MissingResponse);
    }

    @JsonIgnore
    public boolean hasServerError() {
        return hasAnyError(ErrorCode.InvalidSecret, ErrorCode.MissingSecret);
    }

    @JsonIgnore
    private boolean hasAnyError(ErrorCode ...errorCodes) {
        if(getErrorCodes() == null) {
            return false;
        }

        return Arrays.stream(errorCodes).anyMatch(this::contains);
    }

    @JsonIgnore
    public boolean contains(ErrorCode error) {
        for(ErrorCode e: getErrorCodes()) {
            if(e.equals(error)) {
                return true;
            }
        }

        return false;
    }

    public enum ErrorCode {
        MissingSecret,
        InvalidSecret,
        MissingResponse,
        InvalidResponse;

        private static Map<String, ErrorCode> errorsMap = new HashMap<>(4);

        static {
            errorsMap.put("missing-input-secret",   MissingSecret);
            errorsMap.put("invalid-input-secret",   InvalidSecret);
            errorsMap.put("missing-input-response", MissingResponse);
            errorsMap.put("invalid-input-response", InvalidResponse);
        }

        @JsonCreator
        public static ErrorCode forValue(String value) {
            return errorsMap.get(value.toLowerCase());
        }
    }
}
