package demo.konyushkov.captcha.service;

import demo.konyushkov.base.exception.ForbiddenException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.dto.GoogleResponse;
import demo.konyushkov.captcha.utils.CaptchaValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;

@Slf4j
public class CaptchaServiceImpl implements CaptchaService {

    private static final String BLOCKED = "{CaptchaServiceImpl.validate.blocked}";
    private static final String INVALID_RESPONSE = "{CaptchaServiceImpl.validate.invalidResponse}";
    private static final String INTERNAL_ERROR = "{CaptchaServiceImpl.validate.internalError}";


    private final CaptchaValidator captchaValidator;
    private final AttemptStore attemptStore;

    public CaptchaServiceImpl(CaptchaValidator captchaValidator, AttemptStore attemptStore) {
        this.captchaValidator = captchaValidator;
        this.attemptStore = attemptStore;
    }

    @Override
    public CaptchaResponse validate(String ip, String response) {
        boolean blocked = attemptStore.isExceeded(ip);

        if(blocked) {
            log.info("Blocked by IP {}", ip);
            throw new ForbiddenException(BLOCKED);
        }

        try {
            GoogleResponse googleResponse = captchaValidator.validate(ip, response);

            if(googleResponse.isSuccess()) {
                attemptStore.invalidate(ip);
                return new CaptchaResponse(true);
            } else if(ArrayUtils.isEmpty(googleResponse.getErrorCodes())) {
                throw new InternalServerException(INTERNAL_ERROR);
            } else if (googleResponse.hasServerError()) {
                throw new InternalServerException(INTERNAL_ERROR);
            }
        }
        catch (NullPointerException ignore) {}

        attemptStore.increase(ip);
        throw new ForbiddenException(INVALID_RESPONSE);
    }


}
