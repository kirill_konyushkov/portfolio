package demo.konyushkov.captcha.utils;

import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.base.utils.Precondition;
import demo.konyushkov.captcha.dto.CaptchaSettings;
import demo.konyushkov.captcha.dto.GoogleResponse;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class CaptchaValidator {

    private static final String GOOGLE_RECAPTCHA_VERIFY_URL= "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s";

    private final CaptchaSettings captchaSettings;
    private final RestTemplate restTemplate;

    public CaptchaValidator(@NonNull CaptchaSettings captchaSettings,
                            @NonNull RestTemplate restTemplate) {
        this.captchaSettings = captchaSettings;
        this.restTemplate = restTemplate;
    }

    public GoogleResponse validate(@NonNull String ip, @NonNull String response) {
        final String uri = String.format(GOOGLE_RECAPTCHA_VERIFY_URL, // template
                captchaSettings.getSecret(), response, ip); // params

        try {
            GoogleResponse googleResponse = restTemplate.getForObject(uri, GoogleResponse.class);
            Precondition.checkNotNull(googleResponse, InternalServerException.class);
            return googleResponse;
        }
        catch (RestClientException ex) {
            throw new InternalServerException(ex);
        }
    }
}
