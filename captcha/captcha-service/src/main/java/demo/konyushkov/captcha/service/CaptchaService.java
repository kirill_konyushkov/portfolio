package demo.konyushkov.captcha.service;

import demo.konyushkov.captcha.dto.CaptchaResponse;

public interface CaptchaService {
    CaptchaResponse validate(String ip, String response);
}
