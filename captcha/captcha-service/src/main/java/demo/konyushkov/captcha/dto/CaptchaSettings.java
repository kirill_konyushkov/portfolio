package demo.konyushkov.captcha.dto;

import lombok.Value;

@Value
public class CaptchaSettings {
    String site;
    String secret;
}
