package demo.konyushkov.captcha;

import demo.konyushkov.base.ApplicationStarter;
import demo.konyushkov.captcha.config.AppConfig;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        new ApplicationStarter().start(AppConfig.class);
    }

}
