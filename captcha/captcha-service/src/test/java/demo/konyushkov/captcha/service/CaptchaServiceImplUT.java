package demo.konyushkov.captcha.service;

import demo.konyushkov.base.exception.ForbiddenException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.base.utils.AttemptStore;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.dto.GoogleResponse;
import demo.konyushkov.captcha.utils.CaptchaValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CaptchaServiceImplUT {


    private CaptchaService captchaService;
    @Mock private CaptchaValidator captchaValidator;
    @Mock private AttemptStore attemptStore;

    private final static String VALID_RESPONSE = "validResponse";
    private final static String IP = "192.168.1.0";
    private int failedAttempts = 0;

    @BeforeEach
    void init() {
        captchaService = new CaptchaServiceImpl(captchaValidator, attemptStore);
        failedAttempts = 0;
    }

    @Nested
    class Validate {

        @BeforeEach
        void init() {
            Mockito.when(attemptStore.isExceeded(IP)).thenReturn(false);
            Mockito.doAnswer((i) -> failedAttempts++).when(attemptStore).increase(IP);
            Mockito.doAnswer((i) -> failedAttempts = 0).when(attemptStore).invalidate(IP);
        }

        @Test
        void When_ClientIsNotBlockedAndCaptchaResponseIsSuccess_Expect_ReturnCaptchaResponseWithSuccessAndAttemptsInvalidate() {
            GoogleResponse successResponse = build(true);
            Mockito.when(captchaValidator.validate(IP, VALID_RESPONSE)).thenReturn(successResponse);

            attemptStore.increase(IP);
            assertEquals(1, failedAttempts);

            CaptchaResponse captchaResponse = captchaService.validate(IP, VALID_RESPONSE);
            assertTrue(captchaResponse.getSuccess());
            assertEquals(0, failedAttempts);
        }

        @Test
        void When_ClientIsBlocked_Expect_AttemptsNotIncrementAndThrowForbiddenExceptionWithBlockedMessage() {
            Mockito.when(attemptStore.isExceeded(IP)).thenReturn(true);

            ForbiddenException ex = assertThrows(ForbiddenException.class, () -> captchaService.validate(IP, VALID_RESPONSE));
            assertEquals("{CaptchaServiceImpl.validate.blocked}", ex.getMessage());
            assertEquals(0, failedAttempts);
        }

        @Test
        void When_ClientIsNotBlockedAndGoogleResponseSuccessFalseAndHasClientErrors_Expect_IncreaseAttemptsAndThrowForbiddenExceptionWithInvalidResponseMessage() {
            GoogleResponse failedResponse = build(false, GoogleResponse.ErrorCode.InvalidResponse);
            Mockito.when(captchaValidator.validate(IP, VALID_RESPONSE)).thenReturn(failedResponse);

            ForbiddenException ex = assertThrows(ForbiddenException.class, () -> captchaService.validate(IP, VALID_RESPONSE));
            assertEquals("{CaptchaServiceImpl.validate.invalidResponse}", ex.getMessage());
            assertEquals(1, failedAttempts);
        }

        @Test
        void When_ClientIsNotBlockedAndThrowNPE_Expect_IncreaseAttemptsAndThrowForbiddenExceptionWithInvalidResponseMessage() {
            Mockito.when(captchaValidator.validate(IP, VALID_RESPONSE)).thenThrow(new NullPointerException());

            ForbiddenException ex = assertThrows(ForbiddenException.class, () -> captchaService.validate(IP, VALID_RESPONSE));
            assertEquals("{CaptchaServiceImpl.validate.invalidResponse}", ex.getMessage());
            assertEquals(1, failedAttempts);
        }

        @Test
        void When_ClientIsNotBlockedAndGoogleResponseSuccessFalseAndHasServerErrors_Expect_NotIncreaseAttemptsAndThrowForbiddenExceptionWithInternalErrorMessage() {
            GoogleResponse failedResponse = build(false, GoogleResponse.ErrorCode.InvalidSecret);
            Mockito.when(captchaValidator.validate(IP, VALID_RESPONSE)).thenReturn(failedResponse);

            InternalServerException ex = assertThrows(InternalServerException.class, () -> captchaService.validate(IP, VALID_RESPONSE));
            assertEquals("{CaptchaServiceImpl.validate.internalError}", ex.getMessage());
            assertEquals(0, failedAttempts);
        }
    }

    private GoogleResponse build(boolean success, GoogleResponse.ErrorCode... errorCodes) {
        GoogleResponse googleResponse = new GoogleResponse();
        googleResponse.setSuccess(success);
        googleResponse.setErrorCodes(errorCodes);

        return googleResponse;
    }
}
