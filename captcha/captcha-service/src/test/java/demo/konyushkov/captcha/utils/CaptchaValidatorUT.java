package demo.konyushkov.captcha.utils;

import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.captcha.dto.CaptchaSettings;
import demo.konyushkov.captcha.dto.GoogleResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CaptchaValidatorUT {

    private final static String SECRET = "validSecret";
    private final static String SOCKET = "localhost:8080";
    private final static String VALID_RESPONSE = "validResponse";
    private final static String IP = "192.168.1.0";

    private CaptchaValidator captchaValidator;
    @Mock private RestTemplate restTemplate;

    @BeforeEach
    void init() {
        CaptchaSettings captchaSettings = new CaptchaSettings(SOCKET, SECRET);
        captchaValidator = new CaptchaValidator(captchaSettings, restTemplate);
    }

    @Nested
    class Validate {

        @Test
        void When_ResponseAndIpExist_Expect_VerifyCaptchaAndReturnGoogleResponse() {
            GoogleResponse googleResponse = new GoogleResponse();

            Mockito.when(restTemplate.getForObject(anyString(), eq(GoogleResponse.class))).thenReturn(googleResponse);

            GoogleResponse gResponse = captchaValidator.validate(IP, VALID_RESPONSE);

            ArgumentCaptor<String> uriArgument = ArgumentCaptor.forClass(String.class);
            verify(restTemplate).getForObject(uriArgument.capture(), eq(GoogleResponse.class));

            String uri = "https://www.google.com/recaptcha/api/siteverify?secret=validSecret&response=validResponse&remoteip=192.168.1.0";
            assertEquals(uri, uriArgument.getValue());
            assertEquals(googleResponse, gResponse);
        }

        @Test
        void When_IpIsNull_Expect_ThrowNullPointerException() {
            assertThrows(NullPointerException.class,
                    () -> captchaValidator.validate(null, VALID_RESPONSE));
        }

        @Test
        void When_ResponseIsNull_Expect_ThrowNullPointerException() {
            assertThrows(NullPointerException.class,
                    () -> captchaValidator.validate(IP, null));
        }

        @Test
        void When_RestTemplateReturnNull_Expect_ThrowInternalServerException() {
            Mockito.when(restTemplate.getForObject(anyString(), eq(GoogleResponse.class))).thenReturn(null);
            assertThrows(InternalServerException.class, () -> captchaValidator.validate(IP, VALID_RESPONSE));
        }

        @Test
        void When_RestTemplateThrowRestClientException_Expect_ThrowInternalServerException() {
            Mockito.when(restTemplate.getForObject(anyString(), eq(GoogleResponse.class))).thenThrow(RestClientException.class);
            InternalServerException internalServerException = assertThrows(InternalServerException.class, () -> captchaValidator.validate(IP, VALID_RESPONSE));
            assertEquals(RestClientException.class, internalServerException.getCause().getClass());
        }
    }

}
