package demo.konyushkov.captcha.consumer;

import demo.konyushkov.base.exception.ForbiddenException;
import demo.konyushkov.captcha.config.AppContextIT;
import demo.konyushkov.captcha.dto.CaptchaRequest;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CaptchaConsumerIT {

    @Nested
    class Validate extends AppContextIT {

        private static final String CAPTCHA_VALIDATE = "captcha.validate";

        @Autowired AmqpTemplate amqpTemplate;
        @Autowired MessageSourceAccessor messageSourceAccessor;

        @Test
        void When_InvalidGRecaptchaResponse_Expect_ThrowForbiddenException() {
            CaptchaRequest captchaRequest = new CaptchaRequest("192.168.0.1", "invalid_captcha_response");
            ForbiddenException exception = assertThrows(ForbiddenException.class, () -> amqpTemplate.convertSendAndReceive(CAPTCHA_VALIDATE, captchaRequest));
            assertEquals(messageSourceAccessor.getMessage("CaptchaServiceImpl.validate.invalidResponse"), exception.getMessage());
        }
    }

}