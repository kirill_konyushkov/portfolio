package demo.konyushkov.captcha.dto;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GoogleResponseUT {

    @Nested
    class HasServerError {

        @Test
        void When_Success_Expect_ReturnFalse() {
            GoogleResponse googleResponse = build(true);
            assertFalse(googleResponse.hasServerError());
        }

        @Test
        void When_ErrorCodesIsEmpty_Expect_ReturnFalse() {
            GoogleResponse googleResponse = build(false);
            assertFalse(googleResponse.hasServerError());
        }

        @Test
        void When_ErrorCodeHasMissingSecret_Expect_ReturnTrue() {
            GoogleResponse googleResponse = build(false, GoogleResponse.ErrorCode.MissingSecret);
            assertTrue(googleResponse.hasServerError());
        }

        @Test
        void When_ErrorCodeHasInvalidSecret_Expect_ReturnTrue() {
            GoogleResponse googleResponse = build(false, GoogleResponse.ErrorCode.InvalidSecret);
            assertTrue(googleResponse.hasServerError());
        }
    }

    @Nested
    class HasClientError {

        @Test
        void When_Success_Expect_ReturnFalse() {
            GoogleResponse googleResponse = build(true);
            assertFalse(googleResponse.hasClientError());
        }

        @Test
        void When_ErrorCodesIsEmpty_Expect_ReturnFalse() {
            GoogleResponse googleResponse = build(false);
            assertFalse(googleResponse.hasClientError());
        }

        @Test
        void When_ErrorCodeHasMissingResponse_Expect_ReturnTrue() {
            GoogleResponse googleResponse = build(false, GoogleResponse.ErrorCode.MissingResponse);
            assertTrue(googleResponse.hasClientError());
        }

        @Test
        void When_ErrorCodeHasMissingSecret_Expect_ReturnTrue() {
            GoogleResponse googleResponse = build(false, GoogleResponse.ErrorCode.InvalidResponse);
            assertTrue(googleResponse.hasClientError());
        }
    }

    @Nested
    class Contains {

        @Test
        void When_Contains_Expect_ReturnTrue() {
            GoogleResponse googleResponse = build(true, GoogleResponse.ErrorCode.InvalidResponse);
            assertEquals(1, googleResponse.getErrorCodes().length);
            assertTrue(googleResponse.contains(GoogleResponse.ErrorCode.InvalidResponse));
        }

        @Test
        void When_NotContains_Expect_ReturnFalse() {
            GoogleResponse googleResponse = build(true, GoogleResponse.ErrorCode.InvalidResponse);
            assertEquals(1, googleResponse.getErrorCodes().length);
            assertFalse(googleResponse.contains(GoogleResponse.ErrorCode.MissingResponse));
        }
    }

    private GoogleResponse build(boolean success, GoogleResponse.ErrorCode ... errorCodes) {
        GoogleResponse googleResponse = new GoogleResponse();
        googleResponse.setSuccess(success);
        googleResponse.setErrorCodes(errorCodes);

        return googleResponse;
    }

}
