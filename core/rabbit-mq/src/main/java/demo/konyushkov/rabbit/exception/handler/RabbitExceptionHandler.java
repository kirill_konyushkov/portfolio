package demo.konyushkov.rabbit.exception.handler;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.dto.ErrorDetails;
import demo.konyushkov.base.exception.handler.DefaultExceptionHandler;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.messaging.handler.invocation.MethodArgumentResolutionException;

public class RabbitExceptionHandler {

    private DefaultExceptionHandler exceptionHandler;

    public RabbitExceptionHandler(DefaultExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    public ErrorDetails handleException(Throwable t) {
        if(t instanceof MessageConversionException
                || t instanceof MethodArgumentResolutionException
                || t instanceof NoSuchMethodException
                || t instanceof ClassCastException) {
            return exceptionHandler.handleException(new BadRequestException(t));
        }

        return exceptionHandler.handleException(t);
    }
}
