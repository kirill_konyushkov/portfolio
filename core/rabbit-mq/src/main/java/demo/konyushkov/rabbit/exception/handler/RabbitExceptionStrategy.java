package demo.konyushkov.rabbit.exception.handler;

import demo.konyushkov.base.exception.dto.ErrorDetails;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.listener.FatalExceptionStrategy;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;

public class RabbitExceptionStrategy implements FatalExceptionStrategy {

    private final AmqpTemplate amqpTemplate;
    private final RabbitExceptionHandler exceptionHandler;

    public RabbitExceptionStrategy(RabbitExceptionHandler exceptionHandler, AmqpTemplate amqpTemplate) {
        this.exceptionHandler = exceptionHandler;
        this.amqpTemplate = amqpTemplate;
    }

    @Override
    public boolean isFatal(@NonNull Throwable t) {
        if (t instanceof ListenerExecutionFailedException) {
            ListenerExecutionFailedException ex = (ListenerExecutionFailedException) t;
            ErrorDetails errorDetails = exceptionHandler.handleException(ex.getCause());

            Message failedMessage = ex.getFailedMessage();
            sendErrorToReplyTo(failedMessage, errorDetails);

            return !isServerError(errorDetails);
        }

        return false;
    }

    private boolean isServerError(ErrorDetails errorDetails) {
        return HttpStatus.INTERNAL_SERVER_ERROR.equals(errorDetails.getStatus());
    }

    private void sendErrorToReplyTo(Message failedMessage, ErrorDetails errorDetails) {
        MessageProperties failedProperties = failedMessage.getMessageProperties();
        if(failedProperties == null) {
            return;
        }

        String replyTo = failedProperties.getReplyTo();
        if(StringUtils.isNotBlank(replyTo)) {
            String correlationId = failedProperties.getCorrelationId();

            if(StringUtils.isBlank(correlationId)) {
                return;
            }

            amqpTemplate.convertAndSend(replyTo, errorDetails, message -> {
                message.getMessageProperties().setCorrelationId(correlationId);
                return message;
            });
        }
    }
}
