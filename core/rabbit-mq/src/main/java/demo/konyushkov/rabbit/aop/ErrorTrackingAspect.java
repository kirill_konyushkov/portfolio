package demo.konyushkov.rabbit.aop;

import com.google.common.collect.ImmutableMap;
import demo.konyushkov.base.exception.*;
import demo.konyushkov.base.exception.dto.ErrorDetails;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.function.Function;

@Aspect
public class ErrorTrackingAspect {

    private static final Map<HttpStatus, Function<String, RequestException>> exsMap;

    static {
        exsMap = ImmutableMap.<HttpStatus, Function<String, RequestException>>builder()
                .put(HttpStatus.BAD_REQUEST, BadRequestException::new)
                .put(HttpStatus.FORBIDDEN, ForbiddenException::new)
                .put(HttpStatus.INTERNAL_SERVER_ERROR, InternalServerException::new)
                .put(HttpStatus.NOT_FOUND, NotFoundException::new)
                .put(HttpStatus.UNAUTHORIZED, message -> new UnauthorizedException())
                .build();
    }

    @AfterReturning(pointcut = "execution(public * org.springframework.amqp.rabbit.core.RabbitTemplate.convertSendAndReceive(..))",
                    returning= "result")
    public void interceptErrorDetailsAndThrowException(Object result) {
        if(result instanceof ErrorDetails) {
            ErrorDetails details = (ErrorDetails) result;
            throw exsMap.get(details.getStatus()).apply(details.getMessage());
        }
    }

}
