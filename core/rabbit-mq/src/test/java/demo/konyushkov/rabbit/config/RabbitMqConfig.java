package demo.konyushkov.rabbit.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfig extends AbstractRabbitMqConfig {

    @Bean
    public Queue testQueue() {
        return new Queue("TEST_QUEUE");
    }


}
