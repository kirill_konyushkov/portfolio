package demo.konyushkov.rabbit.exception.handler;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.base.exception.RequestException;
import demo.konyushkov.base.exception.dto.ErrorDetails;
import demo.konyushkov.base.exception.handler.DefaultExceptionHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.invocation.MethodArgumentResolutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class RabbitExceptionHandlerUT {

    @Mock private DefaultExceptionHandler defaultHandler;
    private RabbitExceptionHandler rabbitExceptionHandler;

    @BeforeEach
    void init() {
        rabbitExceptionHandler = new RabbitExceptionHandler(defaultHandler);
        Mockito.when(defaultHandler.handleException(any(BadRequestException.class))).thenAnswer(i -> {
            RequestException ex = i.getArgument(0);
            return new ErrorDetails(ex.getHttpStatus(), "{BadRequestException.message}");
        });
    }

    @Nested
    class HandleException {


        @Test
        void When_CauseIsMessageConversionException_Expect_WrapBadRequestExceptionAndHandleException() {
            String causeMessage = "exception message";
            MessageConversionException cause = new MessageConversionException(causeMessage);
            ErrorDetails errorDetails = rabbitExceptionHandler.handleException(cause);
            assertEquals(HttpStatus.BAD_REQUEST, errorDetails.getStatus());
            assertEquals("{BadRequestException.message}", errorDetails.getMessage());
        }

        @Test
        void When_CauseIsMethodArgumentResolutionException_Expect_WrapBadRequestExceptionAndHandleException() {
            MethodArgumentResolutionException cause = Mockito.mock(MethodArgumentResolutionException.class);
            ErrorDetails errorDetails = rabbitExceptionHandler.handleException(cause);
            assertEquals(HttpStatus.BAD_REQUEST, errorDetails.getStatus());
            assertEquals("{BadRequestException.message}", errorDetails.getMessage());
        }

        @Test
        void When_CauseIsNoSuchMethodException_Expect_WrapBadRequestExceptionAndHandleException() {
            String causeMessage = "exception message";
            NoSuchMethodException cause = new NoSuchMethodException(causeMessage);
            ErrorDetails errorDetails = rabbitExceptionHandler.handleException(cause);
            assertEquals(HttpStatus.BAD_REQUEST, errorDetails.getStatus());
            assertEquals("{BadRequestException.message}", errorDetails.getMessage());
        }

        @Test
        void When_CauseIsClassCastException_Expect_WrapBadRequestExceptionAndHandleException() {
            String causeMessage = "exception message";
            ClassCastException cause = new ClassCastException(causeMessage);
            ErrorDetails errorDetails = rabbitExceptionHandler.handleException(cause);
            assertEquals(HttpStatus.BAD_REQUEST, errorDetails.getStatus());
            assertEquals("{BadRequestException.message}", errorDetails.getMessage());
        }

        @Test
        void When_CauseIsOtherException_Expect_HandleException() {
            Mockito.when(defaultHandler.handleException(any(RequestException.class))).thenAnswer(i -> {
                RequestException ex = i.getArgument(0);
                return new ErrorDetails(ex.getHttpStatus(), ex.getMessage());
            });

            String causeMessage = "exception message";
            InternalServerException cause = new InternalServerException(causeMessage);
            ErrorDetails errorDetails = rabbitExceptionHandler.handleException(cause);
            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, errorDetails.getStatus());
            assertEquals(causeMessage, errorDetails.getMessage());
        }

    }

}