package demo.konyushkov.rabbit.exception.handler;

import demo.konyushkov.base.exception.*;
import demo.konyushkov.base.exception.dto.ErrorDetails;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.converter.MessageConversionException;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class RabbitExceptionStrategyUT {

    private RabbitExceptionStrategy strategy;
    @Mock private RabbitExceptionHandler handler;
    @Mock private AmqpTemplate amqpTemplate;

    private AtomicBoolean sendReply;

    @BeforeEach
    void init() {
        sendReply = new AtomicBoolean(false);
        strategy = new RabbitExceptionStrategy(handler, amqpTemplate);
        Mockito.doAnswer(i -> {
            sendReply.set(true);
            return null;
        }).when(amqpTemplate).convertAndSend(anyString(), any(ErrorDetails.class), any(MessagePostProcessor.class));
    }

    @Nested
    class IsFatal {

        @Test
        void When_CauseIsNotListenerExecutionFailedException_Expect_ReturnFalse() {
            Exception exception = new MessageConversionException("conversion failed");
            assertFalse(strategy.isFatal(exception));
        }

        @Test
        void When_ExceptionHandlerReturnErrorDetailsWithBadRequestErrorStatus_Expect_ReturnTrue() {
            String exMessage = "Exception message";
            Message failedMessage = new Message(null,  new MessageProperties());
            BadRequestException cause = new BadRequestException("{BadRequestException.message}");
            ListenerExecutionFailedException exception = new ListenerExecutionFailedException(exMessage, cause, failedMessage);
            Mockito.when(handler.handleException(cause)).thenReturn(new ErrorDetails(HttpStatus.BAD_REQUEST, "{BadRequestException.message}"));

            assertTrue(strategy.isFatal(exception));
            assertFalse(sendReply.get());
        }

        @Test
        void When_ExceptionHandlerReturnErrorDetailsWithForbiddenStatus_Expect_ReturnFalse() {
            String exMessage = "Exception message";
            Message failedMessage = new Message(null,  new MessageProperties());
            ForbiddenException cause = new ForbiddenException();
            ListenerExecutionFailedException exception = new ListenerExecutionFailedException(exMessage, cause, failedMessage);
            Mockito.when(handler.handleException(cause)).thenReturn(new ErrorDetails(HttpStatus.FORBIDDEN, "{ForbiddenException.message}"));

            assertTrue(strategy.isFatal(exception));
            assertFalse(sendReply.get());
        }

        @Test
        void When_ExceptionHandlerReturnErrorDetailsWithInternalServerErrorStatus_Expect_ReturnFalse() {
            String exMessage = "Exception message";
            Message failedMessage = new Message(null,  new MessageProperties());
            InternalServerException cause = new InternalServerException();
            ListenerExecutionFailedException exception = new ListenerExecutionFailedException(exMessage, cause, failedMessage);
            Mockito.when(handler.handleException(cause)).thenReturn(new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, "{InternalServerException.message}"));

            assertFalse(strategy.isFatal(exception));
            assertFalse(sendReply.get());
        }

        @Test
        void When_ExceptionHandlerReturnErrorDetailsWithNotFoundErrorStatus_Expect_ReturnTrue() {
            String exMessage = "Exception message";
            Message failedMessage = new Message(null,  new MessageProperties());
            NotFoundException cause = new NotFoundException("{NotFoundException.message}");
            ListenerExecutionFailedException exception = new ListenerExecutionFailedException(exMessage, cause, failedMessage);
            Mockito.when(handler.handleException(cause)).thenReturn(new ErrorDetails(HttpStatus.NOT_FOUND, "{NotFoundException.message}"));

            assertTrue(strategy.isFatal(exception));
            assertFalse(sendReply.get());
        }

        @Test
        void When_ExceptionHandlerReturnErrorDetailsWithUnauthorizedStatus_Expect_ReturnFalse() {
            String exMessage = "Exception message";
            Message failedMessage = new Message(null,  new MessageProperties());
            UnauthorizedException cause = new UnauthorizedException();
            ListenerExecutionFailedException exception = new ListenerExecutionFailedException(exMessage, cause, failedMessage);
            Mockito.when(handler.handleException(cause)).thenReturn(new ErrorDetails(HttpStatus.UNAUTHORIZED, "{UnauthorizedException.message}"));

            assertTrue(strategy.isFatal(exception));
            assertFalse(sendReply.get());
        }

        @Test
        void When_FailedMessageHasReplyToAndNotFatalException_Expect_SendErrorDetailsToReplyToAndReturnFalse() {
            String exMessage = "Exception message";
            String replyTo = "reply.to.queue";
            MessageProperties messageProperties = new MessageProperties();
            messageProperties.setCorrelationId(UUID.randomUUID().toString());
            messageProperties.setReplyTo(replyTo);

            Message failedMessage = new Message(null, messageProperties);

            InternalServerException cause = new InternalServerException();
            ListenerExecutionFailedException exception = new ListenerExecutionFailedException(exMessage, cause, failedMessage);
            Mockito.when(handler.handleException(cause)).thenReturn(new ErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, "{InternalServerException.message}"));

            assertFalse(strategy.isFatal(exception));
            assertTrue(sendReply.get());

            ArgumentCaptor<ErrorDetails> argument = ArgumentCaptor.forClass(ErrorDetails.class);
            verify(amqpTemplate).convertAndSend(anyString(), argument.capture(), any(MessagePostProcessor.class));
            ErrorDetails value = argument.getValue();

            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, value.getStatus());
            assertEquals("{InternalServerException.message}", value.getMessage());

        }

        @Test
        void When_FailedMessageHasReplyToAndFatalException_Expect_SendErrorDetailsToReplyToAndReturnTrue() {
            String exMessage = "Exception message";
            String replyTo = "reply.to.queue";
            Message failedMessage = MessageBuilder.withBody("Hello world".getBytes())
                    .setCorrelationId(UUID.randomUUID().toString())
                    .setReplyTo(replyTo)
                    .build();

            BadRequestException cause = new BadRequestException("{BadRequestException.message}");
            ListenerExecutionFailedException exception = new ListenerExecutionFailedException(exMessage, cause, failedMessage);
            Mockito.when(handler.handleException(cause)).thenReturn(new ErrorDetails(HttpStatus.BAD_REQUEST, "{BadRequestException.message}"));

            assertTrue(strategy.isFatal(exception));
            assertTrue(sendReply.get());

            ArgumentCaptor<ErrorDetails> argument = ArgumentCaptor.forClass(ErrorDetails.class);
            verify(amqpTemplate).convertAndSend(anyString(), argument.capture(), any(MessagePostProcessor.class));
            ErrorDetails value = argument.getValue();

            assertEquals(HttpStatus.BAD_REQUEST, value.getStatus());
            assertEquals("{BadRequestException.message}", value.getMessage());
        }

    }

}