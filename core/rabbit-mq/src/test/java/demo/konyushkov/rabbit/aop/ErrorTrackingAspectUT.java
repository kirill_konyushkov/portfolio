package demo.konyushkov.rabbit.aop;

import demo.konyushkov.base.exception.*;
import demo.konyushkov.base.exception.dto.ErrorDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsSecondArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ErrorTrackingAspectUT {

    private static final String QUEUE = "queue";

    @Mock private RabbitTemplate amqpTemplate;
    private AmqpTemplate proxy;

    @BeforeEach
    void init() {
        ErrorTrackingAspect errorTrackingAspect = new ErrorTrackingAspect();
        AspectJProxyFactory factory = new AspectJProxyFactory(amqpTemplate);
        factory.addAspect(errorTrackingAspect);
        proxy = factory.getProxy();
    }

    @Nested
    class InterceptErrorDetailsAndThrowException {

        @Test
        void When_ReturnObject_Expect_ReturnObject() {
            Mockito.when(amqpTemplate.convertSendAndReceive(eq(QUEUE), any(Object.class))).thenAnswer(returnsSecondArg());
            RestMsg msg = new RestMsg("Hello");
            Object result = amqpTemplate.convertSendAndReceive(QUEUE, msg);
            Object proxyResult = proxy.convertSendAndReceive(QUEUE, msg);
            assertEquals(result, proxyResult);
        }

        @Test
        void When_ReturnErrorDetailsWithBadRequest_Expect_ThrowBadRequestExceptionWithExceptionMessage() {
            String exMessage = "Bad request message";
            prepareErrorDetails(HttpStatus.BAD_REQUEST, exMessage);

            BadRequestException exception = assertThrows(BadRequestException.class,
                    () -> proxy.convertSendAndReceive(QUEUE, new RestMsg("Hello")));
            assertEquals(exMessage, exception.getMessage());
        }

        @Test
        void When_ReturnErrorDetailsWithForbidden_Expect_ThrowForbiddenExceptionWithExceptionMessage() {
            String exMessage = "forbidden message";
            prepareErrorDetails(HttpStatus.FORBIDDEN, exMessage);

            ForbiddenException exception = assertThrows(ForbiddenException.class,
                    () -> proxy.convertSendAndReceive(QUEUE, new RestMsg("Hello")));
            assertEquals(exMessage, exception.getMessage());
        }

        @Test
        void When_ReturnErrorDetailsWithInternalServerError_Expect_ThrowInternalServerExceptionWithExceptionMessage() {
            String exMessage = "internal error message";
            prepareErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, exMessage);

            InternalServerException exception = assertThrows(InternalServerException.class,
                    () -> proxy.convertSendAndReceive(QUEUE, new RestMsg("Hello")));
            assertEquals(exMessage, exception.getMessage());
        }


        @Test
        void When_ReturnErrorDetailsWithNotFound_Expect_ThrowNotFoundExceptionWithExceptionMessage() {
            String exMessage = "not found message";
            prepareErrorDetails(HttpStatus.NOT_FOUND, exMessage);

            NotFoundException exception = assertThrows(NotFoundException.class,
                    () -> proxy.convertSendAndReceive(QUEUE, new RestMsg("Hello")));
            assertEquals(exMessage, exception.getMessage());
        }

        @Test
        void When_ReturnErrorDetailsWithUnauthorized_Expect_ThrowUnauthorizedExceptionWithDefaultMessage() {
            String exMessage = "{UnauthorizedException.message}";
            prepareErrorDetails(HttpStatus.UNAUTHORIZED, exMessage);

            UnauthorizedException exception = assertThrows(UnauthorizedException.class,
                    () -> proxy.convertSendAndReceive(QUEUE, new RestMsg("Hello")));
            assertEquals(exMessage, exception.getMessage());
        }

        private void prepareErrorDetails(HttpStatus status, String message) {
            ErrorDetails errorDetails = new ErrorDetails(status, message);
            Mockito.when(amqpTemplate.convertSendAndReceive(eq(QUEUE), any(Object.class))).thenReturn(errorDetails);
        }
    }

    @Data
    @EqualsAndHashCode
    @AllArgsConstructor
    private static class RestMsg {
        private String msg;
    }

}