package demo.konyushkov.rabbit;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.rabbit.config.RabbitContextIT;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static org.junit.jupiter.api.Assertions.*;


class RabbitTemplateIT {

    @Nested
    class ConvertSendAndReceive extends RabbitContextIT {

        @Autowired AmqpTemplate amqpTemplate;
        @Autowired AmqpAdmin amqpAdmin;

        @Value("#{testQueue.name}")
        String TEST_QUEUE;

        @BeforeEach
        void init() {
            amqpAdmin.purgeQueue(TEST_QUEUE);
        }

        @Test
        void When_CorrectWork_Expect_ReturnExpectedValue() {
            String msg = "Hello world 2";
            Object obj = amqpTemplate.convertSendAndReceive(TEST_QUEUE, msg);
            RestEntity entity = (RestEntity) obj;
            assertNotNull(entity);
            assertEquals(msg, entity.getMsg());
        }

        @Test
        void When_NotCorrectWork_Expect_ThrowException() {
            String msg = "not correct";
            BadRequestException exception = assertThrows(BadRequestException.class, () -> amqpTemplate.convertSendAndReceive(TEST_QUEUE, msg));
            assertEquals("Message not correct", exception.getMessage());
        }

        @Test
        void WhenServerErrorWork_Expect_ThrowException() {
            String msg = "server error";
            InternalServerException exception = assertThrows(InternalServerException.class, () -> amqpTemplate.convertSendAndReceive(TEST_QUEUE, msg));
            assertEquals("server error message", exception.getMessage());
        }

    }

    @Component
    static class TestLogic {

        @RabbitListener(queues = "#{testQueue.name}")
        private RestEntity test(String msg) {
            if(msg.equals("not correct")) {
                throw new BadRequestException("Message not correct");
            }
            if(msg.equals("server error")) {
                throw new InternalServerException("server error message");
            }
            return new RestEntity(msg);
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class RestEntity {
        String msg;
    }

}
