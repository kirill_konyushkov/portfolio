package demo.konyushkov.web.aop;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RestResponseDetails {
    String title() default "";
    String message();
}
