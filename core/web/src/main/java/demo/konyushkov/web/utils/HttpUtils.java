package demo.konyushkov.web.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class HttpUtils {

    public static String getClientIP(HttpServletRequest request) {
        try {
            String xfHeader = request.getHeader("X-Forwarder-For");
            return xfHeader.split(",")[0];
        }
        catch (NullPointerException ex) {
            if(request == null) {
                log.warn("Request is null");
                return null;
            }

            return request.getRemoteAddr();
        }
    }

    public static boolean isRequestRefererUrl(HttpServletRequest request, String url) {

        try {
            String referer = request.getHeader("referer");
            return referer.startsWith(url);
        }
        catch (NullPointerException e) {
            if(request == null) {
                log.warn("Request is null");
                return false;
            }
            if(url == null) {
                log.warn("Url is null");
                return false;
            }

            log.warn("Request not contain header or url { referer: {} }. Request from ip: {}.", url, getClientIP(request));
            return false;
        }
    }
}
