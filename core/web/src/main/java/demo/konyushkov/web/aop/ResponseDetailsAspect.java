package demo.konyushkov.web.aop;

import demo.konyushkov.web.dto.Notice;
import demo.konyushkov.web.dto.ResponseDetails;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.ResponseEntity;

import java.lang.reflect.Method;

@Aspect
public class ResponseDetailsAspect {

    private final MessageSourceAccessor messageSourceAccessor;

    public ResponseDetailsAspect(MessageSourceAccessor messageSourceAccessor) {
        this.messageSourceAccessor = messageSourceAccessor;
    }

    @Around("execution(public * *(..)) && @annotation(demo.konyushkov.web.aop.RestResponseDetails)")
    public Object fillRestResponseDetails(ProceedingJoinPoint joinPoint) throws Throwable {
        // Get annotation
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        RestResponseDetails annotation = method.getAnnotation(RestResponseDetails.class);

        // Build notice
        String message = messageSourceAccessor.getMessage(annotation.message(), annotation.message());
        String title = annotation.title().isEmpty() ? null : messageSourceAccessor.getMessage(annotation.title(), annotation.title());
        Notice notice = new Notice(title, message);

        //Build new ResponseEntity with ResponseDetails
        ResponseEntity proceed = (ResponseEntity) joinPoint.proceed();
        ResponseDetails responseDetails = ResponseDetails
                .builder()
                .notice(notice)
                .content(proceed.getBody()).build();

        return new ResponseEntity<>(responseDetails, proceed.getHeaders(), proceed.getStatusCode());
    }
}
