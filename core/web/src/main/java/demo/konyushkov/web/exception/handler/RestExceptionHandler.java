package demo.konyushkov.web.exception.handler;

import demo.konyushkov.base.exception.dto.ErrorDetails;
import demo.konyushkov.base.exception.handler.DefaultExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler {

    private final DefaultExceptionHandler exceptionHandler;

    @Autowired
    public RestExceptionHandler(DefaultExceptionHandler exceptionHandler) {
        this.exceptionHandler = exceptionHandler;
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<ErrorDetails> handleException(Exception ex) {
        ErrorDetails errorDetails = exceptionHandler.handleException(ex);
        return buildResponseEntity(errorDetails);
    }

    private ResponseEntity<ErrorDetails> buildResponseEntity(ErrorDetails error) {
        return new ResponseEntity<>(error, new HttpHeaders(), error.getStatus());
    }
}
