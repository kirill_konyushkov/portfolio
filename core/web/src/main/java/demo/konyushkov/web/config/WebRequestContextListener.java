package demo.konyushkov.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextListener;

import javax.servlet.annotation.WebListener;

@WebListener
@Configuration
public class WebRequestContextListener extends RequestContextListener {}
