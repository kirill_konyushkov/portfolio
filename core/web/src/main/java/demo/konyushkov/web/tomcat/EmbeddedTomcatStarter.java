package demo.konyushkov.web.tomcat;

import org.apache.catalina.startup.Tomcat;

import java.io.File;
import java.io.IOException;

public class EmbeddedTomcatStarter {

    public static void start(int port) throws Exception {
        String appBase = ".";
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir(createTempDir(port));
        tomcat.setPort(port);
        tomcat.getHost().setAppBase(appBase);
        tomcat.addWebapp("", appBase);
        tomcat.getConnector();
        tomcat.start();
        tomcat.getServer().await();
    }

    private static String createTempDir(int port) {
        try {
            File tempDir = File.createTempFile("tomcat.", "." + port);
            tempDir.delete();
            tempDir.mkdir();
            tempDir.deleteOnExit();
            return tempDir.getAbsolutePath();
        } catch (IOException ex) {
            throw new RuntimeException(
                    "Unable to create tempDir. java.io.tmpdir is set to " + System.getProperty("java.io.tmpdir"),
                    ex
            );
        }
    }
}
