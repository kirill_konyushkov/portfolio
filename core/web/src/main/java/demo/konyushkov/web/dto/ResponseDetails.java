package demo.konyushkov.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResponseDetails<T> {
    private Notice notice;
    private T content;
}
