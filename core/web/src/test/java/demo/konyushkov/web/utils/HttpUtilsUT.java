package demo.konyushkov.web.utils;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class HttpUtilsUT {

    @Mock HttpServletRequest request;

    private static String IP = "192.168.0.1";

    @Nested
    class GetClientIP {
        @Test
        void When_XForwarderForHeaderIsMissing_Expect_ReturnRemoteAddr() {
            Mockito.when(request.getHeader("X-Forwarder-For")).thenReturn(null);
            Mockito.when(request.getRemoteAddr()).thenReturn(IP);

            assertEquals(IP, HttpUtils.getClientIP(request));
        }

        @Test
        void When_XForwarderForHeaderExist_Expect_ReturnXForwarderFor() {
            String[] ips = {"203.0.113.195", "70.41.3.18", "150.172.238.178"};
            Mockito.when(request.getHeader("X-Forwarder-For")).thenReturn(String.format("%s, %s, %s", ips[0], ips[1], ips[2]));
            Mockito.when(request.getRemoteAddr()).thenReturn(IP);

            assertEquals(ips[0], HttpUtils.getClientIP(request));
        }
    }

    @Nested
    class IsRequestRefererUrl {

        @Test
        void When_RequestRefererHeaderStartWithUrl_Expect_True() {
            String url = "http://localhost:8080/random";
            Mockito.when(request.getHeader("referer")).thenReturn(url);

            assertTrue(HttpUtils.isRequestRefererUrl(request, "http://localhost:8080"));
        }

        @Test
        void When_RequestRefererHeaderIsNull_Expect_False() {
            Mockito.when(request.getHeader("referer")).thenReturn(null);

            assertFalse(HttpUtils.isRequestRefererUrl(request, "http://localhost:8080"));
        }

        @Test
        void When_RequestRefererHeaderIsNotStartWithUrl_Expect_False() {
            String url = "http://another-domain/random";
            Mockito.when(request.getHeader("referer")).thenReturn(url);

            assertFalse(HttpUtils.isRequestRefererUrl(request, "http://localhost:8080"));
        }

        @Test
        void When_UrlIsNull_Expect_False() {
            String url = "http://another-domain/random";
            Mockito.when(request.getHeader("referer")).thenReturn(url);

            assertFalse(HttpUtils.isRequestRefererUrl(request, null));
        }

        @Test
        void When_RequestIsNull_Expect_False() {
            assertFalse(HttpUtils.isRequestRefererUrl(null, "http://localhost:8080"));
        }
    }
}
