package demo.konyushkov.web.aop;

import demo.konyushkov.web.dto.ResponseDetails;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ResponseDetailsAspectUT {

    private static final String TEST_METHOD_TITLE = "TestController.testMethod.title";
    private static final String TEST_METHOD_MESSAGE = "TestController.testMethod.message";

    @Mock
    MessageSourceAccessor messageSourceAccessor;
    TestController controller;
    TestController proxy;

    @BeforeEach
    void init() {
        controller = new TestController();
        ResponseDetailsAspect responseDetailsAspect = new ResponseDetailsAspect(messageSourceAccessor);
        AspectJProxyFactory factory = new AspectJProxyFactory(controller);
        factory.addAspect(responseDetailsAspect);
        proxy = factory.getProxy();
        Mockito.when(messageSourceAccessor.getMessage(anyString(), anyString())).thenAnswer(i -> i.getArguments()[0]);
    }

    @Nested
    class FillRestResponseDetails {

        @Test
        void When_MethodWithRestResponseDetailsAnnotation_Expect_ReturnResponseEntityWithResponseDetailsWithContentWhichReturnDefaultMethodAndNoticeFromAnnotationParams() {
            ResponseEntity<RestMsg> defaultEntity = controller.testMethod();
            ResponseEntity proxyEntity = proxy.testMethod();
            ResponseDetails<RestMsg> proxyBody = (ResponseDetails<RestMsg>) proxyEntity.getBody();
            // Test content
            assertEquals(defaultEntity.getBody(), proxyBody.getContent());
            // Test notice
            assertEquals(TEST_METHOD_TITLE, proxyBody.getNotice().getTitle());
            assertEquals(TEST_METHOD_MESSAGE, proxyBody.getNotice().getMessage());
            // Test status and headers
            assertEquals(defaultEntity.getStatusCode(), proxyEntity.getStatusCode());
            assertEquals(defaultEntity.getHeaders(), proxyEntity.getHeaders());
        }
    }

    @NoArgsConstructor
    private static class TestController {
        @RestResponseDetails(title = TEST_METHOD_TITLE, message = TEST_METHOD_MESSAGE)
        public ResponseEntity<RestMsg> testMethod() {
            return new ResponseEntity<>(new RestMsg("Mock msg"), HttpStatus.OK);
        }
    }

    @Data
    @EqualsAndHashCode
    @AllArgsConstructor
    private static class RestMsg {
        private String msg;
    }
}
