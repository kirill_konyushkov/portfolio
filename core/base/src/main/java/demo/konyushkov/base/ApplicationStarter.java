package demo.konyushkov.base;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Slf4j
public class ApplicationStarter {

    @SuppressWarnings("InfiniteLoopStatement")
    public void start(Class configClass) throws InterruptedException {
        new AnnotationConfigApplicationContext(configClass);
        while (true) {
            Thread.sleep(Long.MAX_VALUE);
        }
    }

}
