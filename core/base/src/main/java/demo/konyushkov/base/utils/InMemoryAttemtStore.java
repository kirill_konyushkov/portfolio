package demo.konyushkov.base.utils;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class InMemoryAttemtStore implements AttemptStore {

    private LoadingCache<String, Integer> attemptsCache;
    private int MAX_ATTEMPT;

    public InMemoryAttemtStore(int MAX_ATTEMPT, int duration, TimeUnit timeUnit) {
        Precondition.checkArgument(MAX_ATTEMPT > 1, "MAX_ATTEMPT must be large 1");
        Precondition.checkArgument(duration > 0, "duration must be positive");
        Precondition.checkNotNull(timeUnit, "timeUnit must not be null");

        this.MAX_ATTEMPT = MAX_ATTEMPT;
        attemptsCache = CacheBuilder.newBuilder()
                .expireAfterWrite(duration, timeUnit)
                .build(new CacheLoader<>() {
                    @Override
                    public Integer load(@NonNull final String key) {
                        return 0;
                    }
                });
    }

    @Override
    public void increase(String key) {
        int failedAttempts = getAttempts(key) + 1;
        log.info(String.format("Increase attempt from ( KEY: %s; failed attempts: %s", key, failedAttempts));
        attemptsCache.put(key, failedAttempts);
    }

    @Override
    public void invalidate(String key) {
        attemptsCache.invalidate(key);
    }

    @Override
    public boolean isExceeded(String key) {
        return getAttempts(key) >= MAX_ATTEMPT;
    }

    @Override
    public int getAttempts(String key) {
        try {
            return attemptsCache.get(key);
        } catch (ExecutionException e) {
            return 0;
        }
    }
}
