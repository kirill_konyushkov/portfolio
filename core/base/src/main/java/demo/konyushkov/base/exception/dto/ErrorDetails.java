package demo.konyushkov.base.exception.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDetails<T> {
    private HttpStatus status;
    private String message;
    private T content;

    public ErrorDetails(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }
}
