package demo.konyushkov.base.exception;

import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.Set;

public class ForbiddenException extends RequestException {

    private static final HttpStatus HTTP_STATUS = HttpStatus.FORBIDDEN;
    private static final String NOTICE_MESSAGE_I18N_CODE = "{ForbiddenException.message}";

    public ForbiddenException() {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, null, null);
    }

    public ForbiddenException(String message) {
        super(message, HTTP_STATUS, null, null);
    }

    public ForbiddenException(String message, Map<String, Set<String>> content) {
        super(message, HTTP_STATUS, content, null);
    }

    public ForbiddenException(String message, Map<String, Set<String>> content, Throwable cause) {
        super(message, HTTP_STATUS, content, cause);
    }

    public ForbiddenException(Throwable cause) {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, null, cause);
    }
}
