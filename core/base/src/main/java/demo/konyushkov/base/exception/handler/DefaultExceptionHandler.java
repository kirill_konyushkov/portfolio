package demo.konyushkov.base.exception.handler;


import com.google.common.base.Throwables;
import demo.konyushkov.base.exception.RequestException;
import demo.konyushkov.base.exception.dto.ErrorDetails;
import demo.konyushkov.base.validation.constraints.SpELAssert;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.metadata.ConstraintDescriptor;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Slf4j
@Component
public class DefaultExceptionHandler {

    private static final String CONSTRAINT_VIOLATION_EXCEPTION_MESSAGE_CODE = "{DefaultExceptionHandler.ConstraintViolationException.message}";
    private static final String EXCEPTION_MESSAGE_CODE = "{DefaultExceptionHandler.InternalServerError.message}";

    private final MessageSourceAccessor messageSourceAccessor;

    @Autowired
    protected DefaultExceptionHandler(MessageSourceAccessor messageSourceAccessor) {
        this.messageSourceAccessor = messageSourceAccessor;
    }

    public ErrorDetails handleException(Throwable t) {
        if(t instanceof RequestException) {
            return handleRequestException((RequestException) t);
        }

        Throwable cause = Throwables.getRootCause(t);

        if(cause instanceof RequestException) {
            return handleRequestException((RequestException) cause);
        }
        else if(cause instanceof MethodArgumentNotValidException) {
            return handleMethodArgumentNotValid((MethodArgumentNotValidException) cause);
        }
        else if(cause instanceof ConstraintViolationException) {
            return handleConstraintValidationException((ConstraintViolationException) cause);
        }

        return handleAnyException(cause);
    }

    private ErrorDetails handleRequestException(final RequestException ex) {
        log.warn("", ex);

        String message = getMessage(ex.getMessage());
        if(ex.getDetails() != null) {
            final Map<String, Set<String>> errors = ex.getDetails().entrySet()
                    .stream().collect(Collectors.toMap(Map.Entry::getKey, this::prepareMessages));

            return new ErrorDetails<>(ex.getHttpStatus(), message, errors);
        }

        return new ErrorDetails<>(ex.getHttpStatus(), message);
    }

    private ErrorDetails handleMethodArgumentNotValid(final MethodArgumentNotValidException ex) {
        final Map<String, Set<String>> errors = ex.getBindingResult().getAllErrors().stream()
                .collect(groupingBy(
                            this::getPropertyPath,
                            mapping(ObjectError::getDefaultMessage, toSet())));

        log.warn("", ex);

        return getConstraintViolationErrorDetails(errors);
    }

    private ErrorDetails handleConstraintValidationException(final ConstraintViolationException ex) {

        final Map<String, Set<String>> errors = ex.getConstraintViolations().stream()
                .collect(groupingBy(this::getPropertyPath,
                        mapping(ConstraintViolation::getMessage, toSet())));

        log.warn("", ex);

        return getConstraintViolationErrorDetails(errors);
    }

    private String getPropertyPath(ObjectError objectError) {
        if(objectError instanceof FieldError) {
            return ((FieldError) objectError).getField();
        }
        try {
            ConstraintViolation constraintViolation = objectError.unwrap(ConstraintViolation.class);
            return getPropertyPath(constraintViolation);
        }
        catch (Exception ex) {
            log.error("", ex);
            return "";
        }
    }

    private String getPropertyPath(ConstraintViolation violation) {
        ConstraintDescriptor constraintDescriptor = violation.getConstraintDescriptor();
        if(constraintDescriptor.getAnnotation() instanceof SpELAssert) {
            Map attributes = constraintDescriptor.getAttributes();
            return (String) attributes.get("propertyPath");
        }

        return violation.getPropertyPath().toString();
    }

    private ErrorDetails getConstraintViolationErrorDetails(Map<String, Set<String>> errors) {
        return new ErrorDetails<>(HttpStatus.BAD_REQUEST, getMessage(CONSTRAINT_VIOLATION_EXCEPTION_MESSAGE_CODE), errors);
    }

    private ErrorDetails handleAnyException(Throwable t) {
        log.error("", t);

        return new ErrorDetails<>(HttpStatus.INTERNAL_SERVER_ERROR, getMessage(EXCEPTION_MESSAGE_CODE));
    }

    private Set<String> prepareMessages(Map.Entry<String, Set<String>> entry) {
        return entry.getValue().stream().map(this::getMessage).collect(toSet());
    }

    private String getMessage(String messageOrCode) {
        if(StringUtils.isBlank(messageOrCode)) {
            return messageOrCode;
        } else if(messageOrCode.startsWith("{") && messageOrCode.endsWith("}")) {
            String code = messageOrCode.substring(1, messageOrCode.length()-1);
            return messageSourceAccessor.getMessage(code, messageOrCode);
        }

        return messageOrCode;
    }
}
