package demo.konyushkov.base.validation.constraints;

import demo.konyushkov.base.validation.validator.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PasswordValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Password {

    String message() default "{demo.konyushkov.validation.constraints.Password}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
