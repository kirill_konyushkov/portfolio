package demo.konyushkov.base.exception;

import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.Set;

public class NotFoundException extends RequestException {

    private static final HttpStatus HTTP_STATUS = HttpStatus.NOT_FOUND;

    public NotFoundException() {
        super(null, HTTP_STATUS, null, null);
    }

    public NotFoundException(String message) {
        super(message, HTTP_STATUS, null, null);
    }

    public NotFoundException(String message, Map<String, Set<String>> content) {
        super(message, HTTP_STATUS, content, null);
    }

    public NotFoundException(String message, Map<String, Set<String>> content, Throwable cause) {
        super(message, HTTP_STATUS, content, cause);
    }

    public NotFoundException(Throwable cause) {
        super(null, HTTP_STATUS, null, cause);
    }
}
