package demo.konyushkov.base.utils;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;

@Slf4j
public class Precondition {

    public static <T extends Exception> void checkArgument(boolean expression, String errorMesage) throws T {
        checkArgument(expression, IllegalArgumentException.class, errorMesage);
    }

    public static <T extends Exception> void checkArgument(boolean expression, Class<T> exClass) throws T {
        checkArgument(expression, exClass, null);
    }

    public static <T extends Exception> void checkArgument(boolean expression, Class<T> exClass, String errorMessage, Object... args) throws T {
        if (!expression) {
            generateException(exClass, errorMessage, args);
        }
    }

    public static <T extends Exception> void checkNotNull(Object reference, String errorMessage) throws T {
        checkNotNull(reference, IllegalArgumentException.class, errorMessage);
    }

    public static <T extends Exception> void checkNotNull(Object reference, Class<T> exClass) throws T {
        checkNotNull(reference, exClass, null);
    }

    public static <T extends Exception> void checkNotNull(Object reference, Class<T> exClass, String errorMessage, Object... args) throws T {
        if (reference == null) {
            generateException(exClass, errorMessage, args);
        }
    }

    private static <T extends Exception> void generateException(Class<T> exClass, String errorMessage, Object... args) throws T {
        try {
            if (errorMessage == null) {
                throw exClass.getDeclaredConstructor().newInstance();
            }
            else if (args == null || args.length == 0) {
                throw exClass.getDeclaredConstructor(String.class).newInstance(errorMessage);
            }
            else {
                throw exClass.getDeclaredConstructor(String.class, Object[].class).newInstance(errorMessage, args);
            }
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
            log.error("", ex);
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
