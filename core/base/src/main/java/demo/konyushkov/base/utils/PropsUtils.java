package demo.konyushkov.base.utils;

import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.function.Predicate;
import java.util.stream.StreamSupport;

public abstract class PropsUtils {

    public static Properties getProperties(Environment environment, String prefix, String ...ignored) {
        List<String> ignoredProps = Arrays.asList(ignored);

        Properties props = new Properties();
        MutablePropertySources sources = ((AbstractEnvironment) environment).getPropertySources();
        StreamSupport.stream(sources.spliterator(), false)
                .filter(ps -> ps instanceof EnumerablePropertySource)
                .map(ps -> ((EnumerablePropertySource) ps).getPropertyNames())
                .flatMap(Arrays::stream)
                .filter(propName -> propName.startsWith(prefix))
                .filter(Predicate.not(ignoredProps::contains))
                .forEach(propName -> props.put(propName, environment.getProperty(propName)));

        return props;
    }
}
