package demo.konyushkov.base.validation.validator;

import demo.konyushkov.base.validation.constraints.Password;

import java.util.regex.Pattern;

public class PasswordValidator extends CustomPatternConstraintValidator<Password> {

    public PasswordValidator() {
        super(Pattern.compile("^(?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z\\d#$%&'()*+,\\-.\\\\/:;<>=?@\\[\\]{}^_`~]{6,20}$"));
    }
}
