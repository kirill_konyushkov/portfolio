package demo.konyushkov.base.validation.validator;

import demo.konyushkov.base.utils.Precondition;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.Optional;
import java.util.regex.Pattern;

abstract class CustomPatternConstraintValidator<T extends Annotation> implements ConstraintValidator<T, String> {

    private final Pattern PATTERN;

    CustomPatternConstraintValidator(Pattern pattern) {
        Precondition.checkNotNull(pattern, "Pattern not be null");

        this.PATTERN = pattern;
    }

    @Override
    public final boolean isValid(String value, ConstraintValidatorContext context) {

        return Optional.ofNullable(value)
                .map((v) -> PATTERN.matcher(v).matches())
                .orElse(true);
    }
}
