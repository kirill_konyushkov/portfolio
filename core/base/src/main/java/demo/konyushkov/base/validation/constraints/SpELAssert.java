package demo.konyushkov.base.validation.constraints;


import demo.konyushkov.base.validation.validator.SpELAssertValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * JSR-303 constraint for validation with Spring Expression Language (SpEL).
 *
 * @author Jakub Jirutka <jakub@jirutka.cz>
 */
@Documented
@Retention(RUNTIME)
@Target({METHOD, FIELD, TYPE})
@Constraint(validatedBy = SpELAssertValidator.class)
public @interface SpELAssert {

    String message();
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    /**
     * Validation expression in SpEL.
     *
     * @see <a href="http://static.springsource.org/spring/docs/3.0.x/reference/expressions.html">
     *     documentation of Spring Expression Language</a>
     */
    String value();

    /**
     * Perform validation only if this SpEL expression evaluates to <tt>true</tt>.
     *
     * @see <a href="http://static.springsource.org/spring/docs/3.0.x/reference/expressions.html">
     *     documentation of Spring Expression Language</a>
     */
    String applyIf() default "";

    /**
     * Classes with static methods to register as helper functions which you
     * can call from expression as <tt>#methodName(arg1, arg2, ...)</tt>.
     * This does not support overloading, only last registered method of each
     * name will be used!
     */
    Class<?>[] helpers() default { };

    String propertyPath();


    /**
     * Defines several <code>@SpELAssertValidator</code> annotations on the same element.
     *
     * @see SpELAssert
     */
    @Documented
    @Retention(RUNTIME)
    @Target({METHOD, FIELD, TYPE})
    @interface List {
        SpELAssert[] value();
    }
}
