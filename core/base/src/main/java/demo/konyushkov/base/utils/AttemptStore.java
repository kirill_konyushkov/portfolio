package demo.konyushkov.base.utils;

public interface AttemptStore {

    void increase(final String ip);
    void invalidate(final String ip);
    boolean isExceeded(String ip);
    int getAttempts(String key);

}
