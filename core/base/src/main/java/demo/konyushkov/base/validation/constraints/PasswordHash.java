package demo.konyushkov.base.validation.constraints;

import demo.konyushkov.base.validation.validator.PasswordHashValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PasswordHashValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordHash {

    String message() default "{demo.konyushkov.validation.constraints.PasswordHash}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
