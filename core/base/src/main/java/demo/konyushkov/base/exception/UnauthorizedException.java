package demo.konyushkov.base.exception;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends RequestException {

    private static final HttpStatus HTTP_STATUS = HttpStatus.UNAUTHORIZED;
    private static final String NOTICE_MESSAGE_I18N_CODE = "{UnauthorizedException.message}";

    public UnauthorizedException() {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, null, null);
    }

    public UnauthorizedException(Throwable cause) {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, null, cause);
    }
}
