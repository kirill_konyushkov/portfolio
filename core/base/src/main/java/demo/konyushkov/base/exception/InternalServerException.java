package demo.konyushkov.base.exception;

import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.Set;

public class InternalServerException extends RequestException {

    private static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;
    private static final String NOTICE_MESSAGE_I18N_CODE = "{InternalServerException.message}";

    public InternalServerException() {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, null, null);
    }

    public InternalServerException(String message) {
        super(message, HTTP_STATUS, null, null);
    }

    public InternalServerException(String message, Map<String, Set<String>> content) {
        super(message, HTTP_STATUS,  content, null);
    }

    public InternalServerException(String message, Throwable cause) {
        super(message, HTTP_STATUS, null, cause);
    }

    public InternalServerException(String message, Map<String, Set<String>> content, Throwable cause) {
        super(message, HTTP_STATUS, content, cause);
    }

    public InternalServerException(Throwable cause) {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, null, cause);
    }
}
