package demo.konyushkov.base.exception;

import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.Set;

public class BadRequestException extends RequestException {

    private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;
    private static final String NOTICE_MESSAGE_I18N_CODE = "{BadRequestException.message}";

    public BadRequestException() {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, null, null);
    }

    public BadRequestException(String message) {
        super(message, HTTP_STATUS, null, null);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, HTTP_STATUS, null, cause);
    }

    public BadRequestException(Map<String, Set<String>> content) {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, content, null);
    }

    public BadRequestException(String message, Map<String, Set<String>> content) {
        super(message, HTTP_STATUS, content, null);
    }

    public BadRequestException(String message, Map<String, Set<String>> content, Throwable cause) {
        super(message, HTTP_STATUS, content, cause);
    }

    public BadRequestException(Throwable cause) {
        super(NOTICE_MESSAGE_I18N_CODE, HTTP_STATUS, null, cause);
    }
}
