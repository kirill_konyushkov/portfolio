package demo.konyushkov.base.validation.constraints;

import demo.konyushkov.base.validation.validator.DateBetweenValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateBetweenValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DateBetween {

    String message() default "{demo.konyushkov.validation.constraints.DateBetween}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    /**
     * @return size the element must be higher or equal to
     */
    String min();

    /**
     * @return size the element must be lower or equal to
     */
    String max();
}
