package demo.konyushkov.base.validation.validator;

import demo.konyushkov.base.validation.constraints.PasswordHash;

import java.util.regex.Pattern;

public class PasswordHashValidator extends CustomPatternConstraintValidator<PasswordHash> {

    public PasswordHashValidator() {
        super(Pattern.compile("^\\$2[ayb]\\$[\\S]{56}$"));
    }
}
