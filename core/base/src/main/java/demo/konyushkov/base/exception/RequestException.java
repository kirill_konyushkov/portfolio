package demo.konyushkov.base.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Map;
import java.util.Set;

@Getter
public abstract class RequestException extends RuntimeException {

    private HttpStatus httpStatus;
    private Map<String, Set<String>> details;

    RequestException(String message, HttpStatus httpStatus, Map<String, Set<String>> details, Throwable cause) {
        super(message, cause);
        this.httpStatus = httpStatus;
        this.details = details;
    }
}
