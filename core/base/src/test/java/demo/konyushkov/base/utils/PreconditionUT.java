package demo.konyushkov.base.utils;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.UnauthorizedException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class PreconditionUT {

    @Nested
    class CheckNotNull {
        @Test
        void When_ObjectIsNullAndMessageNotNullAndEmptyExceptionClass_Expect_ThrowIllegalArgumentExceptionWithMessage() {
            String message = "custom message";
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                    () -> Precondition.checkNotNull(null, message));
            assertEquals(message, ex.getMessage());
        }

        @Test
        void When_ObjectIsNullAndMessageNotNullAndValidExceptionClass_Expect_ThrowExceptionInstanceOfExceptionClassWithMessage() {
            String message = "custom message";
            BadRequestException ex = assertThrows(BadRequestException.class, () -> Precondition.checkNotNull(null, BadRequestException.class, message));
            assertEquals(message, ex.getMessage());
        }

        @Test
        void When_ObjectIsNullAndMessageNotNullAndValidExceptionClass_Expect_ThrowExceptionInstanceOfExceptionClassWithDefaultConstructor() {
            final String NOTICE_MESSAGE_I18N_CODE = "UnauthorizedException.noticeMessage";
            UnauthorizedException ex = assertThrows(UnauthorizedException.class, () -> Precondition.checkNotNull(null, UnauthorizedException.class));
            assertNotNull(NOTICE_MESSAGE_I18N_CODE, ex.getMessage());
        }

        @Test
        void When_ObjectIsNullAndCreateInstanceOfExceptionIsFailed_Expect_ThrowIllegalArgumentException() {
            String message = "custom message";
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> Precondition.checkNotNull(null, BadRequestException.class, message,
                    "invalid", "invalid"));
            assertEquals(message, ex.getMessage());
        }
    }

    @Nested
    class CheckArgument {

        @Test
        void When_FalseArgumentAndMessageNotNullAndEmptyExceptionClass_Expect_ThrowIllegalArgumentExceptionWithMessage() {
            String message = "custom message";
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class,
                    () -> Precondition.checkArgument(false, message));
            assertEquals(message, ex.getMessage());
        }

        @Test
        void When_FalseArgumentAndMessageNotNullAndValidExceptionClass_Expect_ThrowExceptionInstanceOfExceptionClassWithMessage() {
            String message = "custom message";
            BadRequestException ex = assertThrows(BadRequestException.class, () -> Precondition.checkArgument(false, BadRequestException.class, message));
            assertEquals(message, ex.getMessage());
        }

        @Test
        void When_FalseArgumentAndMessageNotNullAndValidExceptionClass_Expect_ThrowExceptionInstanceOfExceptionClassWithDefaultConstructor() {
            final String NOTICE_MESSAGE_I18N_CODE = "UnauthorizedException.noticeMessage";
            UnauthorizedException ex = assertThrows(UnauthorizedException.class, () -> Precondition.checkArgument(false, UnauthorizedException.class));
            assertNotNull(NOTICE_MESSAGE_I18N_CODE, ex.getMessage());
        }

        @Test
        void When_ObjectIsNullAndCreateInstanceOfExceptionIsFailed_Expect_ThrowIllegalArgumentException() {
            String message = "custom message";
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> Precondition.checkNotNull(null, BadRequestException.class, message,
                    "invalid", "invalid"));
            assertEquals(message, ex.getMessage());
        }
    }

}
