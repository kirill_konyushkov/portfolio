package demo.konyushkov.base.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class InMemoryAttemptStoreUT {

    private AttemptStore attemptStore;
    private static final int MAX_ATTEMPT = 10;
    private static final int DURATION = 10;
    private static TimeUnit TIME_UNIT = TimeUnit.HOURS;

    private static String KEY = "test_key";


    @BeforeEach
    void init() {
        this.attemptStore = new InMemoryAttemtStore(MAX_ATTEMPT, DURATION, TIME_UNIT);
    }

    @Nested
    class Increase {
        @Test
        void When_AttemptsIsEmpty_Expect_AttemptsReturnOneAttempt() {
            assertEquals(0, attemptStore.getAttempts(KEY));

            attemptStore.increase(KEY);
            assertEquals(1, attemptStore.getAttempts(KEY));
        }

        @Test
        void When_AttemptsIsNotEmpty_Expect_IncrementAttempts() {
            attemptStore.increase(KEY);
            attemptStore.increase(KEY);
            assertEquals(2, attemptStore.getAttempts(KEY));
        }
    }

    @Nested
    class Invalidate {
        @Test
        void When_AttemptsIsEmpty_Expect_AttemptsInvalidate() {
            attemptStore.invalidate(KEY);
            assertEquals(0, attemptStore.getAttempts(KEY));
        }

        @Test
        void When_AttemptsIsNotEmpty_Expect_AttemptsInvalidate() {
            attemptStore.increase(KEY);
            attemptStore.invalidate(KEY);
            assertEquals(0, attemptStore.getAttempts(KEY));
        }
    }

    @Nested
    class isExceeded {
        @Test
        void When_AttemptsIsEmpty_Expect_ReturnFalse() {
            assertFalse(attemptStore.isExceeded(KEY));
        }

        @Test
        void When_AttemptsLessThenMaxAttempts_Expect_ReturnFalse() {
            IntStream.range(0, MAX_ATTEMPT-1).forEach((i) -> attemptStore.increase(KEY));
            assertFalse(attemptStore.isExceeded(KEY));
        }

        @Test
        void When_AttemptsEqualsMaxAttempts_Expect_ReturnTrue() {
            IntStream.range(0, MAX_ATTEMPT).forEach((i) -> attemptStore.increase(KEY));
            assertTrue(attemptStore.isExceeded(KEY));
        }

        @Test
        void When_AttemptsMoreThenMaxAttempts_Expect_ReturnTrue() {
            IntStream.range(0, MAX_ATTEMPT+1).forEach((i) -> attemptStore.increase(KEY));
            assertTrue(attemptStore.isExceeded(KEY));
        }
    }

    @Nested
    class GetAttempts {
        @Test
        void When_AttemptsIsEmpty_Expect_Return0() {
            assertEquals(0, attemptStore.getAttempts(KEY));
        }

        @Test
        void When_AttemptsLessThenMaxAttempts_Expect_ReturnAttempts() {
            IntStream.range(0, MAX_ATTEMPT-1).forEach((i) -> attemptStore.increase(KEY));
            assertEquals(MAX_ATTEMPT-1, attemptStore.getAttempts(KEY));
        }

        @Test
        void When_AttemptsEqualsMaxAttempts_Expect_ReturnAttempts() {
            IntStream.range(0, MAX_ATTEMPT).forEach((i) -> attemptStore.increase(KEY));
            assertEquals(MAX_ATTEMPT, attemptStore.getAttempts(KEY));
        }

        @Test
        void When_AttemptsMoreThenMaxAttempts_Expect_ReturnAttempts() {
            IntStream.range(0, MAX_ATTEMPT+1).forEach((i) -> attemptStore.increase(KEY));
            assertEquals(MAX_ATTEMPT+1, attemptStore.getAttempts(KEY));
        }
    }
}
