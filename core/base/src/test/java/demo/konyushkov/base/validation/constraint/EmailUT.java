package demo.konyushkov.base.validation.constraint;

import demo.konyushkov.base.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmailUT {


    private static Validator validator;
    private static Entity entity;
    private static final String messageTemplate = "{demo.konyushkov.validation.constraints.Email}";
    private static final String VALID_EMAIL = "valid@email.ru";

    @BeforeAll
    static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Nested
    class Success {
        @AfterEach
        void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertTrue(violations.isEmpty());
        }

        @Test
        void When_IsValid_Expect_ViolationsIsEmpty() {
            createEntity(VALID_EMAIL);
        }

        @Test
        void When_IsNull_Expect_ViolationsIsEmpty() {
            createEntity(null);
        }
    }

    @Nested
    class Failed {
        @AfterEach
        void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertEquals(1, violations.size());
            assertEquals(messageTemplate, violations.iterator().next().getMessageTemplate());
        }

        @Test
        void When_IsEmpty_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("");
        }

        @Test
        void When_OnlyWhitespaces_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity(" ");
        }

        @Test
        void When_StartsWithASpace_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity(" invalid@mail.ru");
        }

        @Test
        void When_StartsWithATab_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("\tinvalid@mail.ru");
        }

        @Test
        void When_EndWithASpace_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("invalid@mail.ru ");
        }

        @Test
        void When_HasSpaceFirstOption_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("invalid @mail.ru");
        }

        @Test
        void When_HasSpaceSecondOption_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("invalid@ mail.ru");
        }

        @Test
        void  When_HasSpaceThirdOption_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("invalid@mail .ru");
        }

        @Test
        void When_HasSpaceFourthOption_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("invalid@mail. ru");
        }

        @Test
        void When_MissingLogin_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("@mail.ru");
        }

        @Test
        void When_MissingDomain_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("@mail.ru");
        }

        @Test
        void When_MissingAt_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("invalid.ru");
        }

        @Test
        void When_MissingDot_Expect_OneViolationWithPatternMessageTemplate() {
            createEntity("invalid@mailru");
        }
    }

    private void createEntity(String email) {
        entity = new Entity(email);
    }

    @Data
    @AllArgsConstructor
    private static class Entity {
        @Email
        String email;
    }

}
