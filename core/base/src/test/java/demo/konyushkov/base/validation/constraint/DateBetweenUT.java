package demo.konyushkov.base.validation.constraint;

import demo.konyushkov.base.validation.constraints.DateBetween;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.LocalDate;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DateBetweenUT {

    private static Validator validator;
    private static Entity entity;
    private static final String messageTemplate = "{demo.konyushkov.validation.constraints.DateBetween}";

    @BeforeAll
    static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Nested
    class Success {

        @AfterEach
        void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertTrue(violations.isEmpty());
        }

        @Test
        void When_DateIsNull_Expect_ViolationsIsEmpty() {
            createEntity(null);
        }

        @Test
        void When_DateBetweenMinAndMax_Expect_ViolationsIsEmpty() {
            createEntity("1999-12-31");
        }

        @Test
        void When_DateEqualsMin_Expect_ViolationsIsEmpty() {
            createEntity("1999-01-01");
        }

        @Test
        void When_DateEqualsMax_Expect_ViolationsIsEmpty() {
            createEntity("2000-01-01");
        }
    }

    @Nested
    class Failed {
        @AfterEach
        void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertEquals(1, violations.size());
            assertEquals(messageTemplate, violations.iterator().next().getMessageTemplate());
            System.out.println(violations.iterator().next());
        }

        @Test
        void When_Date_MoreThanMax_Expect_OneViolationConstraint() {
            createEntity("2001-01-01");
        }

        @Test
        void When_Date_LessThanMin_Expect_OneViolationConstraint() {
            createEntity("1998-01-01");
        }
    }

    private void createEntity(String date) {
        if(date != null) {
            entity = new Entity(LocalDate.parse(date));
        }
        else
            entity = new Entity(null);

    }

    @Data
    @AllArgsConstructor
    private static class Entity {
        @DateBetween(min = "1999-01-01",
                max = "2000-01-01")
        private LocalDate birthday;
    }

}
