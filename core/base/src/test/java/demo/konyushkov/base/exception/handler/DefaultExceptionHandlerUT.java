package demo.konyushkov.base.exception.handler;

import demo.konyushkov.base.exception.*;
import demo.konyushkov.base.exception.dto.ErrorDetails;
import demo.konyushkov.base.validation.constraints.Email;
import demo.konyushkov.base.validation.constraints.PasswordHash;
import demo.konyushkov.base.validation.constraints.SpELAssert;
import demo.konyushkov.test.tools.ValidatorTest;
import lombok.Data;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.lang.reflect.Method;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DefaultExceptionHandlerUT {

    @Mock MessageSourceAccessor messageSourceAccessor;
    private DefaultExceptionHandler exceptionHandler;
    private static ErrorDetails errorDetails;

    private static final String CVE_MESSAGE = "{DefaultExceptionHandler.ConstraintViolationException.message}";

    private static final String NOT_FOUND_MESSAGE = "{NotFoundException.message}";

    private static final String INTERNAL_SERVER_MESSAGE = "{InternalServerException.message}";

    private static final String UNAUTHORIZED_MESSAGE = "{UnauthorizedException.message}";

    private static final String FORBIDDEN_MESSAGE = "{ForbiddenException.message}";

    private static final String BAD_REQUEST_TITLE = "{BadRequestException.noticeTitle}";
    private static final String BAD_REQUEST_MESSAGE = "{BadRequestException.message}";

    private static final String ALL_EXCEPTION_MESSAGE = "{DefaultExceptionHandler.InternalServerError.message}";

    @BeforeEach
    void init() {
        exceptionHandler = new DefaultExceptionHandler(messageSourceAccessor){};
        Mockito.when(messageSourceAccessor.getMessage(anyString(), anyString())).thenAnswer(i -> i.getArguments()[1]);
    }

    @Nested
    class HandleException {

        @Test
        void When_HandleMethodArgumentNotValid_Expect_ReturnErrorDetailsWithBadRequestStatusAndMessageWithConstraintViolationExceptionInfoAndMapWithInvalidFieldsNameAndDescription() {
            MethodArgumentNotValidException ex = PrepareTools.prepareMethodArgumentNotValidException();
            errorDetails = exceptionHandler.handleException(ex);

            AssertionTools.assertErrorDetails(HttpStatus.BAD_REQUEST, CVE_MESSAGE, PrepareTools.prepareErrors());
        }

        @Test
        void When_HandleConstraintViolationException_Expect_ReturnErrorDetailsWithBadRequestStatusAndMessageWithConstraintViolationExceptionInfoAndMapWithInvalidFieldsNameAndDescription() {
            ConstraintViolationException ex = PrepareTools.prepareConstraintViolationException();
            errorDetails = exceptionHandler.handleException(ex);

            AssertionTools.assertErrorDetails(HttpStatus.BAD_REQUEST, CVE_MESSAGE, PrepareTools.prepareErrors());
        }

        @Test
        void When_HandleRequestExceptionWithPureMessage_Expect_ReturnErrorDetailsWithPureMessage() {
            String pureNotFoundMessage = "Pure not found message";
            NotFoundException ex = new NotFoundException(pureNotFoundMessage);
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.NOT_FOUND, pureNotFoundMessage);
        }

        @Test
        void When_HandleNotFoundException_Expect_ReturnErrorDetailsWithNotFoundStatusAndMessageWithNotFoundExceptionInfo() {
            NotFoundException ex = new NotFoundException(NOT_FOUND_MESSAGE);
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.NOT_FOUND, NOT_FOUND_MESSAGE);
        }

        @Test
        void When_HandleInternalServerException_Expect_ReturnErrorDetailsWithInternalServerErrorStatusAndMessageWithInternalServerErrorInfo() {
            InternalServerException ex = new InternalServerException(INTERNAL_SERVER_MESSAGE);
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_MESSAGE);
        }

        @Test
        void When_HandleUnauthorizedException_Expect_ReturnErrorDetailsWithUnauthorizedStatusAndMessageWithUnauthorizedExceptionInfo() {
            UnauthorizedException ex = new UnauthorizedException();
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_MESSAGE);
        }

        @Test
        void When_HandleForbiddenException_Expect_ReturnErrorDetailsWithForbiddenStatusAndMessageWithForbiddenExceptionInfo() {
            ForbiddenException ex = new ForbiddenException();
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.FORBIDDEN, FORBIDDEN_MESSAGE);
        }

        @Test
        void When_HandleRequestExceptionWithCause_Expect_ReturnReturnErrorDetailsWithNotFoundStatusAndMessageWithNotFoundExceptionInfo() {
            NotFoundException cause = new NotFoundException(NOT_FOUND_MESSAGE);
            InternalServerException ex = new InternalServerException(INTERNAL_SERVER_MESSAGE, cause);
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, INTERNAL_SERVER_MESSAGE);
        }

        @Test
        void When_HandleBadRequestException_Expect_ReturnReturnErrorDetailsWithBadRequestStatusAndMessageWithBadRequestExceptionInfo() {
            BadRequestException ex = new BadRequestException(BAD_REQUEST_MESSAGE);
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.BAD_REQUEST, BAD_REQUEST_MESSAGE);
        }

        @Test
        void When_CauseContainsDetails_Expect_ReturnReturnErrorDetailsWithExceptionInfoAndDetails() {
            Map<String, Set<String>> details = Map.ofEntries(
                    new AbstractMap.SimpleEntry<>("field", Set.of(BAD_REQUEST_TITLE))
            );

            BadRequestException ex = new BadRequestException(BAD_REQUEST_MESSAGE, details);
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.BAD_REQUEST, BAD_REQUEST_MESSAGE, details);
        }

        @Test
        void When_CauseNotContainsDetails_Expect_ReturnReturnErrorDetailsWithExceptionInfoAndWithoutDetails() {
            BadRequestException ex = new BadRequestException(BAD_REQUEST_MESSAGE);
            errorDetails = exceptionHandler.handleException(ex);
            AssertionTools.assertErrorDetails(HttpStatus.BAD_REQUEST, BAD_REQUEST_MESSAGE, null);
        }

        @Test
        void When_HandleOtherException_Expect_ThrowNewInternalServerException() {
            Exception exception = new Exception("Exception message");
            errorDetails = exceptionHandler.handleException(exception);
            AssertionTools.assertErrorDetails(HttpStatus.INTERNAL_SERVER_ERROR, ALL_EXCEPTION_MESSAGE);
        }
    }

    private static class AssertionTools {

        static void assertErrorDetails(HttpStatus expectedHttpStatus, String expectedMessage) {
            assertErrorDetails(expectedHttpStatus, expectedMessage, null);
        }

        static void assertErrorDetails(HttpStatus expectedHttpStatus,
                                       String expectedMessage,
                                       Map<String, Set<String>> expectedErrors) {
            ErrorDetails errorDetails = DefaultExceptionHandlerUT.errorDetails;
            assertEquals(expectedHttpStatus, DefaultExceptionHandlerUT.errorDetails.getStatus());
            assertEquals(expectedMessage, errorDetails.getMessage());
            assertEquals(expectedErrors, errorDetails.getContent());
        }
    }

    private static class PrepareTools {
        private static Validator validator = ValidatorTest.getValidator();

        static Entity createEntity() {
            Entity entity = new Entity();
            entity.setEmail("not_valid_email");
            entity.setPassword("not_valid_password");
            entity.setChange("change");
            entity.setRepeat("not_repeat_change");

            return entity;
        }

        static ConstraintViolationException prepareConstraintViolationException() {
            Entity entity = createEntity();
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            return new ConstraintViolationException(violations);
        }

        static MethodArgumentNotValidException prepareMethodArgumentNotValidException() {

            try {
                Entity entity = createEntity();
                Method setEmail = entity.getClass().getMethod("setEmail", String.class);
                MethodParameter methodParameter = MethodParameter.forParameter(setEmail.getParameters()[0]);
                SpringValidatorAdapter adapter = new SpringValidatorAdapter(validator);
                BindingResult bindingResult= new BeanPropertyBindingResult(entity, "entity");
                adapter.validate(entity, bindingResult);

                return new MethodArgumentNotValidException(methodParameter, bindingResult);
            } catch (NoSuchMethodException e) {
               throw new RuntimeException(e);
            }
        }

        static Map<String, Set<String>> prepareErrors() {
            Map<String, Set<String>> errors = new HashMap<>();
            errors.put("email", new HashSet<>(Collections.singletonList("{demo.konyushkov.validation.constraints.Email}")));
            errors.put("password", new HashSet<>(Collections.singletonList("{demo.konyushkov.validation.constraints.PasswordHash}")));
            errors.put("repeat", new HashSet<>(Collections.singletonList("{Entity.crossfield.changeAndRepeat}")));

            return errors;
        }
    }

    @Data
    @SpELAssert(value = "change.equals(repeat)",
            applyIf = "change != null && repeat != null",
            message = "{Entity.crossfield.changeAndRepeat}",
            propertyPath = "repeat")
    private static class Entity {

        @Email
        private String email;

        @PasswordHash
        private String password;

        private String change;
        private String repeat;
    }
}
