package demo.konyushkov.base.validation.constraint;

import demo.konyushkov.base.validation.constraints.Password;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PasswordUT {


    private static Validator validator;
    private static Entity entity;
    private static final String messageTemplate = "{demo.konyushkov.validation.constraints.Password}";
    private static final String VALID_PASSWORD = "valid134";

    @BeforeAll
    static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Nested
    class Success {

        @AfterEach
        void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertTrue(violations.isEmpty());
        }

        @Test
        void When_PasswordIsContainsNumberAndLowercaseLettersAndUppercaseLetters_Expect_ViolationsIsEmpty() {
            createEntity(VALID_PASSWORD);
        }

        @Test
        void When_PasswordIsContainsNumberAndOnlyLowercaseLetters_Expect_ViolationsIsEmpty() {
            createEntity(VALID_PASSWORD.toLowerCase());
        }

        @Test
        void When_PasswordIsContainsNumberAndOnlyUppercaseLetters_Expect_ViolationsIsEmpty() {
            createEntity(VALID_PASSWORD.toUpperCase());
        }

        @Test
        void When_PasswordIsContainsNumberAndLettersLettersAndSpecialSymbols_Expect_ViolationsIsEmpty() {
            createEntity("1d" + "#$%&'()*+,\\-.\\\\");
        }

        @Test
        void When_PasswordIsContainsNumberAndLettersLettersAndSpecialSymbols2_Expect_ViolationsIsEmpty() {
            createEntity("1d" + "/:;<>=?@\\[\\]{}^_`~");
        }

        @Test
        void When_IsNull_Expect_ViolationsIsEmpty() {
            createEntity(null);
        }
    }

    @Nested
    class Failed {

        @AfterEach
        void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertEquals(1, violations.size());
            assertEquals(messageTemplate, violations.iterator().next().getMessageTemplate());
        }

        @Test
        void When_NotContainsDigit_Expect_OneViolationConstraint() {
            createEntity("notDigit");
        }

        @Test
        void When_ContainsNotEnglishLetters_Expect_OneViolationConstraint() {
            createEntity("1руссиш");
        }

        @Test
        void When_NotContainsLetters_Expect_OneViolationConstraint() {
            createEntity("123456");
        }

        @Test
        void When_StartWithSpaces_Expect_OneViolationConstraint() {
            createEntity(" " + VALID_PASSWORD);
        }

        @Test
        void When_StartWithTab_Expect_OneViolationConstraint() {
            createEntity("\t" + VALID_PASSWORD);
        }

        @Test
        void When_EndWithSpace_Expect_OneViolationConstraint() {
            createEntity(VALID_PASSWORD + " ");
        }

        @Test
        void When_EndWithTab_Expect_OneViolationConstraint() {
            createEntity(VALID_PASSWORD + " \t");
        }

        @Test
        void When_ContainsSpace_Expect_OneViolationConstraint() {
            createEntity(VALID_PASSWORD.substring(0, 2) + " " + VALID_PASSWORD.substring(4));
        }

        @Test
        void When_ContainsTab_Expect_OneViolationConstraint() {
            createEntity(VALID_PASSWORD.substring(0, 2) + "\t" + VALID_PASSWORD.substring(4));
        }
    }

    private void createEntity(String password) {
        entity = new Entity(password);
    }

    @Data
    @AllArgsConstructor
    private static class Entity {
        @Password
        String password;
    }

}
