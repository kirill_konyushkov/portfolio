package demo.konyushkov.base.validation.constraint;

import demo.konyushkov.base.validation.constraints.PasswordHash;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PasswordHashUT {


    private static Validator validator;
    private static Entity entity;
    private static final String messageTemplate = "{demo.konyushkov.validation.constraints.PasswordHash}";
    private static final String VALID_HASH = "$2a$04$KXKrdHLqqWXex11KGXqHreJRD3jPIHF1y07s.f8aVc0qo/nj5y8Gi";


    @BeforeAll
    static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Nested
    class Success {

        @AfterEach
        void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertTrue(violations.isEmpty());
        }

        @Test
        void When_PasswordHashIsValid_Expect_ViolationsIsEmpty() {
            createEntity(VALID_HASH);
        }

        @Test
        void When_IsNull_Expect_ViolationsIsEmpty() {
            createEntity(null);
        }
    }

    @Nested
    class Failed {
        @AfterEach
        void test() {
            Set<ConstraintViolation<Entity>> violations = validator.validate(entity);
            assertEquals(1, violations.size());
            assertEquals(messageTemplate, violations.iterator().next().getMessageTemplate());
        }

        @Test
        void When_ShorterThen60Symbols_Expect_OneViolationConstraint() {
            createEntity(VALID_HASH.substring(0, 59));
        }

        @Test
        void When_StartNotWith$2_Expect_OneViolationConstraint() {
            createEntity(VALID_HASH.replace("$2", "$1"));
        }

        @Test
        void When_StartWithSpaces_Expect_OneViolationConstraint() {
            createEntity(" " + VALID_HASH.substring(0, 59));
        }

        @Test
        void When_StartWithTab_Expect_OneViolationConstraint() {
            createEntity("\t" + VALID_HASH.substring(0, 59));
        }

        @Test
        void When_EndWithSpace_Expect_OneViolationConstraint() {
            createEntity(VALID_HASH.substring(0, 59) + " ");
        }

        @Test
        void When_EndWithTab_Expect_OneViolationConstraint() {
            createEntity(VALID_HASH.substring(0, 59) + " \t");
        }

        @Test
        void When_ContainsSpace_Expect_OneViolationConstraint() {
            createEntity(VALID_HASH.substring(0, 5) + " " + VALID_HASH.substring(7, 60));
        }

        @Test
        void When_ContainsTab_Expect_OneViolationConstraint() {
            createEntity(VALID_HASH.substring(0, 5) + "\t" + VALID_HASH.substring(7, 60));
        }
    }

    private void createEntity(String password) {
        entity = new Entity(password);
    }

    @Data
    @AllArgsConstructor
    private static class Entity {
        @PasswordHash
        String password;
    }

}
