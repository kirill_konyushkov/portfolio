package demo.konyushkov.test.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile(Profiles.WEB_API_IT)
@PropertySource("classpath:application-webapp-test.properties")
public class WebAppPropertySourceIT {}
