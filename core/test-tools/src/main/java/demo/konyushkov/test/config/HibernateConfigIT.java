package demo.konyushkov.test.config;

import demo.konyushkov.database.config.AbstractHibernateConfig;
import net.ttddyy.dsproxy.listener.logging.SLF4JLogLevel;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;


@Configuration
@Profile(value = {Profiles.REPOSITORY_IT})
public class HibernateConfigIT extends AbstractHibernateConfig {

    @Bean
    @Primary
    public DataSource dataSource(Environment environment) {
        ProxyDataSourceBuilder builder = new ProxyDataSourceBuilder();
        builder.logQueryBySlf4j(SLF4JLogLevel.INFO)
                .dataSource(super.dataSource(environment)).countQuery();

        return builder.build();
    }
}
