package demo.konyushkov.test.tools;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class JsonTest<T> {

    private static ObjectMapper mapper;

    static  {
        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.registerModule(new JavaTimeModule());
    }

    @SneakyThrows
    public void testSerialization(T instance, Class view, String ...expectedFields) {
        String json = mapper.writerWithView(view)
                .withDefaultPrettyPrinter()
                .writeValueAsString(instance);

        System.out.println(json);

        JsonNode root = mapper.readTree(json);
        assertEquals(expectedFields.length, root.size());
        Arrays.stream(expectedFields)
                .forEach(field -> assertNotNull(root.get(field)));
    }

}
