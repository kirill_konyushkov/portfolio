package demo.konyushkov.test.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile(Profiles.REPOSITORY_IT)
@PropertySource("classpath:application-repository-test.properties")
public class RepositoryPropertySourceIT {}
