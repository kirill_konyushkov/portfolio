package demo.konyushkov.test.tools;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
@Getter
public class OAuth2Helper {

    @Value("${app.authorization_server.url}")
    private String authServerHost;
    @Value("${oauth.client_id}")
    private String clientId;
    @Value("${oauth.client_secret}")
    private String clientSecret;
    @Value("${oauth.grant_type}")
    private String grantType;

    public String obtainAccessToken(String username, String password) throws Exception {
        ResourceOwnerPasswordResourceDetails resourceDetails = getResourceDetails(username, password);
        OAuth2RestOperations restOperations = getRestOperations(resourceDetails);
        return restOperations.getAccessToken().getValue();
    }

    private ResourceOwnerPasswordResourceDetails getResourceDetails(String username, String password) {
        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername(username);
        resourceDetails.setPassword(password);
        resourceDetails.setAccessTokenUri(authServerHost + "/oauth/token");
        resourceDetails.setClientId(clientId);
        resourceDetails.setClientSecret(clientSecret);
        resourceDetails.setGrantType(grantType);
        return resourceDetails;
    }

    private OAuth2RestOperations getRestOperations(ResourceOwnerPasswordResourceDetails resourceDetails) {
        AccessTokenRequest atr = new DefaultAccessTokenRequest();
        return new OAuth2RestTemplate(resourceDetails, new DefaultOAuth2ClientContext(atr));
    }

    public String getAuthenticationClientDetailsHeader() {
        return "Basic " + Base64.getEncoder().encodeToString((clientId + ":" + clientSecret).getBytes());
    }
}
