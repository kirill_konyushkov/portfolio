package demo.konyushkov.test.config;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { BaseConfigIT.class,
        HibernateConfigIT.class, RepositoryPropertySourceIT.class})
@ActiveProfiles(Profiles.REPOSITORY_IT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Sql(scripts = {"classpath:drop_schema.sql", "classpath:schema.sql", "classpath:data.sql"})
public class RepositoryContextIT {}
