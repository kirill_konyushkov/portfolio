package demo.konyushkov.service;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.base.exception.NotFoundException;
import demo.konyushkov.dto.ConfirmRegistrationDto;
import demo.konyushkov.dto.RegistrationDto;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.service.EmailClient;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.TransactionSystemException;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class RegistrationServiceUT {

    @Mock private UserService userService;
    @Mock private PasswordEncoder passwordEncoder;
    @Mock private ConfirmCodeService confirmCodeService;
    @Mock private EmailClient emailClient;
    private RegistrationService registrationService;

    private String frontendUrl = "localhost:8081";

    @BeforeEach
    void init() {
        this.registrationService = new RegistrationServiceImpl(userService, passwordEncoder,
                confirmCodeService, emailClient);

        ((RegistrationServiceImpl) registrationService).frontendUrl = frontendUrl;
    }

    @Nested
    class Registration {

        private RegistrationDto registrationDto;
        private ConfirmCode confirmCode;

        @BeforeEach
        void init() {
            final String password = "new_password123";
            final String email = "newemail@mail.ru";
            registrationDto = new RegistrationDto();
            registrationDto.setName("Кирилл");
            registrationDto.setPassword(password);
            registrationDto.setConfirmPassword(password);
            registrationDto.setEmail(email);
        }

        @Test
        void When_ValidRegistrationDto_Expect_CreateNewUserSaveConfirmCodeToDbAndSendMessageToEmail() {
            Mockito.when(userService.existsByEmail(registrationDto.getEmail())).thenReturn(false);

            doAnswer(returnsFirstArg()).when(userService).save(any(User.class));

            Mockito.when(confirmCodeService.prepareAndSave(any(User.class), eq(ConfirmCode.Action.REGISTRATION)))
                    .then(i -> {
                        User user = (User) i.getArguments()[0];
                        confirmCode = ConfirmCode.builder()
                                .code(UUID.randomUUID())
                                .user(user)
                                .action(ConfirmCode.Action.REGISTRATION)
                                .build();
                        return confirmCode;
                    });

            registrationService.registration(registrationDto);

            ArgumentCaptor<HtmlMail> argumentMail = ArgumentCaptor.forClass(HtmlMail.class);
            verify(emailClient).send(argumentMail.capture());
            HtmlMail mail = argumentMail.getValue();

            assertEquals(registrationDto.getEmail(), mail.getTo());
            assertEquals(ConfirmCode.Action.REGISTRATION.name().toLowerCase(), mail.getTemplate());
            assertEquals(Map.of(
                    "confirmCode", confirmCode.getCode(),
                    "userId", confirmCode.getUser().getId(),
                    "frontendUrl", frontendUrl),
                    mail.getVariables());
        }

        @Test
        void When_EmailBusy_Expect_ThrowBadRequestExceptionWithBusyMessage() {
            Mockito.when(userService.existsByEmail(registrationDto.getEmail())).thenReturn(true);

            BadRequestException exception = assertThrows(BadRequestException.class, () -> registrationService.registration(registrationDto));
            assertEquals(Map.of("email", Set.of("{RegistrationServiceImpl.registration.emailBusy}")), exception.getDetails());
            assertEquals("{BadRequestException.message}", exception.getMessage());
        }

        @Test
        void When_FailedSaveUser_Expect_ThrowInternalServerException() {
            Mockito.when(userService.existsByEmail(anyString())).thenReturn(false);
            String exMessage = "Failed save user";
            Mockito.when(userService.save(any(User.class))).thenThrow(new TransactionSystemException(exMessage));

            InternalServerException ex = assertThrows(InternalServerException.class, () -> registrationService.registration(registrationDto));
            assertEquals("{RegistrationServiceImpl.registration.internalServerException}", ex.getMessage());
            assertEquals(exMessage, ex.getCause().getMessage());
        }

        @Test
        void When_FailedSaveConfirmCode_Expect_ThrowInternalServerException() {
            String exMessage = "Failed save code to Db";

            Mockito.when(userService.existsByEmail(anyString())).thenReturn(false);
            Mockito.when(userService.save(any(User.class))).thenAnswer(returnsFirstArg());
            Mockito.when(confirmCodeService.prepareAndSave(any(User.class), any(ConfirmCode.Action.class))).thenThrow(new TransactionSystemException(exMessage));

            InternalServerException ex = assertThrows(InternalServerException.class, () -> registrationService.registration(registrationDto));
            assertEquals("{RegistrationServiceImpl.registration.internalServerException}", ex.getMessage());
            assertEquals(exMessage, ex.getCause().getMessage());
        }

        @Test
        void When_FailedSendMail_Expect_ThrowInternalServerException() {
            Mockito.when(userService.existsByEmail(registrationDto.getEmail())).thenReturn(false);

            doAnswer(returnsFirstArg()).when(userService).save(any(User.class));

            Mockito.when(confirmCodeService.prepareAndSave(any(User.class), eq(ConfirmCode.Action.REGISTRATION)))
                    .then(i -> {
                        User user = (User) i.getArguments()[0];
                        confirmCode = ConfirmCode.builder()
                                .code(UUID.randomUUID())
                                .user(user)
                                .action(ConfirmCode.Action.REGISTRATION)
                                .build();
                        return confirmCode;
                    });

            String exMessage = "Failed send email";
            Mockito.doThrow(new RuntimeException(exMessage)).when(emailClient).send(any(HtmlMail.class));

            InternalServerException ex = assertThrows(InternalServerException.class, () -> registrationService.registration(registrationDto));
            assertEquals("{RegistrationServiceImpl.registration.internalServerException}", ex.getMessage());
            assertEquals(exMessage, ex.getCause().getMessage());
        }

    }

    @Nested
    class ConfirmRegistration {

        private ConfirmRegistrationDto confirmRegistrationDto;
        private User user;

        @BeforeEach
        void init() {
            user = new User();
            user.setId(UUID.randomUUID());
            user.setEmail("demo_test-user@mail.ru");
            user.setPassword("passwordhash");

            confirmRegistrationDto = new ConfirmRegistrationDto();
            confirmRegistrationDto.setConfirmCode(UUID.randomUUID());
            confirmRegistrationDto.setUserId(user.getId());
        }

        @Test
        void When_ValidConfirmRegistrationDto_When_SaveEnabledUserAndRemoveOldConfirmCode() {
            ConfirmCode validCode = getValid();
            AtomicBoolean confirmUser = new AtomicBoolean(false);
            AtomicBoolean deleteOldCode = new AtomicBoolean(false);

            Mockito.when(confirmCodeService.findByCode(confirmRegistrationDto.getConfirmCode())).thenReturn(validCode);
            Mockito.when(userService.findById(confirmRegistrationDto.getUserId())).thenReturn(user);

            Mockito.when(userService.save(user)).then(i -> {
                confirmUser.set(true);
                return user;
            });

            Mockito.doAnswer((i) -> {
                deleteOldCode.set(true);
                return null;
            }).when(confirmCodeService).deleteByCode(validCode.getCode());

            registrationService.confirm(confirmRegistrationDto);

            ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
            verify(userService).save(argument.capture());
            User value = argument.getValue();

            assertTrue(value.getEnabled());
            assertTrue(confirmUser.get());
            assertTrue(deleteOldCode.get());
        }

        @Test
        void When_ConfirmCodeIsNotExist_When_ThrowInternalServerExceptionWithNotValidMessage () {
            Mockito.when(confirmCodeService.findByCode(confirmRegistrationDto.getConfirmCode()))
                    .thenThrow(new NotFoundException("{ConfirmCodeServiceImpl.findByCode.NotFound}"));

            BadRequestException ex = assertThrows(BadRequestException.class,
                    () -> registrationService.confirm(confirmRegistrationDto));
            assertEquals("{RegistrationServiceImpl.confirm.notValid}", ex.getMessage());
            assertEquals("{ConfirmCodeServiceImpl.findByCode.NotFound}", ex.getCause().getMessage());
        }

        @Test
        void When_InvalidUserId_When_ThrowInternalServerExceptionWithNotValidMessage () {
            ConfirmCode validCode = getValid();
            //set invalid userId
            confirmRegistrationDto.setUserId(UUID.randomUUID());

            Mockito.when(confirmCodeService.findByCode(confirmRegistrationDto.getConfirmCode())).thenReturn(validCode);


            BadRequestException ex = assertThrows(BadRequestException.class,
                    () -> registrationService.confirm(confirmRegistrationDto));
            assertEquals("{RegistrationServiceImpl.confirm.notValid}", ex.getMessage());

            String expectedMessage = String.format("Illegal user. Expected: %s, but actual: %s", validCode.getUserId(), confirmRegistrationDto.getUserId());
            assertEquals(expectedMessage, ex.getCause().getMessage());
        }

        @Test
        void When_InvalidAction_When_ThrowInternalServerExceptionWithNotValidMessage () {
            ConfirmCode validCode = getValid();
            //set invalid action
            validCode.setAction(ConfirmCode.Action.RESTORE_PASSWORD);

            Mockito.when(confirmCodeService.findByCode(confirmRegistrationDto.getConfirmCode())).thenReturn(validCode);


            BadRequestException ex = assertThrows(BadRequestException.class,
                    () -> registrationService.confirm(confirmRegistrationDto));
            assertEquals("{RegistrationServiceImpl.confirm.notValid}", ex.getMessage());

            String expectedMessage = String.format("Illegal action. Expected: %s, but actual: %s", ConfirmCode.Action.REGISTRATION, validCode.getAction());
            assertEquals(expectedMessage, ex.getCause().getMessage());
        }

        @Test
        void When_AccountNotExists_When_ThrowInternalServerExceptionWithFailedUpdateMessage () {
            ConfirmCode validCode = getValid();

            Mockito.when(confirmCodeService.findByCode(confirmRegistrationDto.getConfirmCode())).thenReturn(validCode);
            Mockito.when(userService.findById(confirmRegistrationDto.getUserId()))
                    .thenThrow(new NotFoundException("{UserServiceImpl.findById.NotFound}"));

            InternalServerException ex = assertThrows(InternalServerException.class,
                    () -> registrationService.confirm(confirmRegistrationDto));
            assertEquals("{RegistrationServiceImpl.confirm.failedConfirm}", ex.getMessage());
            assertEquals("{UserServiceImpl.findById.NotFound}", ex.getCause().getMessage());
        }

        @Test
        void When_FailedSaveAccountWithEnabledTrue_When_ThrowInternalServerExceptionWithFailedUpdateMessage () {
            ConfirmCode validCode = getValid();

            Mockito.when(confirmCodeService.findByCode(confirmRegistrationDto.getConfirmCode())).thenReturn(validCode);
            Mockito.when(userService.findById(confirmRegistrationDto.getUserId())).thenReturn(user);

            String exMessage = "failed update";
            Mockito.when(userService.save(user)).thenThrow(new TransactionSystemException(exMessage));

            InternalServerException ex = assertThrows(InternalServerException.class,
                    () -> registrationService.confirm(confirmRegistrationDto));
            assertEquals("{RegistrationServiceImpl.confirm.failedConfirm}", ex.getMessage());
            assertEquals(exMessage, ex.getCause().getMessage());
        }

        @Test
        void When_FailedDeleteConfirmCode_When_SaveUserWithNewPasswordAndCatchException () {
            ConfirmCode validCode = getValid();
            AtomicBoolean confirmUser = new AtomicBoolean(false);

            Mockito.when(confirmCodeService.findByCode(confirmRegistrationDto.getConfirmCode())).thenReturn(validCode);
            Mockito.when(userService.findById(confirmRegistrationDto.getUserId())).thenReturn(user);

            Mockito.when(userService.save(user)).then(i -> {
                confirmUser.set(true);
                return user;
            });

            String exMessage = "failed update";
            Mockito.doThrow(new TransactionSystemException(exMessage))
                    .when(confirmCodeService).deleteByCode(confirmRegistrationDto.getConfirmCode());

            registrationService.confirm(confirmRegistrationDto);

            ArgumentCaptor<User> argument = ArgumentCaptor.forClass(User.class);
            verify(userService).save(argument.capture());
            User value = argument.getValue();

            assertTrue(value.getEnabled());
            assertTrue(confirmUser.get());
        }

        private ConfirmCode getValid() {
            ConfirmCode confirmCode = new ConfirmCode();
            confirmCode.setCode(confirmRegistrationDto.getConfirmCode());
            confirmCode.setAction(ConfirmCode.Action.REGISTRATION);

            confirmCode.setUser(user);
            confirmCode.setUserId(user.getId());

            return confirmCode;
        }
    }

}