package demo.konyushkov.service;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.base.exception.NotFoundException;
import demo.konyushkov.base.exception.UnauthorizedException;
import demo.konyushkov.dto.PasswordDto;
import demo.konyushkov.model.Gender;
import demo.konyushkov.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.ConstraintViolationException;
import java.security.Principal;
import java.time.LocalDate;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.spy;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AccountServiceImplUT {

    @Mock private Principal principal;
    @Mock private UserService userService;
    @Mock private PasswordEncoder passwordEncoder;
    private AccountService accountService;
    private AccountService spy;

    @BeforeEach
    void init() {
        spy = spy(AccountService.class);
        this.accountService = new AccountServiceImpl(userService, passwordEncoder, spy);
    }

    @Nested
    class FindByPrincipal {

        private User user;
        private final UUID ID = UUID.randomUUID();

        @BeforeEach
        void init() {
            user = new User();
            user.setId(ID);
            user.setEmail("demo_test-user@mail.ru");
            user.setPassword("password_hash");
        }

        @Test
        void When_ValidPrincipalAndUserExist_Expect_ReturnUser() {
            Mockito.when(principal.getName()).thenReturn(ID.toString());
            Mockito.when(userService.findById(ID)).thenReturn(user);

            User account = accountService.findByPrincipal(principal);
            assertEquals(user, account);
        }

        @Test
        void When_ValidPrincipalAndUserNotExist_Expect_ThrowInternalServerException() {
            String notFoundMessage = "User not found";
            Mockito.when(principal.getName()).thenReturn(ID.toString());
            Mockito.when(userService.findById(ID)).thenThrow(new NotFoundException(notFoundMessage));

            InternalServerException exception = assertThrows(InternalServerException.class, () -> accountService.findByPrincipal(principal));
            assertEquals(notFoundMessage, exception.getCause().getMessage());
        }

        @Test
        void When_PrincipalIsNull_Expect_ThrowUnauthorizedException() {
            assertThrows(UnauthorizedException.class, () -> accountService.findByPrincipal(null));
        }
    }

    @Nested
    class UpdatePassword {
        private PasswordDto passwordDto;
        private User user;

        @BeforeEach
        void init() {
            user = new User();
            user.setId(UUID.randomUUID());
            user.setEmail("demo_test-user@mail.ru");
            user.setPassword("password_hash");

            String newPassword = "new_password";
            passwordDto = new PasswordDto();
            passwordDto.setCurrentPassword("current_password");
            passwordDto.setNewPassword(newPassword);
            passwordDto.setConfirmPassword(newPassword);
        }

        @Test
        void When_ValidPasswordDto_Expect_ChangePassword() {
            Mockito.when(spy.findByPrincipal(principal)).thenReturn(user);
            Mockito.when(passwordEncoder.matches(passwordDto.getCurrentPassword(), user.getPassword())).thenReturn(true);
            String newPasswordHash = "new_password_hash";
            Mockito.when(passwordEncoder.encode(passwordDto.getNewPassword())).thenReturn(newPasswordHash);
            accountService.updatePassword(passwordDto, principal);
            assertEquals(newPasswordHash, user.getPassword());
        }

        @Test
        void When_CurrentPasswordOfPasswordDtoNotMatchesWithUserPassword_Expect_ThrowBadRequestException() {
            Mockito.when(spy.findByPrincipal(principal)).thenReturn(user);
            Mockito.when(passwordEncoder.matches(user.getPassword(), passwordDto.getCurrentPassword())).thenReturn(false);
            BadRequestException badRequestException = assertThrows(BadRequestException.class, () -> accountService.updatePassword(passwordDto, principal));
            assertEquals("{BadRequestException.message}", badRequestException.getMessage());
            assertEquals(Map.of("currentPassword", Set.of("{AccountServiceImpl.updatePassword.currentPasswordNotMatches}")), badRequestException.getDetails());
        }

        @Test
        void When_NotFoundUserByPrincipal_Expect_ThrowUnauthorizedException() {
            Mockito.when(spy.findByPrincipal(principal)).thenThrow(UnauthorizedException.class);
            assertThrows(UnauthorizedException.class, () -> accountService.updatePassword(passwordDto, principal));
        }
    }

    @Nested
    class Update {

        private User oldUser;
        private User inputUser;

        @BeforeEach
        void init() {
            oldUser = getOld();
            inputUser = getInput();

            Mockito.when(spy.findByPrincipal(principal)).thenReturn(oldUser);
            Mockito.when(userService.save(oldUser)).then(returnsFirstArg());
        }

        @Test
        void When_ChangeId_Expect_SuccessUpdateButIgnoreTheChangeOfIdAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(oldUser.getId(), result.getId());
        }

        @Test
        void When_ChangePassword_Expect_SuccessUpdateButIgnoreTheChangeOfPasswordAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(oldUser.getPassword(), result.getPassword());
        }

        @Test
        void When_ChangeEmail_Expect_SuccessUpdateButIgnoreTheChangeOfEmailAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(oldUser.getEmail(), result.getEmail());
        }

        @Test
        void When_ChangeName_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(inputUser.getName(), result.getName());
        }

        @Test
        void When_ChangeSurname_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(inputUser.getSurname(), result.getSurname());
        }


        @Test
        void When_ChangePhoto_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(inputUser.getPhoto(), result.getPhoto());
        }

        @Test
        void When_ChangeBirthday_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(inputUser.getBirthday(), result.getBirthday());
        }

        @Test
        void When_ChangeNote_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(inputUser.getNote(), result.getNote());
        }

        @Test
        void When_ChangeGender_Expect_SuccessUpdateAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(inputUser.getGender(), result.getGender());
        }

        @Test
        void When_ChangeEnabled_Expect_SuccessUpdateButIgnoreTheChangeOfEnabledAndReturnUpdatedProfile() {
            User result = accountService.updatePersonal(inputUser, principal);
            assertEquals(oldUser.getEnabled(), result.getEnabled());
        }

        @Test
        void When_NotFoundUserByPrincipal_Expect_ThrowUnauthorizedException() {
            Mockito.when(spy.findByPrincipal(principal)).thenThrow(UnauthorizedException.class);
            assertThrows(UnauthorizedException.class, () -> accountService.updatePersonal(inputUser, principal));
        }

        @Test
        void When_TryUpdateProfileFilledInvalidData_Expect_ThrowConstraintViolationException() {
            Mockito.doThrow(new ConstraintViolationException("Constraint violation exception", null)).when(userService).save(any());
            Throwable throwable = assertThrows(ConstraintViolationException.class, () -> accountService.updatePersonal(inputUser, principal));
            assertEquals("Constraint violation exception", throwable.getLocalizedMessage());
        }

        private User getOld() {
            User profile = new User();
            profile.setId(UUID.randomUUID());
            profile.setName("Кирилл");
            profile.setSurname("Конюшков");
            profile.setPhoto("/img/photo.jpg");
            profile.setEmail("demo_test-user@mail.ru");
            profile.setPassword("password_hash");
            profile.setGender(Gender.MALE);
            profile.setNote("note");
            profile.setBirthday(LocalDate.parse("1996-01-27"));
            profile.setEnabled(true);
            return profile;
        }

        private User getInput() {
            User profile = new User();
            profile.setId(UUID.randomUUID());
            profile.setName("NEW NAME");
            profile.setSurname("NEW SURNAME");
            profile.setPhoto("NEW PHOTO");
            profile.setEmail("NEW EMAIL");
            profile.setPassword("NEW PASSWORD_HASH");
            profile.setGender(Gender.FEMALE);
            profile.setNote("NEW NOTE");
            profile.setBirthday(LocalDate.parse("1998-01-27"));
            profile.setEnabled(false);
            return profile;
        }

    }
}
