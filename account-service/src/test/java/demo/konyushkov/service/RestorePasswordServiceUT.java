package demo.konyushkov.service;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.base.exception.NotFoundException;
import demo.konyushkov.dto.EmailDto;
import demo.konyushkov.dto.RestorePasswordDto;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.service.EmailClient;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.TransactionSystemException;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class RestorePasswordServiceUT {

    @Mock private UserService userService;
    @Mock private PasswordEncoder passwordEncoder;
    @Mock private ConfirmCodeService confirmCodeService;
    @Mock private EmailClient emailClient;
    private RestorePasswordService restorePasswordService;

    private User user = new User();
    private String frontendUrl = "localhost:8080";

    @BeforeEach
    void init() {
        this.restorePasswordService = new RestorePasswordServiceImpl(userService, passwordEncoder,
                confirmCodeService, emailClient);
        user.setId(UUID.randomUUID());
        user.setEmail("demo_test-user@mail.ru");
        user.setPassword("passwordhash");

        ((RestorePasswordServiceImpl) restorePasswordService).frontendUrl = frontendUrl;
    }

    @Nested
    class Restore {

        private RestorePasswordDto restorePasswordDto;

        @BeforeEach
        void init() {
            final String change = "new_password123";
            restorePasswordDto = new RestorePasswordDto();
            restorePasswordDto.setNewPassword(change);
            restorePasswordDto.setConfirmPassword(change);
            restorePasswordDto.setConfirmCode(UUID.randomUUID());
            restorePasswordDto.setUserId(user.getId());
        }

        @Test
        void When_ValidRestorePasswordDto_When_EncodeNewPasswordAndSaveNewPasswordAndRemoveOldConfirmCode () {
            ConfirmCode validCode = getValid();
            AtomicBoolean savedNewPassword = new AtomicBoolean(false);
            AtomicBoolean deleteOldCode = new AtomicBoolean(false);

            Mockito.when(confirmCodeService.findByCode(restorePasswordDto.getConfirmCode())).thenReturn(validCode);
            Mockito.when(userService.findById(restorePasswordDto.getUserId())).thenReturn(user);
            Mockito.when(passwordEncoder.encode(restorePasswordDto.getNewPassword())).thenReturn("changeHash");

            Mockito.when(userService.save(user)).then(i -> {
                savedNewPassword.set(true);
                return user;
            });

            Mockito.doAnswer((i) -> {
                deleteOldCode.set(true);
                return null;
            }).when(confirmCodeService).deleteByCode(validCode.getCode());

            restorePasswordService.restore(restorePasswordDto);
            assertTrue(savedNewPassword.get());
            assertTrue(deleteOldCode.get());
            assertEquals("changeHash", user.getPassword());
        }

        @Test
        void When_ConfirmCodeIsNotExist_When_ThrowInternalServerExceptionWithNotValidMessage () {
            Mockito.when(confirmCodeService.findByCode(restorePasswordDto.getConfirmCode()))
                    .thenThrow(new NotFoundException("{ConfirmCodeServiceImpl.findByCode.NotFound}"));

            BadRequestException ex = assertThrows(BadRequestException.class,
                    () -> restorePasswordService.restore(restorePasswordDto));
            assertEquals("{RestorePasswordServiceImpl.restore.notValid}", ex.getMessage());
            assertEquals("{ConfirmCodeServiceImpl.findByCode.NotFound}", ex.getCause().getMessage());
        }

        @Test
        void When_InvalidUserId_When_ThrowInternalServerExceptionWithNotValidMessage () {
            ConfirmCode validCode = getValid();
            //set invalid userId
            restorePasswordDto.setUserId(UUID.randomUUID());

            Mockito.when(confirmCodeService.findByCode(restorePasswordDto.getConfirmCode())).thenReturn(validCode);


            BadRequestException ex = assertThrows(BadRequestException.class,
                    () -> restorePasswordService.restore(restorePasswordDto));
            assertEquals("{RestorePasswordServiceImpl.restore.notValid}", ex.getMessage());

            String expectedMessage = String.format("Illegal user. Expected: %s, but actual: %s", validCode.getUserId(), restorePasswordDto.getUserId());
            assertEquals(expectedMessage, ex.getCause().getMessage());
        }

        @Test
        void When_InvalidAction_When_ThrowInternalServerExceptionWithNotValidMessage () {
            ConfirmCode validCode = getValid();
            //set invalid action
            validCode.setAction(ConfirmCode.Action.REGISTRATION);

            Mockito.when(confirmCodeService.findByCode(restorePasswordDto.getConfirmCode())).thenReturn(validCode);


            BadRequestException ex = assertThrows(BadRequestException.class,
                    () -> restorePasswordService.restore(restorePasswordDto));
            assertEquals("{RestorePasswordServiceImpl.restore.notValid}", ex.getMessage());

            String expectedMessage = String.format("Illegal action. Expected: %s, but actual: %s", ConfirmCode.Action.RESTORE_PASSWORD, validCode.getAction());
            assertEquals(expectedMessage, ex.getCause().getMessage());
        }

        @Test
        void When_AccountNotExists_When_ThrowInternalServerExceptionWithFailedUpdateMessage () {
            ConfirmCode validCode = getValid();

            Mockito.when(confirmCodeService.findByCode(restorePasswordDto.getConfirmCode())).thenReturn(validCode);
            Mockito.when(userService.findById(restorePasswordDto.getUserId()))
                    .thenThrow(new NotFoundException("{UserServiceImpl.findById.NotFound}"));

            InternalServerException ex = assertThrows(InternalServerException.class,
                    () -> restorePasswordService.restore(restorePasswordDto));
            assertEquals("{RestorePasswordServiceImpl.restore.failedUpdate}", ex.getMessage());
            assertEquals("{UserServiceImpl.findById.NotFound}", ex.getCause().getMessage());
        }

        @Test
        void When_FailedSaveAccountWithNewPassword_When_ThrowInternalServerExceptionWithFailedUpdateMessage () {
            ConfirmCode validCode = getValid();

            Mockito.when(confirmCodeService.findByCode(restorePasswordDto.getConfirmCode())).thenReturn(validCode);
            Mockito.when(userService.findById(restorePasswordDto.getUserId())).thenReturn(user);
            Mockito.when(passwordEncoder.encode(restorePasswordDto.getNewPassword())).thenReturn("changeHash");

            String exMessage = "failed update";
            Mockito.when(userService.save(user)).thenThrow(new TransactionSystemException(exMessage));

            InternalServerException ex = assertThrows(InternalServerException.class,
                    () -> restorePasswordService.restore(restorePasswordDto));
            assertEquals("{RestorePasswordServiceImpl.restore.failedUpdate}", ex.getMessage());
            assertEquals(exMessage, ex.getCause().getMessage());
        }

        @Test
        void When_FailedDeleteConfirmCode_When_SaveUserWithNewPasswordAndCatchException () {
            ConfirmCode validCode = getValid();
            AtomicBoolean savedNewPassword = new AtomicBoolean(false);

            Mockito.when(confirmCodeService.findByCode(restorePasswordDto.getConfirmCode())).thenReturn(validCode);
            Mockito.when(userService.findById(restorePasswordDto.getUserId())).thenReturn(user);
            Mockito.when(passwordEncoder.encode(restorePasswordDto.getNewPassword())).thenReturn("changeHash");

            Mockito.when(userService.save(user)).then(i -> {
                savedNewPassword.set(true);
                return user;
            });

            String exMessage = "failed update";
            Mockito.doThrow(new TransactionSystemException(exMessage))
                    .when(confirmCodeService).deleteByCode(restorePasswordDto.getConfirmCode());

            restorePasswordService.restore(restorePasswordDto);
            assertTrue(savedNewPassword.get());
            assertEquals("changeHash", user.getPassword());
        }

        private ConfirmCode getValid() {
            ConfirmCode confirmCode = new ConfirmCode();
            confirmCode.setCode(restorePasswordDto.getConfirmCode());
            confirmCode.setAction(ConfirmCode.Action.RESTORE_PASSWORD);

            confirmCode.setUser(user);
            confirmCode.setUserId(user.getId());

            return confirmCode;
        }
    }

    @Nested
    class CreateRequest {

        @Test
        void When_UserFindByEmail_Expect_SaveConfirmCodeToDbAndSendMessageToEmail() {
            ConfirmCode confirmCode = ConfirmCode.builder()
                    .code(UUID.randomUUID())
                    .user(user)
                    .action(ConfirmCode.Action.RESTORE_PASSWORD)
                    .build();

            Mockito.when(userService.findByEmail(user.getEmail())).thenReturn(user);
            Mockito.when(confirmCodeService.prepareAndSave(user, ConfirmCode.Action.RESTORE_PASSWORD)).thenReturn(confirmCode);

            EmailDto emailDto = new EmailDto(user.getEmail());
            restorePasswordService.createRequest(emailDto);

            ArgumentCaptor<HtmlMail> argument = ArgumentCaptor.forClass(HtmlMail.class);
            verify(emailClient).send(argument.capture());
            HtmlMail mail = argument.getValue();
            assertEquals(user.getEmail(), mail.getTo());
            assertEquals(ConfirmCode.Action.RESTORE_PASSWORD.name().toLowerCase(), mail.getTemplate());
            assertEquals(Map.of(
                    "confirmCode", confirmCode.getCode(),
                    "userId", confirmCode.getUser().getId(),
                    "frontendUrl", frontendUrl),
                    mail.getVariables());
        }

        @Test
        void When_UserNotFindByEmail_Expect_ThrowNotFoundException() {
            String exMessage = "User not found";
            Mockito.when(userService.findByEmail(anyString())).thenThrow(new NotFoundException(exMessage));

            EmailDto emailDto = new EmailDto("notfound@email.ru");
            NotFoundException ex = assertThrows(NotFoundException.class, () -> restorePasswordService.createRequest(emailDto));
            assertEquals(exMessage, ex.getMessage());
        }

        @Test
        void When_FailedSaveConfirmCode_Expect_ThrowTransactionSystemException() {
            String exMessage = "Failed save to Db";
            Mockito.when(userService.findByEmail(anyString())).thenReturn(user);
            Mockito.when(confirmCodeService.prepareAndSave(any(User.class), any(ConfirmCode.Action.class))).thenThrow(new TransactionSystemException(exMessage));

            EmailDto emailDto = new EmailDto(user.getEmail());
            TransactionSystemException ex = assertThrows(TransactionSystemException.class, () -> restorePasswordService.createRequest(emailDto));
            assertEquals(exMessage, ex.getMessage());
        }

        @Test
        void When_FailedSendMail_Expect_ThrowRuntimeException() {
            String exMessage = "Failed send email";
            ConfirmCode confirmCode = ConfirmCode.builder()
                    .code(UUID.randomUUID())
                    .user(user)
                    .action(ConfirmCode.Action.RESTORE_PASSWORD)
                    .build();

            Mockito.when(userService.findByEmail(user.getEmail())).thenReturn(user);
            Mockito.when(confirmCodeService.prepareAndSave(user, ConfirmCode.Action.RESTORE_PASSWORD)).thenReturn(confirmCode);
            Mockito.doThrow(new RuntimeException(exMessage)).when(emailClient).send(any(HtmlMail.class));

            EmailDto emailDto = new EmailDto(user.getEmail());
            RuntimeException ex = assertThrows(RuntimeException.class, () -> restorePasswordService.createRequest(emailDto));
            assertEquals(exMessage, ex.getMessage());
        }
    }
}