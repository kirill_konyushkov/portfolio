package demo.konyushkov.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.service.CaptchaClient;
import demo.konyushkov.config.WebAppContextIT;
import demo.konyushkov.dto.EmailDto;
import demo.konyushkov.dto.RestorePasswordDto;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.service.EmailClient;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;
import demo.konyushkov.repository.ConfirmCodeRepository;
import demo.konyushkov.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class RestorePasswordControllerIT {

    @Nested
    class Restore extends WebAppContextIT {

        @Autowired private WebApplicationContext wac;
        @Autowired private ObjectMapper objectMapper;
        @Autowired private UserService userService;
        @Autowired private MessageSourceAccessor messageSourceAccessor;
        @Autowired private ConfirmCodeRepository confirmCodeRepository;
        @Autowired private PasswordEncoder passwordEncoder;
        @Autowired private CaptchaClient captchaClient;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            CaptchaResponse response = new CaptchaResponse(true);
            Mockito.when(captchaClient.validate(any(CaptchaRequest.class))).thenReturn(response);
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_ValidRestorePasswordDto_Expect_UpdateUserPasswordAndReturnResponseDetails() throws Exception {
            RestorePasswordDto restorePasswordDto = new RestorePasswordDto();
            restorePasswordDto.setNewPassword(Constants.PASSWORD);
            restorePasswordDto.setConfirmPassword(Constants.PASSWORD);
            restorePasswordDto.setConfirmCode(Constants.CODE);
            restorePasswordDto.setUserId(Constants.USER_ID);

            mockMvc.perform(post("/restore-password")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(restorePasswordDto)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.notice.message").value(messageSourceAccessor.getMessage("RestorePasswordController.restore.message")))
                    .andDo(print());

            User user = userService.findById(restorePasswordDto.getUserId());
            assertTrue(passwordEncoder.matches(Constants.PASSWORD, user.getPassword()));
            assertFalse(confirmCodeRepository.existsById(restorePasswordDto.getConfirmCode()));
        }

        @Test
        void When_InvalidRestorePasswordDto_Expect_BadRequestWithMessage() throws Exception {
            RestorePasswordDto restorePasswordDto = new RestorePasswordDto();
            restorePasswordDto.setNewPassword(Constants.PASSWORD);
            restorePasswordDto.setConfirmPassword(Constants.PASSWORD);
            restorePasswordDto.setConfirmCode(Constants.CODE);
            // invalid userId
            restorePasswordDto.setUserId(UUID.randomUUID());

            mockMvc.perform(post("/restore-password")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(restorePasswordDto)))
                    .andExpect(status().isBadRequest())
                    .andDo(print());
        }

        private String getJson(Object obj) throws JsonProcessingException {
            return objectMapper.writeValueAsString(obj);
        }
    }

    @Nested
    class CreateRequest extends WebAppContextIT {

        @Autowired private WebApplicationContext wac;
        @Autowired private ObjectMapper objectMapper;
        @Autowired private MessageSourceAccessor messageSourceAccessor;
        @Autowired private ConfirmCodeRepository confirmCodeRepository;
        @Autowired private EmailClient emailClient;
        @Autowired private CaptchaClient captchaClient;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            CaptchaResponse response = new CaptchaResponse(true);
            Mockito.when(captchaClient.validate(any(CaptchaRequest.class))).thenReturn(response);
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_ValidRestorePasswordDto_Expect_UpdateUserPasswordAndReturnResponseDetails() throws Exception {
            confirmCodeRepository.deleteAll();
            AtomicBoolean send = new AtomicBoolean(false);

            EmailDto emailDto = new EmailDto(Constants.EXIST_EMAIL);
            Mockito.doAnswer(i -> {
                send.set(true);
                return null;
            }).when(emailClient).send(any(HtmlMail.class));

            mockMvc.perform(post("/restore-password/request")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(emailDto)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.notice.title").value(messageSourceAccessor.getMessage("RestorePasswordController.createRequest.title")))
                    .andExpect(jsonPath("$.notice.message").value(messageSourceAccessor.getMessage("RestorePasswordController.createRequest.message")))
                    .andDo(print());

            Iterable<ConfirmCode> codeIterable = confirmCodeRepository.findAll();
            List<ConfirmCode> codeList = StreamSupport.stream(codeIterable.spliterator(), false)
                    .collect(Collectors.toList());

            ArgumentCaptor<HtmlMail> argument = ArgumentCaptor.forClass(HtmlMail.class);
            verify(emailClient).send(argument.capture());
            HtmlMail value = argument.getValue();

            assertEquals(1, codeList.size());
            assertTrue(send.get());
            assertNotNull(value.getTo());
            assertEquals(emailDto.getEmail(), value.getTo());
            assertNotNull(value.getVariables());
            System.out.println(value);
        }

        private String getJson(Object obj) throws JsonProcessingException {
            return objectMapper.writeValueAsString(obj);
        }
    }

    static class Constants {
        static final UUID CODE = UUID.fromString("5722a55d-3eb7-421e-84e9-2eb1ce70cce9");
        static final UUID USER_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");
        static final String EXIST_EMAIL = "demo_test-user@mail.ru";
        static final  String PASSWORD = "newPassword123";
    }

}
