package demo.konyushkov.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.konyushkov.config.WebAppContextIT;
import demo.konyushkov.dto.PasswordDto;
import demo.konyushkov.json.views.UserViews;
import demo.konyushkov.model.User;
import demo.konyushkov.repository.UserRepository;
import demo.konyushkov.service.AccountService;
import demo.konyushkov.test.tools.OAuth2Helper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class AccountControllerIT {

    @Nested
    class AccountMe extends WebAppContextIT {

        @Autowired private WebApplicationContext wac;
        @Autowired private OAuth2Helper oAuth2Helper;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_UserAuthorized_Expect_ReturnUserAccount() throws Exception {
            String accessToken = oAuth2Helper.obtainAccessToken(Constants.VALID_EMAIL, Constants.VALID_PASSWORD);

            this.mockMvc.perform(get("/account/me")
                    .header("Authorization", "Bearer " + accessToken)
                    .accept(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                    .andExpect(jsonPath("$.*", hasSize(9)))
                    .andExpect(jsonPath("$.name").exists())
                    .andExpect(jsonPath("$.id").exists())
                    .andExpect(jsonPath("$.surname").exists())
                    .andExpect(jsonPath("$.photo").exists())
                    .andExpect(jsonPath("$.phoneNumber").exists())
                    .andExpect(jsonPath("$.birthday").exists())
                    .andExpect(jsonPath("$.note").exists())
                    .andExpect(jsonPath("$.gender").exists())
                    .andExpect(jsonPath("$.email").value(Constants.VALID_EMAIL))
                    .andReturn();
        }
    }

    @Nested
    class ChangePassword extends WebAppContextIT {

        @Autowired private WebApplicationContext wac;
        @Autowired private OAuth2Helper oAuth2Helper;
        @Autowired private ObjectMapper objectMapper;
        @Autowired private UserRepository userRepository;
        @Autowired private PasswordEncoder passwordEncoder;
        @Autowired private MessageSourceAccessor messageSourceAccessor;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_UserAuthorizedAndValidPasswordDto_Expect_ChangePassword() throws Exception {
            String accessToken = oAuth2Helper.obtainAccessToken(Constants.VALID_EMAIL, Constants.VALID_PASSWORD);

            final String change = "new_password123";
            PasswordDto passwordDto = new PasswordDto();
            passwordDto.setCurrentPassword(Constants.VALID_PASSWORD);
            passwordDto.setNewPassword(change);
            passwordDto.setConfirmPassword(change);

            String json = objectMapper.writeValueAsString(passwordDto);

            this.mockMvc.perform(put("/account/password")
                    .header("Authorization", "Bearer " + accessToken)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(json))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.notice.message").value(messageSourceAccessor.getMessage("AccountController.updatePassword.message")))
                    .andExpect(jsonPath("$.content").doesNotExist())
                    .andDo(print());

            User accountFromDb = userRepository.findByEmail(Constants.VALID_EMAIL).get();
            passwordEncoder.matches(change, accountFromDb.getPassword());
        }

        @Test
        void When_UserAuthorizedAndInvalidPasswordDto_Expect_ReturnUserAccount() throws Exception {
            String accessToken = oAuth2Helper.obtainAccessToken(Constants.VALID_EMAIL, Constants.VALID_PASSWORD);

            PasswordDto passwordDto = new PasswordDto();
            passwordDto.setCurrentPassword(Constants.INVALID_PASSWORD);
            passwordDto.setNewPassword(Constants.NEW_PASSWORD);
            passwordDto.setConfirmPassword(Constants.NEW_PASSWORD);

            String json = objectMapper.writeValueAsString(passwordDto);

            this.mockMvc.perform(put("/account/password")
                    .header("Authorization", "Bearer " + accessToken)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(json))
                    .andExpect(status().isBadRequest())
                    .andDo(print());
        }
    }

    @Nested
    class UpdatePersonal extends WebAppContextIT {

        @Autowired private WebApplicationContext wac;
        @Autowired private OAuth2Helper oAuth2Helper;
        @Autowired private ObjectMapper objectMapper;
        @Autowired private UserRepository userRepository;
        @Autowired private MessageSourceAccessor messageSourceAccessor;
        @Autowired AccountService accountService;



        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_ValidPersonal_Expect_SuccessUpdateAndReturnUpdatedAccount() throws Exception {
            String accessToken = oAuth2Helper.obtainAccessToken(Constants.VALID_EMAIL, Constants.VALID_PASSWORD);
            User user = userRepository.findById(Constants.VALID_ID).get();
            user.setName(" Updated");

            mockMvc.perform(put("/account/personal")
                    .header("Authorization", "Bearer " + accessToken)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(user)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.notice.message").value(messageSourceAccessor.getMessage("AccountController.updatePersonal.message")))
                    .andExpect(jsonPath("$.content.*", hasSize(9)))
                    .andExpect(jsonPath("$.content.name").value(user.getName().trim()))
                    .andExpect(jsonPath("$.content.id").exists())
                    .andExpect(jsonPath("$.content.surname").exists())
                    .andExpect(jsonPath("$.content.phoneNumber").exists())
                    .andExpect(jsonPath("$.content.photo").exists())
                    .andExpect(jsonPath("$.content.birthday").exists())
                    .andExpect(jsonPath("$.content.note").exists())
                    .andExpect(jsonPath("$.content.gender").exists())
                    .andExpect(jsonPath("$.content.email").exists())
                    .andDo(print());
        }

        @Test
        void When_NotValidPersonal_Expect_BadRequestWithViolations() throws Exception {
            String accessToken = oAuth2Helper.obtainAccessToken(Constants.VALID_EMAIL, Constants.VALID_PASSWORD);
            User user = userRepository.findById(Constants.VALID_ID).get();
            user.setName("");
            user.setBirthday(LocalDate.MIN);

            mockMvc.perform(put("/account/personal")
                    .header("Authorization", "Bearer " + accessToken)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(user)))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.content.name").isArray())
                    .andExpect(jsonPath("$.content.birthday").isArray())
                    .andDo(print());
        }

        private String getJson(Object obj) throws JsonProcessingException {
            return objectMapper.writerWithView(UserViews.PersonalAndContacts.class).writeValueAsString(obj);
        }
    }


    static class Constants {
        static final UUID VALID_ID = UUID.fromString("c56240bc-b006-4197-90e7-4d31a9e601f1");
        static final String VALID_EMAIL = "demo_test-user@mail.ru";

        static final String VALID_PASSWORD = "zaq12345";
        static final String INVALID_PASSWORD = "invalid_password_123";
        static final String NEW_PASSWORD = "new_password_123";
    }
}
