package demo.konyushkov.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.konyushkov.captcha.dto.CaptchaRequest;
import demo.konyushkov.captcha.dto.CaptchaResponse;
import demo.konyushkov.captcha.service.CaptchaClient;
import demo.konyushkov.config.WebAppContextIT;
import demo.konyushkov.dto.ConfirmRegistrationDto;
import demo.konyushkov.dto.RegistrationDto;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.service.EmailClient;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.Role;
import demo.konyushkov.model.User;
import demo.konyushkov.service.ConfirmCodeService;
import demo.konyushkov.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class RegistrationControllerIT {

    @Nested
    class Registration extends WebAppContextIT {

        @Autowired private WebApplicationContext wac;
        @Autowired private ObjectMapper objectMapper;
        @Autowired private UserService userService;
        @Autowired private MessageSourceAccessor messageSourceAccessor;
        @Autowired private PasswordEncoder passwordEncoder;
        @Autowired private CaptchaClient captchaClient;
        @Autowired private EmailClient emailClient;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            CaptchaResponse response = new CaptchaResponse(true);
            Mockito.doNothing().when(emailClient).send(any(HtmlMail.class));
            Mockito.when(captchaClient.validate(any(CaptchaRequest.class))).thenReturn(response);
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_ValidRegistrationPasswordDto_Expect_CreateNewUserAndSendEmailAndReturnResponseDetails() throws Exception {
            RegistrationDto registrationDto = new RegistrationDto();
            registrationDto.setName(Constants.NEW_NAME);
            registrationDto.setEmail(Constants.NEW_EMAIL);
            registrationDto.setPassword(Constants.NEW_PASSWORD);
            registrationDto.setConfirmPassword(Constants.NEW_PASSWORD);

            mockMvc.perform(post("/registration")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(registrationDto)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.notice.title").value(messageSourceAccessor.getMessage("RegistrationController.registration.title")))
                    .andExpect(jsonPath("$.notice.message").value(messageSourceAccessor.getMessage("RegistrationController.registration.message")))
                    .andDo(print());

            User user = userService.findByEmail(registrationDto.getEmail());
            assertFalse(user.getEnabled());
            assertTrue(passwordEncoder.matches(Constants.NEW_PASSWORD, user.getPassword()));
        }

        @Test
        void When_InvalidRestorePasswordDto_Expect_BadRequestWithMessage() throws Exception {
            RegistrationDto registrationDto = new RegistrationDto();
            registrationDto.setName(Constants.NEW_NAME);
            // invalid email (exist yet)
            registrationDto.setEmail(Constants.EXIST_EMAIL);
            registrationDto.setPassword(Constants.NEW_PASSWORD);
            registrationDto.setConfirmPassword(Constants.NEW_PASSWORD);

            mockMvc.perform(post("/registration")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(registrationDto)))
                    .andExpect(status().isBadRequest())
                    .andDo(print());
        }

        private String getJson(Object obj) throws JsonProcessingException {
            return objectMapper.writeValueAsString(obj);
        }
    }


    @Nested
    class Confirm extends WebAppContextIT {

        @Autowired private WebApplicationContext wac;
        @Autowired private ObjectMapper objectMapper;
        @Autowired private UserService userService;
        @Autowired private ConfirmCodeService confirmCodeService;
        @Autowired private MessageSourceAccessor messageSourceAccessor;
        @Autowired private PasswordEncoder passwordEncoder;
        @Autowired private CaptchaClient captchaClient;

        private MockMvc mockMvc;

        @BeforeEach
        void setup() {
            CaptchaResponse response = new CaptchaResponse(true);
            Mockito.when(captchaClient.validate(any(CaptchaRequest.class))).thenReturn(response);
            this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(springSecurity()).build();
        }

        @Test
        void When_ValidConfirmRegistrationDto_Expect_SetUserEnabledTrueAndSaveAndReturnResponseDetails() throws Exception {
            ConfirmCode confirmCode = prepare();
            ConfirmRegistrationDto dto = new ConfirmRegistrationDto();
            dto.setUserId(confirmCode.getUserId());
            dto.setConfirmCode(confirmCode.getCode());

            mockMvc.perform(post("/registration/confirm")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(dto)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.notice.title").value(messageSourceAccessor.getMessage("RegistrationController.confirm.title")))
                    .andExpect(jsonPath("$.notice.message").value(messageSourceAccessor.getMessage("RegistrationController.confirm.message")))
                    .andDo(print());

            User user = userService.findById(confirmCode.getUserId());
            assertTrue(user.getEnabled());
            assertEquals(Constants.NEW_EMAIL, user.getEmail());
            assertEquals(Constants.NEW_NAME, user.getName());
            assertTrue(passwordEncoder.matches(Constants.NEW_PASSWORD, user.getPassword()));
        }

        @Test
        void When_InvalidConfirmRegistrationDto_Expect_BadRequestWithMessage() throws Exception {
            ConfirmCode confirmCode = prepare();
            ConfirmRegistrationDto dto = new ConfirmRegistrationDto();
            // invalid userId
            dto.setUserId(UUID.randomUUID());
            dto.setConfirmCode(confirmCode.getCode());

            mockMvc.perform(post("/registration/confirm")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(getJson(dto)))
                    .andExpect(status().isBadRequest())
                    .andDo(print());
        }

        private String getJson(Object obj) throws JsonProcessingException {
            return objectMapper.writeValueAsString(obj);
        }

        private ConfirmCode prepare() {
            User user = new User();
            user.setId(UUID.randomUUID());
            user.setName(Constants.NEW_NAME);
            user.setEmail(Constants.NEW_EMAIL);
            user.setEnabled(false);
            user.setPassword(passwordEncoder.encode(Constants.NEW_PASSWORD));

            Role role = new Role();
            role.setCode(Constants.ROLE_ADMIN);

            user.setRole(role);

            userService.save(user);

            ConfirmCode confirmCode = confirmCodeService.prepareAndSave(user, ConfirmCode.Action.REGISTRATION);
            confirmCode.setUserId(user.getId());
            return confirmCode;
        }
    }

    static class Constants {
        static final String NEW_NAME = "Kirill";
        static final String NEW_EMAIL = "new-email@mail.ru";
        static final String NEW_PASSWORD = "newPassword123";
        static final String EXIST_EMAIL = "demo_test-user@mail.ru";
        static final String ROLE_ADMIN = "ROLE_ADMIN";
    }
}
