package demo.konyushkov.config;

import demo.konyushkov.captcha.service.CaptchaClient;
import demo.konyushkov.email.service.EmailClient;
import demo.konyushkov.test.config.Profiles;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile(Profiles.WEB_API_IT)
public class MockBeanConfig {

    @Bean
    @Primary
    public CaptchaClient captchaClientImpl() {
        return Mockito.mock(CaptchaClient.class);
    }

    @Bean
    @Primary
    public EmailClient emailClientImpl() {
        return Mockito.mock(EmailClient.class);
    }
}
