package demo.konyushkov.config;

import demo.konyushkov.test.config.AbstractWebAppContextIT;
import demo.konyushkov.test.config.BaseConfigIT;
import demo.konyushkov.test.config.Profiles;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@Configuration
@ActiveProfiles(value = {Profiles.WEB_API_IT})
@ContextConfiguration(classes = { AppConfig.class, BaseConfigIT.class, MockBeanConfig.class })
public class WebAppContextIT extends AbstractWebAppContextIT {}
