package demo.konyushkov.dto;

import demo.konyushkov.test.tools.ValidatorTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class ConfirmRegistrationDtoUT {

    private ConfirmRegistrationDto confirmRegistrationDto;
    private static ValidatorTest<ConfirmRegistrationDto> validator;

    @BeforeAll
    static void setUp() {
        validator = new ValidatorTest<>();
    }

    private ConfirmRegistrationDto createValid() {
        ConfirmRegistrationDto dto = new ConfirmRegistrationDto();
        dto.setConfirmCode(UUID.randomUUID());
        dto.setUserId(UUID.randomUUID());
        return dto;
    }

    @Nested
    class DefaultGroup {

        @BeforeEach
        void init() {
            confirmRegistrationDto = createValid();
        }

        @Nested
        class Success {
            @Test
            void When_ConfirmRegistrationDtoValid_Expect_ViolationsIsEmpty() {
                validator.successValidation(confirmRegistrationDto);
            }
        }

        @Nested
        class Failed {

            @Test
            void When_ConfirmCodeIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                confirmRegistrationDto.setConfirmCode(null);
                validator.fillTestData("confirmCode", "{ConfirmRegistrationDto.confirmCode.NotNull}");
                validator.failedValidation(confirmRegistrationDto);
            }

            @Test
            void When_UserIdIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                confirmRegistrationDto.setUserId(null);
                validator.fillTestData("userId", "{ConfirmRegistrationDto.userId.NotNull}");
                validator.failedValidation(confirmRegistrationDto);
            }
        }
    }
}
