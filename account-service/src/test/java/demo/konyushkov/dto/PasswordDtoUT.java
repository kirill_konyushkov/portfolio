package demo.konyushkov.dto;

import demo.konyushkov.test.tools.ValidatorTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class PasswordDtoUT {

    private PasswordDto passwordDto;
    private static ValidatorTest<PasswordDto> validator;

    @BeforeAll
    static void setUp() {
        validator = new ValidatorTest<>();
    }

    private PasswordDto createValid() {
        final String CURRENT_PASSWORD = "current_password123";
        final String NEW_PASSWORD = "new_password123";
        PasswordDto passwordDto = new PasswordDto();
        passwordDto.setCurrentPassword(CURRENT_PASSWORD);
        passwordDto.setNewPassword(NEW_PASSWORD);
        passwordDto.setConfirmPassword(NEW_PASSWORD);

        return passwordDto;
    }

    @Nested
    class DefaultGroup {

        @BeforeEach
        void init() {
            passwordDto = createValid();
        }

        @Nested
        class Success {
            @Test
            void When_PasswordDtoValid_Expect_ViolationsIsEmpty() {
                validator.successValidation(passwordDto);
            }
        }

        @Nested
        class Failed {

            @Test
            void When_CurrentPasswordIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                passwordDto.setCurrentPassword(null);
                validator.fillTestData("currentPassword", "{PasswordDto.currentPassword.NotNull}");
                validator.failedValidation(passwordDto);
            }

            @Test
            void When_CurrentPasswordInvalid_Expect_OneViolationWithNotNullMessageTemplate() {
                passwordDto.setCurrentPassword("invalid");
                validator.fillTestData("currentPassword", "{demo.konyushkov.validation.constraints.Password}");
                validator.failedValidation(passwordDto);
            }

            @Test
            void When_NewPasswordIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                passwordDto.setNewPassword(null);
                validator.fillTestData("newPassword", "{PasswordDto.newPassword.NotNull}");
                validator.failedValidation(passwordDto);
            }

            @Test
            void When_NewPasswordInvalid_Expect_OneViolationWithNotNullMessageTemplate() {
                passwordDto.setNewPassword("invalid");
                passwordDto.setConfirmPassword("invalid");
                validator.fillTestData("newPassword", "{demo.konyushkov.validation.constraints.Password}");
                validator.failedValidation(passwordDto);
            }

            @Test
            void When_ConfirmPasswordIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                passwordDto.setConfirmPassword(null);
                validator.fillTestData("confirmPassword", "{PasswordDto.confirmPassword.NotNull}");
                validator.failedValidation(passwordDto);
            }

            @Test
            void When_NewAndConfirmNotEquals_Expect_OneViolationWithCrossfieldMessageTemplate() {
                passwordDto.setConfirmPassword("not_repeat");
                validator.fillTestData("confirmPassword", "{PasswordDto.crossfield.newAndConfirmEquals}");
                validator.failedCrossfieldValidation(passwordDto);
            }
        }
    }
}
