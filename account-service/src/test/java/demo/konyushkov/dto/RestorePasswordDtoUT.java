package demo.konyushkov.dto;

import demo.konyushkov.test.tools.ValidatorTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class RestorePasswordDtoUT {

    private RestorePasswordDto restorePasswordDto;
    private static ValidatorTest<RestorePasswordDto> validator;

    @BeforeAll
    static void setUp() {
        validator = new ValidatorTest<>();
    }

    private RestorePasswordDto createValid() {
        final String change = "new_password123";
        RestorePasswordDto dto = new RestorePasswordDto();
        dto.setNewPassword(change);
        dto.setConfirmPassword(change);
        dto.setConfirmCode(UUID.randomUUID());
        dto.setUserId(UUID.randomUUID());

        return dto;
    }

    @Nested
    class DefaultGroup {

        @BeforeEach
        void init() {
            restorePasswordDto = createValid();
        }

        @Nested
        class Success {
            @Test
            void When_RestorePasswordDtoValid_Expect_ViolationsIsEmpty() {
                validator.successValidation(restorePasswordDto);
            }
        }

        @Nested
        class Failed {

            @Test
            void When_NewPasswordIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                restorePasswordDto.setNewPassword(null);
                validator.fillTestData("newPassword", "{RestorePasswordDto.newPassword.NotNull}");
                validator.failedValidation(restorePasswordDto);
            }

            @Test
            void When_NewPasswordInvalid_Expect_OneViolationWithNotNullMessageTemplate() {
                restorePasswordDto.setNewPassword("invalid");
                restorePasswordDto.setConfirmPassword("invalid");
                validator.fillTestData("newPassword", "{demo.konyushkov.validation.constraints.Password}");
                validator.failedValidation(restorePasswordDto);
            }

            @Test
            void When_ConfirmPasswordIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                restorePasswordDto.setConfirmPassword(null);
                validator.fillTestData("confirmPassword", "{RestorePasswordDto.confirmPassword.NotNull}");
                validator.failedValidation(restorePasswordDto);
            }

            @Test
            void When_NewAndConfirmNotEquals_Expect_OneViolationWithCrossfieldMessageTemplate() {
                restorePasswordDto.setConfirmPassword("not_repeat");
                validator.fillTestData("confirmPassword", "{RestorePasswordDto.crossfield.newAndConfirmEquals}");
                validator.failedCrossfieldValidation(restorePasswordDto);
            }

            @Test
            void When_ConfirmCodeIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                restorePasswordDto.setConfirmCode(null);
                validator.fillTestData("confirmCode", "{RestorePasswordDto.confirmCode.NotNull}");
                validator.failedValidation(restorePasswordDto);
            }

            @Test
            void When_UserIdIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                restorePasswordDto.setUserId(null);
                validator.fillTestData("userId", "{RestorePasswordDto.userId.NotNull}");
                validator.failedValidation(restorePasswordDto);
            }
        }
    }
}
