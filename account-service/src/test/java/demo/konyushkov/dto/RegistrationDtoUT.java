package demo.konyushkov.dto;

import demo.konyushkov.test.tools.ValidatorTest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class RegistrationDtoUT {

    private RegistrationDto registrationDto;
    private static ValidatorTest<RegistrationDto> validator;

    @BeforeAll
    static void setUp() {
        validator = new ValidatorTest<>();
    }

    private RegistrationDto createValid() {
        final String password = "new_password123";
        RegistrationDto dto = new RegistrationDto();
        dto.setName("Кирилл");
        dto.setEmail("demo_test-user@mail.ru");
        dto.setPassword(password);
        dto.setConfirmPassword(password);
        return dto;
    }

    @Nested
    class DefaultGroup {

        @BeforeEach
        void init() {
            registrationDto = createValid();
        }

        @Nested
        class Success {
            @Test
            void When_RegistrationDtoValid_Expect_ViolationsIsEmpty() {
                validator.successValidation(registrationDto);
            }
        }

        @Nested
        class Failed {

            @Test
            void When_NameIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                registrationDto.setName(null);
                validator.fillTestData("name", "{RegistrationDto.name.NotBlank}");
                validator.failedValidation(registrationDto);
            }

            @Test
            void When_NameIsEmpty_Expect_OneViolationWithNotNullMessageTemplate() {
                registrationDto.setName("");
                validator.fillTestData("name", "{RegistrationDto.name.NotBlank}");
                validator.failedValidation(registrationDto);
            }

            @Test
            void When_NameIsOnlySpace_Expect_OneViolationWithNotNullMessageTemplate() {
                registrationDto.setName("  ");
                validator.fillTestData("name", "{RegistrationDto.name.NotBlank}");
                validator.failedValidation(registrationDto);
            }

            @Test
            void When_EmailIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                registrationDto.setEmail(null);
                validator.fillTestData("email", "{RegistrationDto.email.NotNull}");
                validator.failedValidation(registrationDto);
            }

            @Test
            void When_EmailInvalid_Expect_OneViolationWithNotNullMessageTemplate() {
                registrationDto.setEmail("invalid_email");
                validator.fillTestData("email", "{demo.konyushkov.validation.constraints.Email}");
                validator.failedValidation(registrationDto);
            }

            @Test
            void When_PasswordIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                registrationDto.setPassword(null);
                validator.fillTestData("password", "{RegistrationDto.password.NotNull}");
                validator.failedValidation(registrationDto);
            }

            @Test
            void When_PasswordInvalid_Expect_OneViolationWithNotNullMessageTemplate() {
                registrationDto.setPassword("invalid_password");
                registrationDto.setConfirmPassword("invalid_password");
                validator.fillTestData("password", "{demo.konyushkov.validation.constraints.Password}");
                validator.failedValidation(registrationDto);
            }

            @Test
            void When_ConfirmPasswordIsNull_Expect_OneViolationWithNotNullMessageTemplate() {
                registrationDto.setConfirmPassword(null);
                validator.fillTestData("confirmPassword", "{RegistrationDto.confirmPassword.NotNull}");
                validator.failedValidation(registrationDto);
            }

            @Test
            void When_PasswordAndConfirmPasswordNotEquals_Expect_OneViolationWithCrossfieldMessageTemplate() {
                registrationDto.setConfirmPassword("not_repeat");
                validator.fillTestData("confirmPassword", "{RegistrationDto.crossfield.passwordAndConfirmPasswordEquals}");
                validator.failedCrossfieldValidation(registrationDto);
            }
        }
    }
}
