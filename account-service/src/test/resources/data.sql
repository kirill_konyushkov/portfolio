INSERT INTO authority (name) VALUES ('READ'), ('WRITE');

INSERT INTO role (code, description) VALUES ('ROLE_ADMIN', 'Администратор'), ('ROLE_GUEST', 'Гость');

INSERT INTO user (id, email, name, password,
                    photo, birthday, surname, phone_number,
                    note, gender, role_code, enabled)
  VALUES ('c56240bc-b006-4197-90e7-4d31a9e601f1', 'demo_test-user@mail.ru', 'Кирилл', '$2a$04$KXKrdHLqqWXex11KGXqHreJRD3jPIHF1y07s.f8aVc0qo/nj5y8Gi',
          '/img/avatar.png', '1996-01-27', 'Конюшков', '8 (901) 189-12-38',
          'note about me', 1, 'ROLE_ADMIN', true);

INSERT INTO roles_authorities (role_code, authority_name)
  VALUES ('ROLE_ADMIN', 'WRITE'),
        ('ROLE_ADMIN', 'READ'),
        ('ROLE_GUEST', 'READ');

INSERT INTO confirm_code (code, action, user_id)
  VALUES ('5722a55d-3eb7-421e-84e9-2eb1ce70cce9', 'RESTORE_PASSWORD', 'c56240bc-b006-4197-90e7-4d31a9e601f1');
