package demo.konyushkov;

import demo.konyushkov.web.tomcat.EmbeddedTomcatStarter;

public class Application {
    private static final int PORT = 8089;

    public static void main(String[] args) throws Exception {
        EmbeddedTomcatStarter.start(PORT);
    }
}
