package demo.konyushkov.utils;

import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;

import java.util.Map;

public class EmailUtils {

    public static HtmlMail prepare(ConfirmCode confirmCode, String frontendUrl) {
        String template = confirmCode.getAction().name().toLowerCase();

        User user = confirmCode.getUser();

        HtmlMail htmlMail = new HtmlMail();
        htmlMail.setTo(user.getEmail());
        htmlMail.setTemplate(template);
        htmlMail.setVariables(Map.of(
                "userId", user.getId(),
                "confirmCode", confirmCode.getCode(),
                "frontendUrl", frontendUrl));
        htmlMail.setSubject(template);

        return htmlMail;
    }

}
