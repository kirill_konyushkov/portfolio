package demo.konyushkov.utils;

import demo.konyushkov.base.utils.Precondition;
import demo.konyushkov.model.ConfirmCode;

import java.util.UUID;

public class ConfirmCodeUtils {

    public static void validate(ConfirmCode confirmCode, UUID actualUserId, ConfirmCode.Action expectedAction) {
        Precondition.checkArgument(
                confirmCode.getUserId().equals(actualUserId),
                String.format("Illegal user. Expected: %s, but actual: %s", confirmCode.getUserId(), actualUserId));

        Precondition.checkArgument(
                confirmCode.getAction().equals(expectedAction),
                String.format("Illegal action. Expected: %s, but actual: %s", expectedAction, confirmCode.getAction()));
    }

}
