package demo.konyushkov.controller;

import demo.konyushkov.captcha.aop.RequiresCaptcha;
import demo.konyushkov.dto.ConfirmRegistrationDto;
import demo.konyushkov.dto.RegistrationDto;
import demo.konyushkov.service.RegistrationService;
import demo.konyushkov.web.aop.RestResponseDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    private final RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @PostMapping
    @RequiresCaptcha
    @RestResponseDetails(title = "RegistrationController.registration.title", message = "RegistrationController.registration.message")
    public ResponseEntity registration(@RequestBody @Valid RegistrationDto registrationDto) {
        registrationService.registration(registrationDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/confirm")
    @RestResponseDetails(title = "RegistrationController.confirm.title", message = "RegistrationController.confirm.message")
    public ResponseEntity confirm(@RequestBody @Valid ConfirmRegistrationDto confirmRegistrationDto) {
        registrationService.confirm(confirmRegistrationDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
