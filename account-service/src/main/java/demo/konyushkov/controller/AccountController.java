package demo.konyushkov.controller;

import com.fasterxml.jackson.annotation.JsonView;
import demo.konyushkov.dto.PasswordDto;
import demo.konyushkov.json.views.UserViews;
import demo.konyushkov.model.User;
import demo.konyushkov.service.AccountService;
import demo.konyushkov.validation.groups.UserGroups;
import demo.konyushkov.web.aop.RestResponseDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/account")
public class AccountController {

    private final AccountService service;

    @Autowired
    public AccountController(AccountService service) {
        this.service = service;
    }

    @GetMapping(value = "/me")
    @JsonView(UserViews.PersonalAndContacts.class)
    public ResponseEntity<User> accountMe(Principal principal) {
        return new ResponseEntity<>(service.findByPrincipal(principal), HttpStatus.OK);
    }

    @PutMapping(value = "/personal")
    @JsonView(UserViews.PersonalAndContacts.class)
    @RestResponseDetails(message = "AccountController.updatePersonal.message")
    public ResponseEntity<User> updatePersonal(@RequestBody @Validated(UserGroups.Personal.class) User user, Principal principal) {
        return new ResponseEntity<>(service.updatePersonal(user, principal), HttpStatus.OK);
    }

    @PutMapping(value = "/password")
    @RestResponseDetails(message = "AccountController.updatePassword.message")
    public ResponseEntity updatePassword(@RequestBody @Valid PasswordDto passwordDto, Principal principal) {
        service.updatePassword(passwordDto, principal);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
