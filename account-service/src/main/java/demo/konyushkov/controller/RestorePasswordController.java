package demo.konyushkov.controller;

import demo.konyushkov.captcha.aop.RequiresCaptcha;
import demo.konyushkov.dto.EmailDto;
import demo.konyushkov.dto.RestorePasswordDto;
import demo.konyushkov.service.RestorePasswordService;
import demo.konyushkov.web.aop.RestResponseDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/restore-password")
public class RestorePasswordController {

    private final RestorePasswordService service;

    @Autowired
    public RestorePasswordController(RestorePasswordService service) {
        this.service = service;
    }

    @PostMapping
    @RequiresCaptcha
    @RestResponseDetails(message = "RestorePasswordController.restore.message")
    public ResponseEntity restore(@RequestBody @Valid RestorePasswordDto restorePasswordDto) {
        service.restore(restorePasswordDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequiresCaptcha
    @PostMapping(value = "/request")
    @RestResponseDetails(title = "RestorePasswordController.createRequest.title", message = "RestorePasswordController.createRequest.message")
    public ResponseEntity createRequest(@RequestBody @Valid EmailDto emailDto) {
        service.createRequest(emailDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
