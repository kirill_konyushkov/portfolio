package demo.konyushkov.service;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.dto.EmailDto;
import demo.konyushkov.dto.RestorePasswordDto;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.service.EmailClient;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.User;
import demo.konyushkov.utils.ConfirmCodeUtils;
import demo.konyushkov.utils.EmailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class RestorePasswordServiceImpl implements RestorePasswordService {

    @Value("${app.frontend.url}")
    public String frontendUrl;

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final ConfirmCodeService confirmCodeService;
    private final EmailClient emailClient;

    @Autowired
    public RestorePasswordServiceImpl(UserService userService, PasswordEncoder passwordEncoder,
                              ConfirmCodeService confirmCodeService, EmailClient emailService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.emailClient = emailService;
        this.confirmCodeService = confirmCodeService;
    }

    @Override
    @Transactional
    public void restore(RestorePasswordDto restorePasswordDto) {

        // Validation
        try {
            ConfirmCode confirmCode = confirmCodeService.findByCode(restorePasswordDto.getConfirmCode());
            ConfirmCodeUtils.validate(confirmCode, restorePasswordDto.getUserId(), ConfirmCode.Action.RESTORE_PASSWORD);
        }
        catch (Exception ex) {
            throw new BadRequestException("{RestorePasswordServiceImpl.restore.notValid}", ex);
        }

        // Restore Password
        try {
            User account = userService.findById(restorePasswordDto.getUserId());
            account.setPassword(passwordEncoder.encode(restorePasswordDto.getNewPassword()));
            userService.save(account);
        }
        catch (Exception ex) {
            throw new InternalServerException("{RestorePasswordServiceImpl.restore.failedUpdate}", ex);
        }

        // Remove old ConfirmCode
        try {
            confirmCodeService.deleteByCode(restorePasswordDto.getConfirmCode());
        }
        catch (Exception ex) {
            log.error("Failed remove confirmCode: ", ex);
        }
    }

    @Override
    @Transactional
    public void createRequest(EmailDto emailDto) {
        User user = userService.findByEmail(emailDto.getEmail());

        ConfirmCode confirmCode = confirmCodeService.prepareAndSave(user, ConfirmCode.Action.RESTORE_PASSWORD);

        HtmlMail mail = EmailUtils.prepare(confirmCode, frontendUrl);
        emailClient.send(mail);
    }
}
