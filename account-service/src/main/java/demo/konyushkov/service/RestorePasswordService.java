package demo.konyushkov.service;

import demo.konyushkov.dto.EmailDto;
import demo.konyushkov.dto.RestorePasswordDto;

public interface RestorePasswordService {

    void restore(RestorePasswordDto restorePasswordDto);
    void createRequest(EmailDto emailDto);

}
