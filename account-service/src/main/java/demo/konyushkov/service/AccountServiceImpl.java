package demo.konyushkov.service;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.base.exception.UnauthorizedException;
import demo.konyushkov.base.utils.Precondition;
import demo.konyushkov.dto.PasswordDto;
import demo.konyushkov.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final AccountService accountService;

    @Autowired
    public AccountServiceImpl(UserService userService, PasswordEncoder passwordEncoder,
                              @Lazy AccountService accountService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.accountService = accountService;
    }

    @Override
    public User findByPrincipal(Principal principal) {
        Precondition.checkNotNull(principal, UnauthorizedException.class);

        try {
            UUID id = UUID.fromString(principal.getName());
            return userService.findById(id);
        }
        catch (Exception ex) {
            throw new InternalServerException(ex);
        }
    }

    @Override
    @Transactional
    public void updatePassword(PasswordDto passwordDto, Principal principal) {
        User account = accountService.findByPrincipal(principal);
        if(!passwordEncoder.matches(passwordDto.getCurrentPassword(), account.getPassword())) {
            throw new BadRequestException(Map.of("currentPassword", Set.of("{AccountServiceImpl.updatePassword.currentPasswordNotMatches}")));
        }
        final String newPasswordHash = passwordEncoder.encode(passwordDto.getNewPassword());
        account.setPassword(newPasswordHash);
        userService.save(account);
    }

    @Override
    @Transactional
    public User updatePersonal(User user, Principal principal) {
        User account = accountService.findByPrincipal(principal);
        BeanUtils.copyProperties(user, account, "id", "email", "password", "role", "enabled");
        return userService.save(account);
    }
}
