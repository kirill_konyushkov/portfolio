package demo.konyushkov.service;

import demo.konyushkov.base.exception.BadRequestException;
import demo.konyushkov.base.exception.InternalServerException;
import demo.konyushkov.dto.ConfirmRegistrationDto;
import demo.konyushkov.dto.RegistrationDto;
import demo.konyushkov.email.dto.HtmlMail;
import demo.konyushkov.email.service.EmailClient;
import demo.konyushkov.model.ConfirmCode;
import demo.konyushkov.model.Role;
import demo.konyushkov.model.User;
import demo.konyushkov.utils.ConfirmCodeUtils;
import demo.konyushkov.utils.EmailUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Value("${app.frontend.url}")
    public String frontendUrl;

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final ConfirmCodeService confirmCodeService;
    private final EmailClient emailClient;

    @Autowired
    public RegistrationServiceImpl(UserService userService, PasswordEncoder passwordEncoder,
                                      ConfirmCodeService confirmCodeService, EmailClient emailService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.emailClient = emailService;
        this.confirmCodeService = confirmCodeService;
    }

    @Override
    @Transactional
    public void registration(RegistrationDto registrationDto) {
        boolean emailBusy = userService.existsByEmail(registrationDto.getEmail());

        if(emailBusy) {
            throw new BadRequestException(Map.of("email", Set.of("{RegistrationServiceImpl.registration.emailBusy}")));
        }

        try {
            User user = User.builder()
                    .id(UUID.randomUUID())
                    .name(registrationDto.getName())
                    .email(registrationDto.getEmail())
                    .password(passwordEncoder.encode(registrationDto.getPassword()))
                    .enabled(false)
                    .role(new Role("ROLE_GUEST"))
                    .build();

            userService.save(user);

            ConfirmCode confirmCode = confirmCodeService.prepareAndSave(user, ConfirmCode.Action.REGISTRATION);

            HtmlMail mail = EmailUtils.prepare(confirmCode, frontendUrl);
            emailClient.send(mail);
        }
        catch (Exception ex) {
            throw new InternalServerException("{RegistrationServiceImpl.registration.internalServerException}", ex);
        }
    }

    @Override
    @Transactional
    public void confirm(ConfirmRegistrationDto confirmRegistrationDto) {
        // Validation
        try {
            ConfirmCode confirmCode = confirmCodeService.findByCode(confirmRegistrationDto.getConfirmCode());
            ConfirmCodeUtils.validate(confirmCode, confirmRegistrationDto.getUserId(), ConfirmCode.Action.REGISTRATION);
        }
        catch (Exception ex) {
            throw new BadRequestException("{RegistrationServiceImpl.confirm.notValid}", ex);
        }

        // Confirm account
        try {
            User account = userService.findById(confirmRegistrationDto.getUserId());
            account.setEnabled(true);
            userService.save(account);
        }
        catch (Exception ex) {
            throw new InternalServerException("{RegistrationServiceImpl.confirm.failedConfirm}", ex);
        }

        // Remove old ConfirmCode
        try {
            confirmCodeService.deleteByCode(confirmRegistrationDto.getConfirmCode());
        }
        catch (Exception ex) {
            log.error("Failed remove confirmCode: ", ex);
        }
    }

}
