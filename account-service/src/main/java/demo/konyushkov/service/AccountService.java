package demo.konyushkov.service;

import demo.konyushkov.dto.PasswordDto;
import demo.konyushkov.model.User;

import java.security.Principal;


public interface AccountService {
    User findByPrincipal(Principal principal);
    void updatePassword(PasswordDto passwordDto, Principal principal);
    User updatePersonal(User user, Principal principal);
}
