package demo.konyushkov.service;

import demo.konyushkov.dto.ConfirmRegistrationDto;
import demo.konyushkov.dto.RegistrationDto;

public interface RegistrationService {

    void registration(RegistrationDto registrationDto);
    void confirm(ConfirmRegistrationDto confirmRegistrationDto);

}
