package demo.konyushkov.dto;

import demo.konyushkov.base.validation.constraints.Email;
import demo.konyushkov.base.validation.constraints.Password;
import demo.konyushkov.base.validation.constraints.SpELAssert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SpELAssert(value = "password.equals(confirmPassword)",
        applyIf = "password != null && confirmPassword != null",
        message = "{RegistrationDto.crossfield.passwordAndConfirmPasswordEquals}",
        propertyPath = "confirmPassword")
public class RegistrationDto {

    @NotBlank(message = "{RegistrationDto.name.NotBlank}")
    private String name;

    @Email
    @NotNull(message = "{RegistrationDto.email.NotNull}")
    private String email;

    @Password
    @NotNull(message = "{RegistrationDto.password.NotNull}")
    private String password;

    @NotNull(message = "{RegistrationDto.confirmPassword.NotNull}")
    private String confirmPassword;

}
