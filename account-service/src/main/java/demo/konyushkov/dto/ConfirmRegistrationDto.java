package demo.konyushkov.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfirmRegistrationDto {

    @NotNull(message = "{ConfirmRegistrationDto.userId.NotNull}")
    private UUID userId;
    @NotNull(message = "{ConfirmRegistrationDto.confirmCode.NotNull}")
    private UUID confirmCode;

}
