package demo.konyushkov.dto;

import demo.konyushkov.base.validation.constraints.Password;
import demo.konyushkov.base.validation.constraints.SpELAssert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SpELAssert(value = "newPassword.equals(confirmPassword)",
        applyIf = "newPassword != null && confirmPassword != null",
        message = "{PasswordDto.crossfield.newAndConfirmEquals}",
        propertyPath = "confirmPassword")
public class PasswordDto {

    @Password
    @NotNull(message = "{PasswordDto.currentPassword.NotNull}")
    private String currentPassword;

    @Password
    @NotNull(message = "{PasswordDto.newPassword.NotNull}")
    private String newPassword;

    @NotNull(message = "{PasswordDto.confirmPassword.NotNull}")
    private String confirmPassword;
}
