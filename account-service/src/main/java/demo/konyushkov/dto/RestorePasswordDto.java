package demo.konyushkov.dto;

import demo.konyushkov.base.validation.constraints.Password;
import demo.konyushkov.base.validation.constraints.SpELAssert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;


@Data
@NoArgsConstructor
@AllArgsConstructor
@SpELAssert(value = "newPassword.equals(confirmPassword)",
        applyIf = "newPassword != null && confirmPassword != null",
        message = "{RestorePasswordDto.crossfield.newAndConfirmEquals}",
        propertyPath = "confirmPassword")
public class RestorePasswordDto {

    @NotNull(message = "{RestorePasswordDto.userId.NotNull}")
    private UUID userId;
    @NotNull(message = "{RestorePasswordDto.confirmCode.NotNull}")
    private UUID confirmCode;
    @NotNull(message = "{RestorePasswordDto.newPassword.NotNull}")
    @Password
    private String newPassword;
    @NotNull(message = "{RestorePasswordDto.confirmPassword.NotNull}")
    private String confirmPassword;

}
