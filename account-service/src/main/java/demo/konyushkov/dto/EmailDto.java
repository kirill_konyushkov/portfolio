package demo.konyushkov.dto;

import demo.konyushkov.base.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmailDto {

    @Email
    @NotNull(message = "{EmailDto.email.NotNull}")
    private String email;

}
