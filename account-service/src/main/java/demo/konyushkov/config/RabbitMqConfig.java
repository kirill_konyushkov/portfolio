package demo.konyushkov.config;

import demo.konyushkov.captcha.config.CaptchaServiceQueuesConfig;
import demo.konyushkov.email.config.EmailServiceQueueConfig;
import demo.konyushkov.rabbit.config.AbstractRabbitMqConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({CaptchaServiceQueuesConfig.class, EmailServiceQueueConfig.class})
public class RabbitMqConfig extends AbstractRabbitMqConfig {}
