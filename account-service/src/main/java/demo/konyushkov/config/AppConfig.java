package demo.konyushkov.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.konyushkov.base.config.BaseConfig;
import demo.konyushkov.captcha.aop.RequiresCaptchaInterceptor;
import demo.konyushkov.captcha.service.CaptchaClient;
import demo.konyushkov.security.config.SecurityConfig;
import demo.konyushkov.web.config.AbstractWebMvcConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

@Configuration
@Import({BaseConfig.class,
        HibernateConfig.class, SecurityConfig.class,
        RabbitMqConfig.class })
public class AppConfig extends AbstractWebMvcConfig {

    private CaptchaClient captchaClient;

    @Autowired
    protected AppConfig(LocalValidatorFactoryBean validator, ObjectMapper objectMapper, CaptchaClient captchaClient) {
        super(validator, objectMapper);
        this.captchaClient = captchaClient;
    }

    @Bean
    public RequiresCaptchaInterceptor requiresCaptchaInterceptor() {
        return new RequiresCaptchaInterceptor(captchaClient);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requiresCaptchaInterceptor());
        super.addInterceptors(registry);
    }
}
