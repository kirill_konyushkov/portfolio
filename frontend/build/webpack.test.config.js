"use strict";
const baseConfig = require("./webpack.base.config");
const merge = require("webpack-merge");
const config = merge(baseConfig, {
    mode: "development",

    devtool: "#cheap-module-source-map"
});

delete config.entry;
delete config.output;
module.exports = config;
