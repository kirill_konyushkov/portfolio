"use strict";
const path = require("path");
const {VueLoaderPlugin} = require("vue-loader");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = {
    entry: {
        app: "./src/app.js"
    },

    output: {
        path: path.resolve(__dirname, "../dist"),
        publicPath: "/",
        filename: "js/app.js",
        library: "[name]",
        chunkFilename: "js/[name].js"
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.svg$/,
                oneOf: [
                    {
                        resourceQuery: /inline/,
                        use: "svg-inline-loader"
                    },
                    {
                        use: [{
                            loader: "file-loader",
                            options: {
                                name: "img/[name].[ext]"
                            }
                        }]
                    }
                ]
            },
            {
                test: /\.(png|jp(e*)g)$/,
                use: [{
                    loader: "url-loader",
                    options: {
                        limit: 8000,
                        name: "img/[hash]-[name].[ext]"
                    }
                }]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "postcss-loader",
                    "sass-loader",
                    {
                        loader: "sass-resources-loader",
                        options: {
                            resources: [
                                path.resolve(__dirname, "../src/assets/scss/resources/mixins.scss"),
                                path.resolve(__dirname, "../src/assets/scss/resources/variables.scss")
                            ]
                        }
                    }
                ]
            },
            {
                test: /\.vue$/,
                use: "vue-loader"
            }
        ]
    },

    resolve: {
        extensions: [".js", ".vue"],

        modules: [
            path.resolve(__dirname, "../"),
            "node_modules"
        ],
        alias: {
            vue$: "vue/dist/vue.esm.js",
            "@": "src"
        }
    },

    plugins: [
        new MiniCssExtractPlugin({
            filename: "css/styles.css"
        }),
        new CopyWebpackPlugin([
            {from: "src/assets/img", to: "img"}
        ]),
        new CleanWebpackPlugin(path.resolve(__dirname, "../dist")),
        new VueLoaderPlugin()
    ]
};
