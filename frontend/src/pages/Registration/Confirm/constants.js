import {makeEnum} from "@/features/utils/ObjectUtils";

export const Status = makeEnum(["OK", "ERROR"]);
