import {makeEnum} from "@/features/utils/ObjectUtils";

export const Stages = makeEnum(["FORM", "SUCCESS"]);
