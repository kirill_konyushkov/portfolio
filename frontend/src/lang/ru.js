export const dateTimeFormats = {
    short: {
        year: 'numeric', month: 'long', day: 'numeric'
    },
    long: {
        year: 'numeric', month: 'short', day: 'numeric',
        weekday: 'short', hour: 'numeric', minute: 'numeric'
    }
};

export const messages = {
    common: {
        gender: {
            notSpecified: "Не указан",
            male: "Мужской",
            female: "Женский"
        },
        date: {
            months: {
                jan: "Январь",
                feb: "Февраль",
                mar: "Март",
                apr: "Апрель",
                may: "Май",
                jun: "Июнь",
                jul: "Июль",
                aug: "Август",
                sep: "Сентябрь",
                oct: "Октябрь",
                nov: "Ноябрь",
                dec: "Декабрь"
            }
        },
        Notification: {
            constraintViolationMessage: "Проверьте правильность введенных данных",
            serverErrorMessage: "Произошла ошибка сервера. Пожалуйста, попробуйте позже"
        }
    },
    components: {
        ui: {
            AppCaptcha: {
                error: "Пожалуйста, пройдите проверку"
            },
            AppSelect: {
                gender: {
                    title: "Пол:",
                    placeholder: "Пол..."
                }
            },
            DateInput: {
                day: {
                    title: "Число:",
                    placeholder: "Число..."
                },
                month: {
                    title: "Месяц:",
                    placeholder: "Месяц..."
                },
                year: {
                    title: "Год:",
                    placeholder: "Год..."
                },
                birthday: {
                    title: "День рождения:",
                    placeholder: "День рождения..."
                }
            }
        },
        layouts: {
            AppLayout: {
                sections: {
                    Top: {
                        TopProfile: {
                            logout: "Выйти"
                        }
                    }
                }
            }
        }
    },
    pages: {
        Account: {
            index: {
                ctxMenu: {
                    showPersonalModal: "Редактировать",
                    showPasswordModal: "Изменить пароль"
                },
                details: {
                    mobile: {
                        title: "Мобильный телефон:",
                        alt: "Не указан"
                    },
                    email: {
                        title: "Электронная почта:",
                        alt: "Не указана"
                    },
                    birthday: {
                        title: "День рождения:",
                        alt: "Не указана"
                    },
                    note: {
                        title: "Примечания:",
                        alt: "Отсутствуют"
                    }
                }
            },
            modals: {
                editPersonal: {
                    title: "Редактировать личные данные",
                    personal: {
                        name: {
                            title: "Имя:",
                            validations: {
                                required: "Введите имя"
                            }
                        },
                        surname: {
                            title: "Фамилия:"
                        },
                        note: {
                            title: "Примечания:"
                        }
                    },
                    buttons: {
                        cancel: "Отменить",
                        save: "Сохранить"
                    }
                },
                changePassword: {
                    title: "Изменить пароль",
                    passwordDto: {
                        currentPassword: {
                            title: "Текущий пароль:",
                            validations: {
                                required: "Введите текущий пароль",
                                password: "Пароль должен быть не менее 6 символов, а также содержать английские символы и цифры"
                            }
                        },
                        newPassword: {
                            title: "Новый пароль:",
                            validations: {
                                required: "Введите новый пароль",
                                password: "Пароль должен быть не менее 6 символов, а также содержать английские символы и цифры"
                            }
                        },
                        confirmPassword: {
                            title: "Подтверждение пароля:",
                            validations: {
                                required: "Повторите новый пароль",
                                sameAs: "Новый пароль и подтверждение должны совпадать"
                            }
                        }
                    },
                    buttons: {
                        change: "Изменить пароль"
                    }
                }
            }
        },
        Login: {
            index: {
                form: {
                    credentialsDto: {
                        email: {
                            title: "Электронная почта:",
                            validations: {
                                required: "Введите адрес электронной почты",
                                email: "Введите корректный адрес электронной почты"
                            }
                        },
                        password: {
                            title: "Пароль:",
                            validations: {
                                required: "Введите пароль",
                                password: "Пароль должен быть не менее 6 символов, а также содержать английские символы и цифры"
                            }
                        }
                    },
                },
                errorMessage: "Произошла ошибка сервера. Попробуйте немного позже.",
                buttons: {
                    login: "Войти"
                },
                bottom: {
                    registration: {
                        text: "Если у вас еще нет учетной записи,",
                        link: "зарегистрируйтесь"
                    },
                    restorePassword: {
                        link: "Забыли пароль?"
                    }
                }
            }
        },
        Registration: {
            Index: {
                index: {
                    form: {
                        registrationDto: {
                            name: {
                                title: "Имя:",
                                validations: {
                                    required: "Введите имя"
                                }
                            },
                            email: {
                                title: "Электронная почта:",
                                validations: {
                                    required: "Введите адрес электронной почты",
                                    email: "Введите корректный адрес электронной почты"
                                }
                            },
                            password: {
                                title: "Пароль:",
                                validations: {
                                    required: "Введите пароль",
                                    password: "Пароль должен быть не менее 6 символов, а также содержать английские символы и цифры"
                                }
                            },
                            confirmPassword: {
                                title: "Подтверждение пароля:",
                                validations: {
                                    required: "Повторите пароль",
                                    sameAs: "Пароль и подтверждение должны совпадать"
                                }
                            }
                        },
                    },
                    errorMessage: "Произошла ошибка сервера. Попробуйте немного позже.",
                    success: {
                        login: {
                            text: "После подтверждения аккаунта,",
                            link: "войдите"
                        }
                    },
                    bottom: {
                        login: {
                            text: "Если у вас уже есть аккаунт,",
                            link: "войдите"
                        }
                    },
                    buttons: {
                        registration: "Зарегистрироваться"
                    }
                }
            },
            Confirm: {
                index: {
                    ok: {
                        login: {
                            text: "Теперь Вы можете",
                            link: "войти на сайт"
                        }
                    },
                    error: {
                        title: "Произошла ошибка",
                        message: "Что-то пошло не так. Пожалуйста, попробуйте позже",
                        login: {
                            link: "Вернуться на страницу входа"
                        }
                    }
                }
            },
        },
        RestorePassword: {
            Index: {
                index: {
                    form: {
                        restorePasswordDto: {
                            newPassword: {
                                title: "Новый пароль:",
                                validations: {
                                    required: "Введите новый пароль",
                                    password: "Пароль должен быть не менее 6 символов, а также содержать английские символы и цифры"
                                }
                            },
                            confirmPassword: {
                                title: "Подтверждение пароля:",
                                validations: {
                                    required: "Повторите новый пароль",
                                    sameAs: "Новый пароль и подтверждение должны совпадать"
                                }
                            }
                        },
                    },
                    errorMessage: "Произошла ошибка сервера. Попробуйте немного позже.",
                    success: {
                        login: {
                            text: "Теперь Вы можете,",
                            link: "войти"
                        }
                    },
                    bottom: {
                        login: {
                            text: "Если Вы помните пароль,",
                            link: "войдите"
                        },
                        registration: {
                            text: "Если у вас еще нет учетной записи,",
                            link: "зарегистрируйтесь"
                        }
                    },
                    buttons: {
                        restorePassword: "Изменить пароль"
                    }
                }
            },
            Request: {
                index: {
                    form: {
                        emailDto: {
                            email: {
                                title: "Электронная почта:",
                                validations: {
                                    required: "Введите адрес электронной почты",
                                    email: "Введите корректный адрес электронной почты"
                                }
                            },
                        }
                    },
                    errorMessage: "Произошла ошибка сервера. Попробуйте немного позже.",
                    success: {
                        login: {
                            text: "После восстановления пароля,",
                            link: "войдите"
                        }
                    },
                    bottom: {
                        login: {
                            text: "Если Вы помните пароль,",
                            link: "войдите"
                        },
                        registration: {
                            text: "Если у вас еще нет учетной записи,",
                            link: "зарегистрируйтесь"
                        }
                    },
                    buttons: {
                        createRequest: "Восстановить пароль"
                    }
                }
            },
        },
    }
};