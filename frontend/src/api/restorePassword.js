import {ACCOUNT_SERVICE_URL} from "@/constants";
import http from "@/features/axios/anonymous.js";

export function createRequest(emaildDto, captchaResponse) {
    const urlParams = new URLSearchParams();
    urlParams.append("g-recaptcha-response", captchaResponse);

    return http({
        method: "post",
        url: `${ACCOUNT_SERVICE_URL}/restore-password/request`,
        params: urlParams,
        data: emaildDto
    });
}

export function restore(restorePasswordDto, captchaResponse) {
    const urlParams = new URLSearchParams();
    urlParams.append("g-recaptcha-response", captchaResponse);

    return http({
        method: "post",
        url: `${ACCOUNT_SERVICE_URL}/restore-password`,
        params: urlParams,
        data: restorePasswordDto
    });
}
