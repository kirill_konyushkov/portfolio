import qs from "qs";
import {AUTH_SERVER_URL} from "@/constants";
import http from "@/features/axios/anonymous.js";

const CLIENT_ID = "trusted-app";
const SECRET = "secret";
const GRANT_TYPE_PASSWORD = "password";
const SCOPE = "read";

export function authorize(credentialsDto, captchaResponse) {
    const data = {
        grant_type: GRANT_TYPE_PASSWORD,
        client_id: CLIENT_ID,
        username: credentialsDto.email,
        password: credentialsDto.password
    };

    if (captchaResponse) {
        data["g-recaptcha-response"] = captchaResponse;
    }

    return http({
        method: "post",
        url: `${AUTH_SERVER_URL}/oauth/token`,
        data: qs.stringify(data),
        headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=utf-8",
            Authorization: `Basic ${_getClientEncoded()}`
        }
    });
}

export function refreshToken(token) {
    const data = {
        grant_type: "refresh_token",
        refresh_token: token,
        scope: SCOPE
    };

    return http({
        method: "post",
        url: `${AUTH_SERVER_URL}/oauth/token`,
        data: qs.stringify(data),
        headers: {
            "Content-type": "application/x-www-form-urlencoded",
            Authorization: `Basic ${_getClientEncoded()}`
        }
    });
}

export function isCaptchaShow() {
    return http({
        method: "get",
        url: `${AUTH_SERVER_URL}/oauth/captcha/show`
    });
}

function _getClientEncoded() {
    return btoa(`${CLIENT_ID}:${SECRET}`);
}
