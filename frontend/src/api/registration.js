import {ACCOUNT_SERVICE_URL} from "@/constants";
import http from "@/features/axios/anonymous.js";

export function registration(registrationDto, captchaResponse) {
    const urlParams = new URLSearchParams();
    urlParams.append("g-recaptcha-response", captchaResponse);

    return http({
        method: "post",
        url: `${ACCOUNT_SERVICE_URL}/registration`,
        params: urlParams,
        data: registrationDto
    });
}

export function confirm(confirmRegistrationDto) {
    return http({
        method: "post",
        url: `${ACCOUNT_SERVICE_URL}/registration/confirm`,
        data: confirmRegistrationDto
    });
}
