import http from "@/features/axios";
import {ACCOUNT_SERVICE_URL} from "@/constants";

export function findMe() {
    return http({
        method: "get",
        url: `${ACCOUNT_SERVICE_URL}/account/me`
    });
}

export function updatePersonal(personal) {
    return http({
        method: "put",
        url: `${ACCOUNT_SERVICE_URL}/account/personal`,
        data: personal
    });
}

export function updatePassword(passwordDto) {
    return http({
        method: "put",
        url: `${ACCOUNT_SERVICE_URL}/account/password`,
        data: passwordDto
    });
}
