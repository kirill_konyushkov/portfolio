import {Types} from "./const";
import * as api from "@/api/restorePassword";

export default {

    async [Types.actions.RESTORE]({commit}, form) {
        return api.restore(form.restorePasswordDto, form.captchaResponse);
    },

    async [Types.actions.CREATE_REQUEST]({commit}, form) {
        return api.createRequest(form.emailDto, form.captchaResponse);
    },
};
