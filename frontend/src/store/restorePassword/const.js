export const namespace = "restorePassword";

export const Types = {
    actions: {
        RESTORE: "RESTORE_PASSWORD",
        CREATE_REQUEST: "CREATE_REQUEST"
    }
};