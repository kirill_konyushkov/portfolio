import {Types} from "./const";

export default {
    [Types.mutations.SET_ACCESS_TOKEN]: (state, accessToken) => {
        state.accessToken = accessToken;
    },

    [Types.mutations.SET_REFRESH_TOKEN]: (state, refreshToken) => {
        state.refreshToken = refreshToken;
    },

    [Types.mutations.SET_SHOW_CAPTCHA]: (state, showCaptcha) => {
        state.showCaptcha = showCaptcha;
    }
};
