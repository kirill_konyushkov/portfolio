export const namespace = "auth";

export const Types = {
    actions: {
        AUTHORIZE: "AUTHORIZE",
        INIT_CAPTCHA: "INIT_CAPTCHA",
        REFRESH_TOKEN: "REFRESH_TOKEN",
        LOGOUT: "LOGOUT"
    },

    getters: {
        accessToken: "accessToken",
        showCaptcha: "showCaptcha"
    },

    mutations: {
        SET_ACCESS_TOKEN: "SET_ACCESS_TOKEN",
        SET_SHOW_CAPTCHA: "SET_SHOW_CAPTCHA"
    }
};

export const ACCESS_TOKEN = "access_token";

export const REFRESH_TOKEN = "refresh_token";
