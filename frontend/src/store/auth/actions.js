import Cookies from "js-cookie";
import {ACCESS_TOKEN, REFRESH_TOKEN, Types} from "./const";
import * as api from "@/api/auth";
import {eventHub} from "@/features/common/eventHub";

export default {
    async [Types.actions.AUTHORIZE]({commit}, form) {
        return api.authorize(form.credentialsDto, form.captchaResponse).then((response) => {
            const accessToken = response.data[ACCESS_TOKEN];
            const refreshToken = response.data[REFRESH_TOKEN];

            Cookies.set(ACCESS_TOKEN, accessToken);
            Cookies.set(REFRESH_TOKEN, refreshToken);
            commit(Types.mutations.SET_ACCESS_TOKEN, accessToken);
        });
    },

    async [Types.actions.REFRESH_TOKEN]({commit}) {
        const token = Cookies.get(REFRESH_TOKEN);

        return api.refreshToken(token).then((response) => {
            const accessToken = response.data[ACCESS_TOKEN];
            const refreshToken = response.data[REFRESH_TOKEN];

            Cookies.set(ACCESS_TOKEN, accessToken);
            Cookies.set(REFRESH_TOKEN, refreshToken);
            commit(Types.mutations.SET_ACCESS_TOKEN, accessToken);
            return response;
        });
    },

    async [Types.actions.LOGOUT]({commit}) {
        Cookies.remove(ACCESS_TOKEN);
        Cookies.remove(REFRESH_TOKEN);
        commit(Types.mutations.SET_ACCESS_TOKEN, null);
    },

    async [Types.actions.INIT_CAPTCHA]({commit},) {
        api.isCaptchaShow().then((response) => {
            const result = response.data.value0;
            commit(Types.mutations.SET_SHOW_CAPTCHA, result);
        }).catch(() => {
            eventHub.$emit("authError", "Произошла ошибка при формировании страницы");
        });
    }
};
