import Cookies from "js-cookie";
import {ACCESS_TOKEN} from "./const";

export default {
    accessToken: Cookies.get(ACCESS_TOKEN),
    showCaptcha: false
};
