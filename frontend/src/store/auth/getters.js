import {Types} from "./const";

export default {
    [Types.getters.accessToken](state) {
        return state.accessToken;
    },

    [Types.getters.showCaptcha](state) {
        return state.showCaptcha;
    }
};
