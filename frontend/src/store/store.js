import Vue from "vue";
import Vuex from "vuex";
import {namespace as authNamespace} from "./auth/const";
import authModule from "./auth/module";
import {namespace as accountNamespace} from "./account/const";
import accountModule from "./account/module";
import {namespace as registrationNamespace} from "./registration/const";
import registrationModule from "./registration/module";
import {namespace as restorePasswordNamespace} from "./restorePassword/const";
import restorePasswordModule from "./restorePassword/module";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        [authNamespace]: authModule,
        [accountNamespace]: accountModule,
        [registrationNamespace]: registrationModule,
        [restorePasswordNamespace]: restorePasswordModule,
    }
});
