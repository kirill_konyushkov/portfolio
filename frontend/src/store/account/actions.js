import {Types} from "./const";
import {findMe, updatePassword, updatePersonal} from "@/api/account";

export default {
    async [Types.actions.FETCH_ME]({commit}) {
        return findMe().then((response) => {
            commit(Types.mutations.SET_ME, response.data);
            return response;
        });
    },

    async [Types.actions.UPDATE_PERSONAL]({commit}, personal) {
        return updatePersonal(personal).then((response) => {
            commit(Types.mutations.SET_ME, response.data.content);
            return response;
        });
    },

    // eslint-disable-next-line no-empty-pattern
    async [Types.actions.UPDATE_PASSWORD]({}, passwordDto) {
        return updatePassword(passwordDto);
    }
};
