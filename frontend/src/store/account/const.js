export const namespace = "account";

export const Types = {
    getters: {
        me: "me"
    },

    mutations: {
        SET_ME: "SET_ME"
    },

    actions: {
        FETCH_ME: "FETCH_ME",
        UPDATE_PERSONAL: "UPDATE_PERSONAL",
        UPDATE_PASSWORD: "UPDATE_PASSWORD"
    }
};
