import {Types} from "./const";

export default {
    [Types.getters.me](state) {
        return state.me;
    }
};
