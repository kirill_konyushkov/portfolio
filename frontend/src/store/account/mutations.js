import {Types} from "./const";

export default {
    [Types.mutations.SET_ME]: (state, me) => {
        state.me = me;
    }
};
