export const namespace = "registration";

export const Types = {
    actions: {
        REGISTRATION: "REGISTRATION",
        CONFIRM: "CONFIRM"
    }
};
