import {Types} from "./const";
import * as api from "@/api/registration";

export default {
    async [Types.actions.REGISTRATION]({commit}, form) {
        return api.registration(form.registrationDto, form.captchaResponse);
    },

    async [Types.actions.CONFIRM]({commit}, confirmRegistrationDto) {
        return api.confirm(confirmRegistrationDto);
    }
};
