import {makeEnum} from "@/features/utils/ObjectUtils";

export const Align = makeEnum(["LEFT", "CENTER", "RIGHT"]);
