import {makeEnum} from "@/features/utils/ObjectUtils";

export const Types = makeEnum(["TEXT", "PASSWORD", "EMAIL", "TEXTAREA"]);
