export const SELECT_GENDER = {
    title: "components.ui.AppSelect.gender.title",
    placeholder: "components.ui.AppSelect.gender.placeholder",
    options: [
        {
            value: 0,
            text: "common.gender.notSpecified"
        },
        {
            value: 1,
            text: "common.gender.male"
        },
        {
            value: 2,
            text: "common.gender.female"
        }
    ]
};
