export const BIRTHDAY = {
    title: "components.ui.DateInput.birthday.title",
    placeholder: "components.ui.DateInput.birthday.placeholder",
    startYear: 1950,
    lastYear: 2004
};

const month_code = ["common.date.months.jan",
    "common.date.months.feb",
    "common.date.months.mar",
    "common.date.months.apr",
    "common.date.months.may",
    "common.date.months.jun",
    "common.date.months.jul",
    "common.date.months.aug",
    "common.date.months.sep",
    "common.date.months.oct",
    "common.date.months.nov",
    "common.date.months.dec"];

export const MONTHS_OPTIONS = month_code.map((month, index) => ({
    value: index + 1,
    text: month
}));
