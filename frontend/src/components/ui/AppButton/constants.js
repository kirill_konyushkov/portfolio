import {makeEnum} from "@/features/utils/ObjectUtils";

export const Themes = makeEnum(["BLUE", "LIGHT", "WHITE"]);

export const TextAlign = makeEnum(["LEFT", "CENTER"]);

export const Size = makeEnum(["STANDARD", "FILL_AREA"]);
