import {makeEnum} from "@/features/utils/ObjectUtils";

export const AlignItems = makeEnum(["START", "CENTER"]);
