export default {
    data() {
        return {
            isDropped: false
        };
    },

    methods: {
        dropdown(e) {
            if (this.isDropped) {
                const el = this.$refs.dropdown;
                const target = e.target;

                if (el !== target && !el.contains(target)) {
                    this.close();
                }
            }
        },

        toggle() {
            this.isDropped = !this.isDropped;
        },

        close() {
            this.isDropped = false;
        }
    },

    created() {
        document.addEventListener("click", this.dropdown);
    },

    destroyed() {
        document.removeEventListener("click", this.dropdown);
    }
};
