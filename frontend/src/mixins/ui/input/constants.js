import {makeEnum} from "@/features/utils/ObjectUtils";

export const States = makeEnum(["DEFAULT", "ERROR", "VALID"]);
