import {States} from "./constants";

export default {
    props: {
        title: {
            type: String,
            required: true
        },

        value: {
            required: true
        },

        placeholder: {
            type: String
        },

        required: {
            type: Boolean
        },

        validation: {
            type: Object
        },

        serverErrors: {
            type: Array
        }
    },

    constants: {
        input: {
            states: States
        }
    },

    components: {
        InputBlock: () => import("@/components/ui/InputBlock")
    },

    computed: {
        state() {
            if ((!this.validation || !this.validation.$dirty) && !this.hasServerErrors) {
                return this.$const.input.states.DEFAULT;
            }
            else if (this.validation.$invalid || this.hasServerErrors) {
                return this.$const.input.states.ERROR;
            }

            return this.$const.input.states.VALID;
        },

        getErrors() {
            if (this.hasServerErrors) {
                return this.serverErrors.map((er) => er);
            }
            else if (this.hasValidationParams) {
                return Object.keys(this.validation.$params).reduce((result, key) => {
                    if (!this.validation[key] && this.validation.$dirty) {
                        result.push(this.validation.$params[key].msg);
                    }

                    return result;
                }, []);
            }

            return null;
        },

        hasServerErrors() {
            return Boolean(this.serverErrors) && this.serverErrors.length > 0;
        },

        hasValidationParams() {
            return Boolean(this.validation) && Boolean(this.validation.$params);
        }
    }
};
