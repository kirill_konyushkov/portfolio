function getStoreModule(vm) {
    const {storeModule} = vm.$options;

    if (storeModule && typeof storeModule === "function") {
        return storeModule.call();
    }

    return null;
}

export default {
    beforeCreate() {
        const storeModule = getStoreModule(this);
        const store = this.$store;

        if (storeModule && !(store && store.state && store.state[storeModule.namespace])) {
            store.registerModule(storeModule.namespace, storeModule.module);
        }
    }
};
