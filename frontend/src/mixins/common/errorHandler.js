import {addWarnNotify} from "@/features/notification/NotifyService";

export default {
    data() {
        return {
            serverErrors: {}
        };
    },

    methods: {
        handleErrors(error) {
            if (error.response.status === 400) {
                this.$eventHub.$emit("throwErrors", error.response.data.content);
                this.serverErrors = error.response.data.content;
                addWarnNotify(error.response.data.message);
            }
        }
    }
};
