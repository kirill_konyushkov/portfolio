export default {
    install(Vue) {
        Vue.mixin({
            beforeCreate() {
                const constants = this.$options.constants;

                if (!constants) {
                    return;
                }
                if (!this.$const) {
                    this.$const = {};
                }
                Object.assign(this.$const, this.$options.constants);
            }
        });
    }
};
