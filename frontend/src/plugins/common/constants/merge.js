const merge = (mixinConstants, componentConstants) => Object.assign({}, mixinConstants, componentConstants);

export default merge;
