const merge = (mixinUtils, componentUtils) => Object.assign({}, mixinUtils, componentUtils);

export default merge;
