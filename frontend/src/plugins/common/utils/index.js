export default {
    install(Vue) {
        Vue.mixin({
            beforeCreate() {
                const constants = this.$options.utils;

                if (!constants) {
                    return;
                }
                this.$utils = {};
                Object.assign(this.$utils, this.$options.utils);
            }
        });
    }
};
