import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    // SECURE PAGES
    {
        path: "/app", component: () => import("@/components/layouts/AppLayout"),
        children: [
            {
                path: "",
                component: () => import("@/pages/Account"),
                name: "Account",
                meta: {description: "Edit my account"}
            },
        ]
    },

    // ANONYMOUS PAGES
    {
        path: "/", component: () => import("@/components/layouts/AnonymousLayout"),
        children: [
            {
                path: "",
                redirect: {name : "Account"}
            },
            {
                path: "/login",
                name: "Login",
                component: () => import("@/pages/Login"),
            },
            {
                path: "/registration",
                component: () => import("@/pages/Registration"),

                children: [
                    {
                        path: "",
                        name: "Registration",
                        component: () => import("@/pages/Registration/Index"),
                    },
                    {
                        path: "confirm",
                        name: "RegistrationConfirm",
                        component: () => import("@/pages/Registration/Confirm")
                    }
                ]
            },
            {
                path: "/restore-password",
                component: () => import("@/pages/RestorePassword"),

                children: [
                    {
                        path: "",
                        name: "RestorePassword",
                        component: () => import("@/pages/RestorePassword/Index"),
                    },
                    {
                        path: "request",
                        name: "RestorePasswordRequest",
                        component: () => import("@/pages/RestorePassword/Request")
                    }
                ]
            }
        ]
    }
];

export function createRouter() {
    return new VueRouter({
        mode: "history",
        routes
    });
}
