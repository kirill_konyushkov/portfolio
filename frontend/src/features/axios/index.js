import axios from "axios";
import app from "@/app.js";
import {namespace, Types} from "@/store/auth/const";
import {addWarnNotify} from "@/features/notification/NotifyService";


const http = axios.create();

export default http;

http.interceptors.request.use((config) => {
    const token = app.$store.state[namespace][Types.getters.accessToken];

    if ( token !== null ) {
        config.headers.Authorization = `Bearer ${token}`;
    }

    return config;
}, (err) => Promise.reject(err));

http.interceptors.response.use(response => response,
    async(error) => {
    const originalRequest = error.config;

    // 401
    if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true;

        await app.$store.dispatch(`${namespace}/${Types.actions.REFRESH_TOKEN}`).then(() => {
            const accessToken = app.$store.state[namespace][Types.getters.accessToken];
            originalRequest.headers.Authorization = `Bearer ${accessToken}`;
        });

        return http(originalRequest);
    }
    else if (error.response.status === 401 && originalRequest._retry) {
        await app.$store.dispatch(`${namespace}/${Types.actions.LOGOUT}`);
        app.$router.push({name: "Login"});
    }

    // 500
    if(error.response.status === 500) {
        addWarnNotify(error.response.data.message);
    }

    return Promise.reject(error);
});
