import {eventHub} from "@/features/common/eventHub";

export function showPreloader() {
    eventHub.$emit("showPreloader");
}

export function hidePreloader() {
    eventHub.$emit("hidePreloader");
}
