import {makeEnum} from "@/features/utils/ObjectUtils";

export const Status = makeEnum(["SUCCESS", "WARN", "ERROR"]);