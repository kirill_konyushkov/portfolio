import {eventHub} from "@/features/common/eventHub";
import {Status} from "./const";

export function addSuccessNotify(message) {
    eventHub.$emit("addNotify", {
        status: Status.SUCCESS,
        message
    });
}

export function addWarnNotify(message) {
    eventHub.$emit("addNotify", {
        status: Status.WARN,
        message
    });
}
