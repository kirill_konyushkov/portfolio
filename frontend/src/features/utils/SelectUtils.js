export const i18nTransformOptions = (options, vm) => options.map((item) => ({
    value: item.value,
    text: vm.$t(item.text)
}));
