export const pick = (O, ...K) => K.reduce((o, k) => (o[k] = O[k], o), {});

export const getCopy = (obj) => ({...obj});

export const makeEnum = (arr) => {
    const obj = {};

    for (const val of arr) {
        obj[val] = val; // eslint-disable-line no-undef
    }

    return Object.freeze(obj);
};
