const emailRegExp = new RegExp("^[\\S]+@[\\S]+\\.[\\S]+$");
const passwordRegExp = new RegExp("^(?=.*\\d)(?=.*[a-zA-Z])[a-zA-Z\\d#$%&'()*+,\\-.\\\\/:;<>=?@\\[\\]{}^_`~]{6,20}$");

export function email(value) {
    return value ? emailRegExp.test(value) : true;
}

export function password(value) {
    return value ? passwordRegExp.test(value) : true;
}
