import Vue from "vue";
import App from "./App.vue";
import constants from "@/plugins/common/constants";
import mergeConstants from "@/plugins/common/constants/merge.js";
import mergeUtils from "@/plugins/common/utils/merge.js";
import utils from "@/plugins/common/utils";
import Vuelidate from "vuelidate";
import store from "@/store/store";
import {createRouter} from "@/router/router";
import {dateTimeFormats as ruDateTimeFormats, messages as ruMessages} from "@/lang/ru";

import VueI18n from "vue-i18n";

Vue.use(VueI18n);
Vue.use(Vuelidate);
Vue.use(constants);
Vue.config.optionMergeStrategies.constants = mergeConstants;
Vue.use(utils);
Vue.config.optionMergeStrategies.utils = mergeUtils;

const i18n = new VueI18n({
    locale: "ru",
    fallbackLocale: "ru",
    dateTimeFormats: {
        ru: ruDateTimeFormats,
    },
    messages: {
        ru: ruMessages
    }
});
const router = createRouter();
const app = new Vue({ // eslint-disable-line no-unused-vars
    el: "#app",
    i18n,
    store,
    router,
    render: (h) => h(App)
});

export default app;
