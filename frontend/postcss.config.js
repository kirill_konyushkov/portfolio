const autoprefixer = require("autoprefixer");

module.exports = {
    ident: "postcss",
    plugins: [
        autoprefixer({
            browsers: [
                "ie >= 8",
                "last 2 versions",
                "iOS >= 8",
                "Safari >= 8"]
        })
    ],
    sourceMap: true
};
